﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Collections;
using SmartManager.Functionality;
using SmartManager.Objects;

namespace SmartManager
{
    [TestFixture]
    class EmployeeManagerTest
    {
        #region Class Variables

        private EmployeeManager employeeManager;
        private ArrayList employeeInfo;
        private ArrayList employeeInfo2;

        #endregion

        #region SetUp and TearDown

        /// <summary>
        /// Set up method
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            employeeManager = new EmployeeManager();
            employeeInfo = new ArrayList();
            employeeInfo2 = new ArrayList();
        }

        /// <summary>
        /// Tear down method
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            employeeInfo.Clear();
            employeeInfo2.Clear();
        }

        #endregion

        #region Tests

        /// <summary>
        /// adds an employee and retrieves information
        /// </summary>
        [Test]
        public void AddEmployee()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);
            employeeInfo.Insert(3, employeeNumber);
            EmployeeObj employeeObj = employeeManager.GetEmployee(employeeNumber);

            Assert.AreEqual(employeeInfo, employeeObj.EmployeeInfo);
            employeeManager.DeletEmployee(employeeNumber);
        }

        /// <summary>
        /// Adds an employee and then removes the employee
        /// </summary>
        [Test]
        public void RemoveExistingEmployee()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);
            employeeManager.DeletEmployee(employeeNumber);

            Assert.IsNull(employeeManager.GetEmployee(employeeNumber));
        }


        /// <summary>
        /// Tries to remove non-existing employee
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveNonExistingEmployee()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);
            employeeManager.DeletEmployee(employeeNumber);
            employeeManager.DeletEmployee(employeeNumber);
        }

        /// <summary>
        /// Search for existing employee 
        /// </summary>
        [Test]
        public void SearchExistingEmployee()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);
            employeeInfo.Insert(3, employeeNumber);
            Assert.AreEqual(employeeInfo, employeeManager.searchEmployees(new String[] { "Arnold", "" }).ElementAt(0).EmployeeInfo);
            employeeManager.DeletEmployee(employeeNumber);
        }

        /// <summary>
        /// Searches for list of employees with same name but different employee number
        /// </summary>
        [Test]
        public void SearchEmployeeSameName()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arn");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber1 = employeeManager.SetEmployee(employeeInfo);
            employeeInfo.Insert(3, employeeNumber1);

            employeeInfo2.Add("Arnold");
            employeeInfo2.Add("Schwartz");
            employeeInfo2.Add("Regular");
            employeeInfo2.Add("Arn2");
            employeeInfo2.Add(0);
            employeeInfo2.Add(null);

            int employeeNumber2 = employeeManager.SetEmployee(employeeInfo2);
            employeeInfo2.Insert(3, employeeNumber2);

            Assert.AreEqual(employeeInfo, employeeManager.searchEmployees(new String[] { "Arnold", "" }).ElementAt(0).EmployeeInfo);
            Assert.AreEqual(employeeInfo2, employeeManager.searchEmployees(new String[] { "Arnold", "" }).ElementAt(1).EmployeeInfo);
            employeeManager.DeletEmployee(employeeNumber1);
            employeeManager.DeletEmployee(employeeNumber2);
        }

        /// <summary>
        /// Edit employee information and check correctness
        /// </summary>
        [Test]
        public void EditEmployeeInfo()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);
            employeeInfo.Insert(3, employeeNumber);

            employeeInfo[0] = "Terminator";

            employeeManager.EditEmployee(employeeNumber, employeeInfo);

            Assert.AreEqual(employeeInfo, employeeManager.GetEmployee(employeeNumber).EmployeeInfo);
            employeeManager.DeletEmployee(employeeNumber);
        }

        /// <summary>
        /// Logs in an employee and checks flag
        /// </summary>
        [Test]
        public void LogIn()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);
            employeeManager.LogInEmployee(employeeNumber, "Arnie");

            Assert.AreEqual(1, employeeManager.GetEmployee(employeeNumber).EmployeeInfo[5]);
            employeeManager.DeletEmployee(employeeNumber);
        }

        /// <summary>
        /// Logs out an employee and checks flag
        /// </summary>
        [Test]
        public void LogOut()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);

            EmployeeObj employeeObj = employeeManager.GetEmployee(employeeNumber);

            employeeManager.LogOutEmployee(employeeNumber, employeeObj);

            Assert.AreEqual(0, employeeManager.GetEmployee(employeeNumber).EmployeeInfo[5]);
            employeeManager.DeletEmployee(employeeNumber);
        }

        [Test]
        public void LogInIncorrectCredentials()
        {
            employeeInfo.Add("Arnold");
            employeeInfo.Add("Schwartz");
            employeeInfo.Add("Regular");
            employeeInfo.Add("Arnie");
            employeeInfo.Add(0);
            employeeInfo.Add(null);

            int employeeNumber = employeeManager.SetEmployee(employeeInfo);
            employeeManager.LogInEmployee(employeeNumber, "Arnies");

            Assert.AreEqual(0, employeeManager.GetEmployee(employeeNumber).EmployeeInfo[5]);
            employeeManager.DeletEmployee(employeeNumber);
        }

        #endregion
    }
}
