﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Collections;

namespace SmartManager
{
    [TestFixture]
    public class RoomManagerTest
    {
        #region Class Variables

        private RoomManager roomManager;
        private ArrayList roomInfo;

        #endregion


        #region Setup and Teardown

        /// <summary>
        /// Set up method
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            roomManager = new RoomManager();
            roomInfo = new ArrayList();
        }

        /// <summary>
        /// Tear down method
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            roomManager = null;
            roomInfo.Clear();
        }

        #endregion


        #region Set Room Tests

        /// <summary>
        /// Initially sets a room with all attributes
        /// From empty list
        /// </summary>
        [Test]
        public void SetInitRoom()
        {
            roomInfo.Add("101");
            roomInfo.Add("Double");
            roomInfo.Add("80.00");

            roomManager.SetRoom(roomInfo);

            Assert.AreEqual(roomManager.GetRoom(101, "Double").RoomInfo[2].ToString(), "80.00");
            roomManager.DeleteRoom(roomInfo);
        }

        /// <summary>
        /// Sets several rooms with all attributes
        /// </summary>
        [Test]
        public void SetManyRooms()
        {
            roomInfo.Add("102");
            roomInfo.Add("Single");
            roomInfo.Add("90.00");

            ArrayList roomInfo2 = new ArrayList();
            roomInfo2.Add("103");
            roomInfo2.Add("Twin");
            roomInfo2.Add("75.00");

            roomManager.SetRoom(roomInfo);
            roomManager.SetRoom(roomInfo2);

            Assert.AreEqual(roomManager.GetRoom(102, "Single").RoomInfo[2].ToString(), "90.00");
            Assert.AreEqual(roomManager.GetRoom(103, "Twin").RoomInfo[2].ToString(), "75.00");
            roomManager.DeleteRoom(roomInfo);
            roomManager.DeleteRoom(roomInfo2);
        }

        #endregion


        #region Get Room Tests

        /// <summary>
        /// Retrieves room details
        /// That do not exist
        /// </summary>
        [Test]
        public void GetNonExistingRoom()
        {
            RoomObj room = roomManager.GetRoom(1, "Hello");
            Assert.IsNull(room);
        }

        /// <summary>
        /// Retrieves room details
        /// That exist in the database
        /// </summary>
        [Test]
        public void GetExistingRoom()
        {
            var room = roomManager.GetRoom(160, "Queen"); // already exists in DB

            Assert.IsInstanceOf<RoomObj>(room);
        }


        #endregion


        #region Delete Room Tests

        /// <summary>
        /// Deletes room details
        /// That do not exist
        /// </summary>
        [Test]
        [ExpectedException(typeof (System.Exception))]
        public void DeleteNonExistingRoom()
        {
            roomInfo.Add("0");
            roomInfo.Add("hello");
            roomInfo.Add("0");

            roomManager.DeleteRoom(roomInfo);
        }

        /// <summary>
        /// Deletes room details
        /// That exist in the database
        /// </summary>
        [Test]
        public void DeleteExistingRoom()
        {
            roomInfo.Add("510");
            roomInfo.Add("Queen");
            roomInfo.Add("140.00");

            // add a room entry to delete
            roomManager.SetRoom(roomInfo);

            // delete room entry
            roomManager.DeleteRoom(roomInfo);

            Assert.IsNull(roomManager.GetRoom(510, "Queen"));
        }

        #endregion


        #region Temporary Tests (Currently deemed invalid tests)

        /// <summary>
        /// Deletes room details
        /// That are null/empty
        /// </summary>
        //[Test]        NOT A VALID TEST (for now)
        public void DeleteNullRoom()
        {
            roomInfo.Clear();
            roomManager.DeleteRoom(roomInfo);

            // TODO check if this is correct
            //Assert.IsNullOrEmpty(null);

            Assert.Fail();
        }
/*
        /// <summary>
        /// Retrieves room details
        /// That are null/empty
        /// </summary>
        //[Test]        NOT A VALID TEST (for now)
        public void GetNullRoom()
        {
            roomManager.GetRoom(Convert.ToInt32(null), "");

            Assert.IsNullOrEmpty(null);

            //Assert.Fail();
        }

        /// <summary>
        /// Sets a room with empty arrayList
        /// </summary>
        //[Test]        NOT A VALID TEST (for now)
        public void SetNullRoom()
        {
            roomInfo.Clear();// Might not need to do this
            roomManager.SetRoom(roomInfo);

            // Checks if all roomObj variables are null
            Assert.IsNullOrEmpty(roomManager.GetRoomInfo().RoomInfo[0].ToString()); // roomNum
            Assert.IsNullOrEmpty(roomManager.GetRoomInfo().RoomInfo[1].ToString()); // roomType
            Assert.IsNullOrEmpty(roomManager.GetRoomInfo().RoomInfo[2].ToString()); // rackRate
        }
*/
        #endregion
    }
}