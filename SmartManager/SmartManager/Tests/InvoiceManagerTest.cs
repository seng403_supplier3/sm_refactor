﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using NUnit.Framework;
using SmartManager.Functionality;
using SmartManager.Objects;

namespace SmartManager.Tests
{
    [TestFixture]
    class InvoiceManagerTest
    {
        private DBHandler dbh;
        private ArrayList invoiceInfo;
        private ArrayList tempGuest;
        private GuestObj tempGuestObj;
        private InvoiceManager IM;
        SmartManagerDataClassesDataContext db;
        private RoomBooking roombook;
       // private int invoiceID1;
      //  private int bookingID1;
        private int guestID1;

        #region Setup & Teardown
        [SetUp]
        public void SetUp()
        {
            
            #region TempGuest setup
            tempGuest = new ArrayList();
            tempGuest.Add("Lebron");
            tempGuest.Add("James");
            tempGuest.Add("cant_jump@miami.com");
            tempGuest.Add("416-398-5413");
            tempGuest.Add("2901 Jane St.");
            tempGuest.Add("NA");
            tempGuest.Add("Seasdoul");
            tempGuest.Add("KdR");
            tempGuest.Add("100020");
            tempGuest.Add("KOR1EA");
            tempGuest.Add(0);
            tempGuest.Add(0);
            tempGuest.Add(new DateTime(2012, 9, 9));
            tempGuest.Add(new DateTime(2012, 9, 30));
            tempGuest.Add("VI");
            tempGuest.Add("4199888899994321");
            tempGuest.Add("Lebron James");
            tempGuest.Add("835");
            tempGuest.Add("09/14");

            tempGuestObj = new GuestObj(tempGuest);
            #endregion

            db = new SmartManagerDataClassesDataContext();
            roombook = new RoomBooking();

           
            #region InvoiceInfo Setup
            invoiceInfo = new ArrayList();
            invoiceInfo.Add(0);
            invoiceInfo.Add(1757);
            invoiceInfo.Add(87);
            invoiceInfo.Add(1845);
            #endregion 

            dbh = new DBHandler();
            guestID1 =  dbh.AddGuest(tempGuestObj);
            roombook.GuestID = guestID1;

            roombook.BillingRoomRate = 250;
            roombook.RoomID = 10;
            roombook.StartBookedDate = Convert.ToDateTime("01/01/2014 12:00:00 AM");
            roombook.EndBookedDate = Convert.ToDateTime("02/02/2014 12:00:00 AM");

            //insert fakebooking
            db.RoomBookings.InsertOnSubmit(roombook);
            db.SubmitChanges();


            IM = new InvoiceManager();
        }

        [TearDown]
        public void TearDown()
        {
            dbh.RemoveGuest(tempGuestObj);
            db.RoomBookings.DeleteOnSubmit(roombook);
            db.SubmitChanges();

            dbh = null;
            IM = null;
            db = null;


        }
        #endregion

        #region AddInvoiceTest

        [Test]
        public void AddInvoiceTest()
        {
            Assert.NotNull(dbh.AddInvoice(roombook.RoomBookingID, guestID1));
            dbh.RemoveInvoice(roombook.RoomBookingID, guestID1);
        }

        #endregion 

        #region GetInvoiceTest

        [Test]
        public void GetInvoiceTest()
        {
            //InvoiceObj invoiceObj = IM.GetInvoice(tempGuest);
            //Assert.AreEqual("Lebron", invoiceObj.InvoiceInfo[1]);
            
        }
        #endregion 


    }
}
