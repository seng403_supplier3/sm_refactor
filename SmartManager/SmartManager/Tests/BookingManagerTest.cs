﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Collections;


namespace SmartManager
{
    [TestFixture]
    class BookingManagerTest
    {
        #region Class Variables

        private BookingManager bookingManager;
        private GuestManager guestManager;
        private RoomManager roomManager;
        private ArrayList bookingInfo;
        private ArrayList roomInfo;
        private ArrayList guestInfo;

        #endregion

        #region SetUp and TearDown

        /// <summary>
        /// Set up method
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            bookingManager = new BookingManager();
            guestManager = new GuestManager();
            roomManager = new RoomManager();
            bookingInfo = new ArrayList();
            roomInfo = new ArrayList();
            guestInfo = new ArrayList();
        }

        /// <summary>
        /// Tear down method
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            if (bookingManager.GetBooking(roomInfo, guestInfo) != null)
                bookingManager.DeleteBooking(new ArrayList(), roomInfo, guestInfo);
            
            guestManager.DeleteGuest(guestInfo);

            bookingInfo = new ArrayList();
            roomInfo = new ArrayList();
            guestInfo = new ArrayList();

        }

        #endregion

        #region Tests

        /// <summary>
        /// Test adding a new booking and searching for it.
        /// We use booking, room and guest ArrayList and call SetBooking in BookingManager
        /// </summary>
        [Test]
        public void AddBooking()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            //TODO: AddGuest and GetGuest has not been properly implemented, so we will use existing guest's info instead...
            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            Assert.AreEqual(new DateTime(2014, 3, 6), Convert.ToDateTime(bookingManager.GetBooking(roomInfo, guestInfo).BookingInfo[0]));
        }

        /// <summary>
        /// Edit an existing booking and verifying the information
        /// </summary>
        [Test]
        public void GetEditedBooking()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate    

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);

            bookingInfo[0] = new DateTime(2014, 3, 8);
            booking.BookingInfo[0] = bookingInfo[0];

            bookingManager.EditBooking(bookingInfo, roomInfo, guestInfo);
     
            Assert.AreEqual(new DateTime(2014, 3, 8), Convert.ToDateTime(booking.BookingInfo[0]));
        }

        /// <summary>
        /// Check in the guest and check the flag associated with checkin
        /// </summary>
        [Test]
        public void CheckIn()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate  

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);
            GuestObj guest = new GuestObj(guestInfo);

            bookingManager.CheckIn(booking, guest);

            Assert.AreEqual(BookingObj.Status.CheckedIn, booking.BookingStatus);
        }

        /// <summary>
        /// Check the guest in and then check out
        /// </summary>
        [Test]
        public void CheckOut()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate  

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);
            GuestObj guest = new GuestObj(guestInfo);

            bookingManager.CheckIn(booking, guest);
            bookingManager.CheckOut(booking, roomInfo, guestInfo);

            Assert.AreEqual(BookingObj.Status.NotCheckedIn, booking.BookingStatus);
        }

        /// <summary>
        /// Checking same guest out twice
        /// </summary>
        [Test]
        [ExpectedException(typeof(Exception))]
        public void CheckOutTwice()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate  

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);
            GuestObj guest = new GuestObj(guestInfo);

            bookingManager.CheckIn(booking, guest);
            bookingManager.CheckOut(booking, roomInfo, guestInfo);
            bookingManager.CheckOut(booking, roomInfo, guestInfo);
        }

        /// <summary>
        /// Checking same guest in twice
        /// </summary>
        [Test]
        [ExpectedException(typeof(Exception))]
        public void CheckInTwice()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate  

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);
            GuestObj guest = new GuestObj(guestInfo);

            bookingManager.CheckIn(booking,guest);
            bookingManager.CheckIn(booking,guest);
        }

        /// <summary>
        /// Checking out before checking in
        /// </summary>
        [Test]
        [ExpectedException(typeof(Exception))]
        public void CheckOutBeforeCheckIn()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate  

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);

            bookingManager.CheckOut(booking, roomInfo, guestInfo);
        }

        /// <summary>
        /// Remove existing booking
        /// </summary>
        [Test]
        public void GetDeleteBooking()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate    

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            bookingManager.DeleteBooking(bookingInfo, roomInfo, guestInfo);

            Assert.IsNull(bookingManager.GetBooking(roomInfo, guestInfo));
        }

        /// <summary>
        /// Trying to edit a booking that has not been created
        /// </summary>
        [Test]
        public void EditDeletedBooking()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate    

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            bookingManager.DeleteBooking(bookingInfo, roomInfo, guestInfo);

            Assert.IsNull(bookingManager.GetBooking(roomInfo, guestInfo));
        }    
        

        /// <summary>
        /// Edit a booking and then delete
        /// </summary>
        [Test]
        public void DeleteEditedBooking()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate    

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);

            bookingInfo[0] = new DateTime(2014, 3, 8);
            booking.BookingInfo[0] = bookingInfo[0];

            bookingManager.EditBooking(bookingInfo, roomInfo, guestInfo);

            bookingManager.DeleteBooking(bookingInfo, roomInfo, guestInfo);

            Assert.IsNull(bookingManager.GetBooking(roomInfo, guestInfo));
        } 
         
        /// <summary>
        /// Checking in after deleting the booking
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteBookingCheckIn()
        {
            // Create bookingInfo to be passed into bookingManager
            bookingInfo.Add(new DateTime(2014, 3, 6)); //StartBookedDate
            bookingInfo.Add(new DateTime(2014, 3, 10)); //EndBookedDate
            bookingInfo.Add("10.00"); //BillingRoomRate
            bookingInfo.Add(null); //CheckedOutDate=null
            bookingInfo.Add(null); //ModifiedDate=null
            bookingInfo.Add(null); //VDelDate

            // Create roomInfo to be passed into bookingManager
            // Normally, we get roomInfo from querying DB
            roomInfo.Add("101"); //RoomNumber
            roomInfo.Add("Single"); //TypeOfRoom
            roomInfo.Add("15.00"); //RackRate
            roomInfo.Add(null); //VDelDate

            // Create guestInfo to pass into bookingManager and guestManager
            guestInfo.Add("Bill");
            guestInfo.Add("Gates");
            guestInfo.Add("AtonDeras@gmail.com");
            guestInfo.Add("123-4567");
            guestInfo.Add("24 Beverely Hills Street");
            guestInfo.Add(null); //Address2
            guestInfo.Add("Los Angeles");
            guestInfo.Add("California");
            guestInfo.Add("265765"); //ZipPostalCode
            guestInfo.Add("United States");
            guestInfo.Add("1"); //isVIP
            guestInfo.Add("0"); // checkin flag
            guestInfo.Add(new DateTime(2014, 3, 6)); //ArrivalDate
            guestInfo.Add(new DateTime(2014, 3, 10)); //DepartureDate
            guestInfo.Add("Master card");
            guestInfo.Add("4003-7855-0045-3433");
            guestInfo.Add("Bill");
            guestInfo.Add("223"); //BillingCardSecurityNumber
            guestInfo.Add(new DateTime(2017, 6, 20)); //BillingCardExpiration
            guestInfo.Add(null); //CheckedOutDate
            guestInfo.Add(null); //VDelDate    

            bookingManager.SetBooking(bookingInfo, 1, roomInfo);

            bookingManager.DeleteBooking(bookingInfo, roomInfo, guestInfo);

            BookingObj booking = bookingManager.GetBooking(roomInfo, guestInfo);
            GuestObj guest = new GuestObj(guestInfo);

            bookingManager.CheckIn(booking, guest);
        }

        #endregion
    }
}
