﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using NUnit.Framework;
using SmartManager.Functionality;
using SmartManager.Objects;


namespace SmartManager.Tests
{
    class ServiceManagerTest
    {
        #region Variables & Instances
        private ServiceManager SM;
        private ArrayList ServiceInfo;
        private DBHandler dbh;
        #endregion

        #region SetUp & TearDown
        [SetUp]
        public void SetUp()
        {
            ServiceInfo = new ArrayList();
            ServiceInfo.Add("Breakfast");
            ServiceInfo.Add("Meal Charge");
            ServiceInfo.Add(8.00);

            SM = new ServiceManager();
            dbh = new DBHandler();
        }

        [TearDown]
        public void TearDown()
        {
            ServiceInfo.Clear();
            SM = null;
        }
        #endregion

        #region AddServiceTest
        [Test]
        public void AddServiceTest()
        {
            SM.setService(ServiceInfo);

            var db = new SmartManagerDataClassesDataContext();
            var serviceQuery = (from r in db.Services
                                where r.ServiceName.Equals(ServiceInfo[0])
                                select r).SingleOrDefault();

            Assert.AreEqual("Breakfast", ServiceInfo[0]); 
        }
        #endregion

        
    }
}
