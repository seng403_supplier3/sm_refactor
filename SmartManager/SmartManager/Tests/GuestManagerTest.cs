﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace SmartManager.Tests
{
    [TestFixture]
    class GuestManagerTest
    {
        GuestManager gm;
        ArrayList arr1;
        ArrayList arr2;
        ArrayList arr3;

        byte BYTE_NO;
        [SetUp]
        public void SetUp()
        {
            gm = new GuestManager();
            arr1 = new ArrayList(18);
            arr2 = new ArrayList(18);
            arr3 = new ArrayList(18);
            BYTE_NO = 0;

            /// [firstName, lastName, email, phoneNumber, 
            /// address1, address2, city, stateProv, 
            /// zipPostalCode, country,
            /// IsVIP, departureDate, billingCardType, 
            /// billingCardNum, billingCardHolderName, 
            /// billingCardSecurityNum, billingCardExpiration]

            #region Guest1_Setup_Ryu_Hayabusa
            arr1.Add("Ryu");
            arr1.Add("Hayabusa");
            arr1.Add("ryu@gmail.com");
            arr1.Add("555-123-1234");
            arr1.Add("123 Main St. NE");
            arr1.Add(null);
            arr1.Add("Calgary");
            arr1.Add("Alberta");
            arr1.Add("A1B 2C3");
            arr1.Add("Canada");
            arr1.Add(BYTE_NO); //isVip
            arr1.Add(BYTE_NO); //isCheckedIn
            arr1.Add(new DateTime(2014, 4, 20)); //arrival
            arr1.Add(new DateTime(2014, 4, 24)); //departure
            arr1.Add("Mastercard");
            arr1.Add("5212345678901234");
            arr1.Add("Ryu Hayabusa");
            arr1.Add("123");
            arr1.Add("03/15");
            #endregion

            #region Guest2_Setup_Ken_Masters

            #endregion

            #region Guest3_Setup_Terry_Bogard
            arr3.Add("Terry");
            arr3.Add("Bogard");
            arr3.Add("bogard@kof.com");
            arr3.Add("777-777-7777");
            arr3.Add("2000 Buster Wolf Ave NW");
            arr3.Add(null);
            arr3.Add("Kanto");
            arr3.Add("Tokyo");
            arr3.Add("R1U 0K3");
            arr3.Add("Japan");
            arr3.Add(BYTE_NO); //isVip
            arr3.Add(BYTE_NO); //isCheckedIn
            arr3.Add(new DateTime(2014, 4, 25)); //arrival
            arr3.Add(new DateTime(2014, 4, 27)); //departure
            arr3.Add("Mastercard");
            arr3.Add("5311111111111111");
            arr3.Add("Terry Bogard");
            arr3.Add("777");
            arr3.Add("07/17");
            #endregion

        }
        [TearDown]
        public void TearDown()
        {
            gm.DeleteGuest(arr1);
            gm = null;
            arr1 = null;
        }

        [Test]
        public void GuestFound()
        {
            int result = gm.SetGuest(arr1);
            GuestObj actualGuest = gm.GetGuest(arr1);
            Assert.AreEqual(arr1, actualGuest.GuestInfo);
            //gm.DeleteGuest(arr1);
        }

        [Test]
        public void GuestNotFound()
        {
            GuestObj actualGuest = gm.GetGuest(arr3);
            Assert.IsNull(actualGuest);
            //gm.DeleteGuest(arr1);
        }

    }
        
    
}
