﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace SmartManager{
    #region Guest Tests
    [TestFixture]
    public class DBHandlerGuestTest
    {
        DBHandler dbh;
        GuestObj guest1, guest2, guest3;

        #region Setup/Tear down
        /// <summary>
        /// Set up method
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            #region guest1 NOT ADDED
            ArrayList guestInfo1 = new ArrayList();
            guestInfo1.Add("Gary");
            guestInfo1.Add("Kang");
            guestInfo1.Add("GaryKang@k.com");
            guestInfo1.Add("555-342-5424");
            guestInfo1.Add("43 cow town 5 ave SE");
            guestInfo1.Add("yelow");
            guestInfo1.Add("Seoul");
            guestInfo1.Add("KR");
            guestInfo1.Add("10000");
            guestInfo1.Add("KOREA");
            guestInfo1.Add(0);
            guestInfo1.Add(1);
            guestInfo1.Add("01/04/2014 12:00:00 AM");
            guestInfo1.Add("03/04/2014 12:00:00 AM");
            guestInfo1.Add("VI");
            guestInfo1.Add("4199888899991234");
            guestInfo1.Add("Gary Kang");
            guestInfo1.Add("895");
            guestInfo1.Add("09/24");

            guest1 = new GuestObj(guestInfo1);

            #endregion

            #region guest2 ADDED
            ArrayList guestInfo2 = new ArrayList();
            guestInfo2.Add("Mega");
            guestInfo2.Add("Man");
            guestInfo2.Add("Megaman@blueguy.ca");
            guestInfo2.Add("555-342-5412");
            guestInfo2.Add("4da3 cow town 5 ave SE");
            guestInfo2.Add("yelaow");
            guestInfo2.Add("Seasdoul");
            guestInfo2.Add("KdR");
            guestInfo2.Add("100020");
            guestInfo2.Add("KOR1EA");
            guestInfo2.Add(0);
            guestInfo2.Add(0);
            guestInfo2.Add("01/05/2014 12:00:00 AM");
            guestInfo2.Add("06/12/2014 12:00:00 AM");
            guestInfo2.Add("VI");
            guestInfo2.Add("4199888899994321");
            guestInfo2.Add("Mega Man");
            guestInfo2.Add("835");
            guestInfo2.Add("09/14");

            guest2 = new GuestObj(guestInfo2);

            #endregion

            #region guest3 ADDED
            ArrayList guestInfo3 = new ArrayList();
            guestInfo3.Add("Vlien");
            guestInfo3.Add("Client");
            guestInfo3.Add("vcg@k.com");
            guestInfo3.Add("555-323-5424");
            guestInfo3.Add("sevae e SE");
            guestInfo3.Add("yelaaow");
            guestInfo3.Add("Seoadul");
            guestInfo3.Add("rrKR");
            guestInfo3.Add("23200");
            guestInfo3.Add("KOREAB");
            guestInfo3.Add(0);
            guestInfo3.Add(1);
            guestInfo3.Add("01/05/2014 12:00:00 AM");
            guestInfo3.Add("03/06/2014 12:00:00 AM");
            guestInfo3.Add("VI");
            guestInfo3.Add("419988123411234");
            guestInfo3.Add("VCMAN");
            guestInfo3.Add("895");
            guestInfo3.Add("09/11");

            guest3 = new GuestObj(guestInfo3);

            #endregion


            dbh = new DBHandler();
            dbh.AddGuest(guest2);
            dbh.AddGuest(guest3);
        }

        /// <summary>
        ///Tear down method
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            dbh.RemoveGuest(guest1);
            dbh.RemoveGuest(guest2);
            dbh.RemoveGuest(guest3);
            dbh = null;

            
        }
        #endregion

        
        /// <summary>
        /// Testing the getting Guest 
        /// </summary>
        [Test]
        public void TestingGetGuest()
        {
            
            GuestObj guest = dbh.GetGuest("Mega", "Man");
            Assert.AreEqual("Mega", guest.GuestInfo[0]);
            Assert.AreEqual("Man", guest.GuestInfo[1]);
        }

        /// <summary>
        /// Testing if guest is added correctly
        /// </summary>
        [Test]
        public void TestingAddGuest()
        {
            Assert.NotNull(dbh.AddGuest(guest1));
            
        }
        
        /// <summary>
        /// Testing the RemoveGuestMethod
        /// </summary>
        [Test]
        public void TestingRemoveGuest()
        {
            //wroks but dunno how to test it
            //Assert.NotNull(dbh.RemoveGuest(guest2));
        }

        /// <summary>
        /// Testing the Method Delete guest
        /// </summary>
        [Test]
        public void TestingDeleteGuest()
        {
            //check if the date time is within 1 minute
            Assert.That(DateTime.Now, Is.EqualTo(dbh.DeleteGuest(guest2)).Within(1).Minutes);
            
        }

        /// <summary>
        /// Testing the method EditGuest
        /// </summary>
        [Test]
        public void TestingEditGuest()
        {
            ArrayList newGuestInfo = new ArrayList();
            newGuestInfo.Add("Vlien");
            newGuestInfo.Add("Client");
            newGuestInfo.Add("vcg@k.com");
            newGuestInfo.Add("555-323-5424");
            newGuestInfo.Add("sevae e SE");
            newGuestInfo.Add("yelaaow");
            //CHANGE CITY
            newGuestInfo.Add("Edmonton");
            newGuestInfo.Add("rrKR");
            newGuestInfo.Add("23200");
            newGuestInfo.Add("KOREAB");
            newGuestInfo.Add(1);
            newGuestInfo.Add("01/05/2014 12:00:00 AM");
            newGuestInfo.Add("03/06/2014 12:00:00 AM");
            newGuestInfo.Add("VI");
            newGuestInfo.Add("419988123411234");
            newGuestInfo.Add("VCMAN");
            newGuestInfo.Add("895");
            newGuestInfo.Add("09/11");

            //FIX LATER
            /*
            //edits guest
            GuestObj guest = new GuestObj(newGuestInfo);
            
            dbh.EditGuest(guest);
            guest = dbh.GetGuest(guest.GuestInfo[0].ToString(), guest.GuestInfo[1].ToString());
            //check if guest is modified
            Assert.AreEqual("Edmonton", guest.GuestInfo[6].ToString());
            */
        }
        

    }
    #endregion

    #region Booking Tests
    [TestFixture]
    public class DBHandlerBookingTest
    {
        DBHandler dbh;
        BookingObj booking1, booking2;
        RoomObj room1, room2;

        #region SetUp and TearDown Methods
        [SetUp]
        public void SetUp()
        {
            #region guest1
            ArrayList guestInfo1 = new ArrayList();
            guestInfo1.Add("Gary");
            guestInfo1.Add("Kang");
            guestInfo1.Add("GaryKang@k.com");
            guestInfo1.Add("555-342-5424");
            guestInfo1.Add("43 cow town 5 ave SE");
            guestInfo1.Add("yelow");
            guestInfo1.Add("Seoul");
            guestInfo1.Add("KR");
            guestInfo1.Add("10000");
            guestInfo1.Add("KOREA");
            guestInfo1.Add(0);
            guestInfo1.Add(1);
            guestInfo1.Add("01/04/2014 12:00:00 AM");
            guestInfo1.Add("03/04/2014 12:00:00 AM");
            guestInfo1.Add("VI");
            guestInfo1.Add("4199888899991234");
            guestInfo1.Add("Gary Kang");
            guestInfo1.Add("895");
            guestInfo1.Add("09/24");

            GuestObj guest1 = new GuestObj(guestInfo1);

            #endregion
            #region roomobj for testing purpose
            //ADDING
            ArrayList roomInfo1 = new ArrayList();
            roomInfo1.Add(776);
            roomInfo1.Add("VIPSuit");
            roomInfo1.Add(777.00);

            room1 = new RoomObj(roomInfo1);

            //ADDED
            ArrayList roomInfo2 = new ArrayList();
            roomInfo2.Add(779);
            roomInfo2.Add("Double");
            roomInfo2.Add(100.0);

            room2 = new RoomObj(roomInfo2);
            #endregion
            //ADD
            ArrayList bookinginfo1 = new ArrayList();
            //We believe you used the wrong format... giggity
            //bookinginfo1.Add("11/03/2014 12:00:00 AM");
            //bookinginfo1.Add("14/03/2014 12:00:00 AM");
            bookinginfo1.Add(new DateTime(2014,3,11));
            bookinginfo1.Add(new DateTime(2014, 3, 14));
            bookinginfo1.Add(999.99);

            booking1 = new BookingObj(bookinginfo1);

            //UNADDED
            ArrayList bookinginfo2 = new ArrayList();
            //bookinginfo2.Add("12/03/2014 12:00:00 AM");
            //bookinginfo2.Add("15/03/2014 12:00:00 AM");
            bookinginfo1.Add(new DateTime(2014, 3, 12));
            bookinginfo1.Add(new DateTime(2014, 3, 15));
            bookinginfo2.Add(999.99);

            booking2 = new BookingObj(bookinginfo1);


            //invoice add to remove
            


            dbh = new DBHandler();
            dbh.AddRoom(room1);
            dbh.AddRoom(room2);
            dbh.AddBooking(booking1, 1, room1);
            //dbh.AddGuest(guest1);

        }

        [TearDown]
        public void TearDown()
        {
            //new inioice detail obj

            ArrayList invDet = new ArrayList();
            invDet.Add("Room Charges ");
            invDet.Add(999.99);
            invDet.Add(DateTime.Now.Date);
            invDet.Add(" Rack Rate");

            //Objects.InvoiceDetailsObj invDetObj = dbh.RemoveInvoiceDetails(1

            

            dbh.RemoveBooking(booking1, room1);
            dbh.RemoveBooking(booking2, room2);
            dbh.RemoveRoom(room1);
            dbh.RemoveRoom(room2);
            dbh = null;
            
        }
        #endregion

        /// <summary>
        /// simple add booking test
        /// </summary>
        [Test]
        public void AddBookingTest()
        {
            dbh.AddBooking(booking2, 1, room2);
        }

        //simple get booking test
        [Test]
        public void GetBookingTest()
        {
            GuestObj guest = dbh.GetGuest("Bill", "Gates");
            BookingObj booking = dbh.GetBooking(room1, guest);

            //Assert.AreEqual("11/03/2014 12:00:00 AM", booking.BookingInfo[0]);
            Assert.AreEqual(new DateTime(2014,3,11,12,0,0).AddHours(-12).ToString("G"), booking.BookingInfo[0]);
        }

        /// <summary>
        /// Simple edit Booking test
        /// </summary>
        [Test]
        public void EditBookingTest()
        {
            GuestObj guest = dbh.GetGuest("Bill", "Gates");
            BookingObj booking = dbh.GetBooking(room1, guest);

            booking.BookingInfo[2] = 70.00;
            dbh.EditBooking(booking, room1, guest);
            booking = dbh.GetBooking(room1, guest);
            Assert.AreEqual("70.00", booking.BookingInfo[2]);
            

        }

        //simple Delete booking test
        [Test]
        public void DeleteBookingTest()
        {
            GuestObj guest = dbh.GetGuest("Bill", "Gates");
            BookingObj booking = dbh.GetBooking(room1, guest);
            Assert.Pass();
            
        }


    }
    #endregion

    #region Room Tests
    [TestFixture]
    public class DBHandlerRoomTest
    {
        DBHandler dbh;
        RoomObj room1, room2;

        #region SetUp and TearDown Methods

        [SetUp]
        public void SetUp()
        {
            //added room for testing purpose
            ArrayList roomInfo1 = new ArrayList();
            roomInfo1.Add(777);
            roomInfo1.Add("VIPSuit");
            roomInfo1.Add(777.00);

            room1 = new RoomObj(roomInfo1);

            //unadded room for testing purpose
            ArrayList roomInfo2 = new ArrayList();
            roomInfo2.Add(778);
            roomInfo2.Add("Double");
            roomInfo2.Add(100.0);

            room2 = new RoomObj(roomInfo2);

            dbh = new DBHandler();
            dbh.AddRoom(room1);
        }

        [TearDown]
        public void TearDown()
        {
            dbh.RemoveRoom(room1);
            dbh.RemoveRoom(room2);
            dbh = null;
        }
        #endregion

        /// <summary>
        /// simple add room test
        /// </summary>
        [Test]
        public void AddRoomTest()
        {
            Assert.NotNull(dbh.AddRoom(room2));
        }

        /// <summary>
        /// simple get room test
        /// </summary>
        [Test]
        public void GetRoomtest()
        {
            RoomObj room = dbh.GetRoom(777, "VIPSuit");
            Assert.AreEqual(777, Convert.ToInt32(room.RoomInfo[0]));
            Assert.AreEqual(777.00, Convert.ToDecimal(room.RoomInfo[2]));
        }

        /// <summary>
        /// simple edit room test
        /// </summary>
        [Test]
        public void EditRoomTest()
        {
            RoomObj room = dbh.GetRoom(777, "VIPSuit");
            room.RoomInfo[2] = 25.50;

            dbh.EditRoom(room);
            Assert.AreEqual(777, Convert.ToInt32(room.RoomInfo[0]));
            Assert.AreEqual(25.50, Convert.ToDecimal((room.RoomInfo[2])));
        }

        /// <summary>
        /// simple delete room test
        /// </summary>
        [Test]
        public void DeleteRoomTest()
        {
            //fix the return type
            Assert.That(DateTime.Now, Is.EqualTo(dbh.DeleteRoom(room1)).Within(1).Minutes);
            
        }


    }
    #endregion

    //#region Invoice Tests
    //[TestFixture]
    //public class DBHandlerInvoiceTest
    //{
    //    DBHandler dbh;
    //    Objects.InvoiceObj inv1;
    //    GuestObj guest1;
        
    //    #region SetUp and TearDown Methods
    //    [SetUp]
    //    public void SetUp()
    //    {
    //        #region guest1
    //        ArrayList guestInfo1 = new ArrayList();
    //        guestInfo1.Add("Gary");
    //        guestInfo1.Add("Kang");
    //        guestInfo1.Add("GaryKang@k.com");
    //        guestInfo1.Add("555-342-5424");
    //        guestInfo1.Add("43 cow town 5 ave SE");
    //        guestInfo1.Add("yelow");
    //        guestInfo1.Add("Seoul");
    //        guestInfo1.Add("KR");
    //        guestInfo1.Add("10000");
    //        guestInfo1.Add("KOREA");
    //        guestInfo1.Add(0);
    //        guestInfo1.Add(1);
    //        guestInfo1.Add("01/04/2014 12:00:00 AM");
    //        guestInfo1.Add("03/04/2014 12:00:00 AM");
    //        guestInfo1.Add("VI");
    //        guestInfo1.Add("4199888899991234");
    //        guestInfo1.Add("Gary Kang");
    //        guestInfo1.Add("895");
    //        guestInfo1.Add("09/24");

    //        guest1 = new GuestObj(guestInfo1);
    //        #endregion
    //        dbh = new DBHandler();
    //        ArrayList inv1detail = new ArrayList();
    //        inv1detail.Add(777);
    //        inv1detail.Add(777);
    //        inv1detail.Add(90.0);
    //        inv1detail.Add(5.4);
    //        inv1detail.Add(852.32);
           
    //        inv1 = new Objects.InvoiceObj(inv1detail);


    //        dbh.AddGuest(guest1);
            
    //    }

    //    [TearDown]
    //    public void TearDown()
    //    {
    //        dbh.RemoveInvoice("Gary", "Kang");
    //        dbh.RemoveGuest(guest1);
    //        dbh = null;

    //    }

    //    /// <summary>
    //    /// testing the add invoice methods
    //    /// </summary>
    //    [Test]
    //    public void AddInvoiceTest()
    //    {
    //        dbh.AddInvoice(inv1, guest1);
    //        Objects.InvoiceObj invoice = dbh.GetInvoice("Bill", "Gates");

    //        Assert.NotNull(invoice);
    //    }

    //    /// <summary>
    //    /// testing the get invoice test
    //    /// </summary>
    //    [Test]
    //    public void GetInvoiceTest()
    //    {
           
    //        Objects.InvoiceObj invoice = dbh.GetInvoice("Bill", "Gates");

    //        Assert.NotNull(invoice);
    //    }
    //    #endregion

        


    //}
    //#endregion

    #region Service Tests
    [TestFixture]
    public class DBHandlerServiceTest
    {
        DBHandler dbh;
        ServiceObj service1, service2;

        #region SetUp and TearDown Methods
        [SetUp]
        public void SetUp()
        {
            //added
            ArrayList service1Info = new ArrayList();
            service1Info.Add("Candy");
            service1Info.Add("Food");
            service1Info.Add(123.45);

            service1 = new ServiceObj(service1Info);

            //unadded
            ArrayList service2Info = new ArrayList();
            service2Info.Add("Clean up");
            service2Info.Add("Clean");
            service2Info.Add(555.55);

            service2 = new ServiceObj(service2Info);


            dbh = new DBHandler();

            dbh.AddService(service1);
        }

        [TearDown]
        public void TearDown()
        {
            dbh.RemoveService(service1);
            dbh.RemoveService(service2);
            dbh = null;
        }
        #endregion

        /// <summary>
        /// TEst the Add services
        /// </summary>
        [Test]
        public void AddServiceTest()
        {


            Assert.NotNull(dbh.AddService(service2));

        }

        //Test the Get Services
        [Test]
        public void GetServiceTest()
        {
            ServiceObj service = dbh.GetService("Candy");
            Assert.AreEqual("Candy", service.ServiceInfo[0].ToString());
            Assert.AreEqual("Food", service.ServiceInfo[1].ToString());


        }
        /// <summary>
        /// TEsting the edit service Test
        /// </summary>
        [Test]
        public void EditServiceTest()
        {
            ServiceObj service = dbh.GetService("Candy");
            service.ServiceInfo[1] = "Sugar";
            dbh.EditService(service);

            ServiceObj newService = dbh.GetService("Candy");

            Assert.AreEqual("Sugar", newService.ServiceInfo[1]);

        }

        //test the delete services test
        [Test]
        public void DeleteServiceTest()
        {
            //FIX IN DBHANDLER
            //Assert.That(DateTime.Now, Is.EqualTo(dbh.DeleteService(service1)).Within(1).Minutes);
        }

        [Test]
        public void RemoveServiceTest()
        {
            //TODO
        }

    }
    #endregion


}
