﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager.Objects
{
    partial class InvoiceObj
    {
        #region Instance and Class Variables
        private ArrayList invoiceInfo;
        private int invoiceID;
        #endregion

        #region Constructors and Main Methods

        public InvoiceObj(ArrayList info)
        {
            this.invoiceInfo = new ArrayList();
            InvoiceInfo = info;
        }

        
        public InvoiceObj(int invoiceID, ArrayList invoiceInfo)
        {
            this.invoiceInfo = new ArrayList();
            InvoiceID = invoiceID;
            InvoiceInfo = invoiceInfo;
        }
        #endregion 

        #region Properties and Get/Set Methods

        public int InvoiceID
        {
            get
            {
                return this.invoiceID;
            }
            set
            {
                invoiceID = value;
            }
        }

        // the array list will look like the following:
        // { invoice number, subtotal, tax, total, Arraylist of InvoiceDetailsObj }
        public ArrayList InvoiceInfo
        {
            get
            {
                return this.invoiceInfo;
            }
            set
            {
                invoiceInfo = value;
            }
        }

        #endregion
    }
}
