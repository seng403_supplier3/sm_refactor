﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace SmartManager
{
    class BookingObj
    {
        #region Instance and Class Variables

        public enum Status { NotCheckedIn, CheckedIn };

        private Status bookStatus;
        private ArrayList bookingInfo;
        private int bookingID;

        #endregion


        #region Constructors and Main Methods

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="BookingInfo"></param>
        public BookingObj(int bookingID, ArrayList info)
        {
            this.bookingInfo = new ArrayList();
            this.BookingInfo = info;
        }

        //sample constructor to fix compilation errors
        public BookingObj(ArrayList bookingInfo)
        {
            this.BookingInfo = bookingInfo;
        }

        #endregion


        #region Properties and Get/Set Methods

        /// <summary>
        /// BookingID Property
        /// </summary>
        public int BookingID
        {
            get
            { return this.bookingID; }
            set
            { this.bookingID = value; }
        }

        /// <summary>
        /// BookingInformationProperty
        /// Expects the following ArrayList format(ordering of items in list matters!):
        /// [startBookDate, endBookDate, billingRoomRate]
        /// </summary>
        public ArrayList BookingInfo
        {
            get
            { return this.bookingInfo; }
            set
            { this.bookingInfo = value; }
        }

        /// <summary>
        /// BookingStatus Property
        /// </summary>
        public Status BookingStatus
        {
            get
            { return this.bookStatus; }
            set
            { this.bookStatus = value; }
        }

        #endregion
    }
}
