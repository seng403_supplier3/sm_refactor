﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager.Objects
{
    partial class InvoiceDetailsObj
    {
        #region Instance and Class Variables
        private ArrayList invoiceDetailsInfo;
        private int invoiceDetailsID;
        #endregion

        #region Constructors and Main Methods

        public InvoiceDetailsObj(ArrayList info)
        {
            this.invoiceDetailsInfo = new ArrayList();
            InvoiceDetailsInfo = info;
        }

        public InvoiceDetailsObj(int invoiceDetailsID, ArrayList invoiceDetailsInfo)
        {
            this.invoiceDetailsInfo = new ArrayList();
            InvoiceDetailsID = invoiceDetailsID;
            InvoiceDetailsInfo = invoiceDetailsInfo;
        }
        #endregion 

        #region Properties and Get/Set Methods

        public int InvoiceDetailsID
        {
            get
            {
                return this.invoiceDetailsID;
            }
            set
            {
                invoiceDetailsID = value;
            }
        }

        // the array list will look like the following:
        // { ServiceType, Price Charge, Date, Details }
        public ArrayList InvoiceDetailsInfo
        {
            get
            {
                return this.invoiceDetailsInfo;
            }
            set
            {
                invoiceDetailsInfo = value;
            }
        }

        #endregion
    }
}
