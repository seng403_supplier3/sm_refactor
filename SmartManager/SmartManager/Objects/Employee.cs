﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager
{
    public class EmployeeObj
    {
        #region Instance and Class Variables

        public enum Status { Regular, Manager };

        private Status employeeStatus; //Regular or Manager
        private ArrayList employeeInfo;
        private int employeeID;

        #endregion


        #region Constructor and Main Methods

        /// <summary>
        /// Constructor for employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="EmployeeInfo"></param>
        public EmployeeObj(int employeeID, ArrayList info)
        {
            this.EmployeeInfo = new ArrayList();
            this.EmployeeInfo = info;
            this.EmployeeID = employeeID;
        }

        //sample constructor to fix compilation errors
        public EmployeeObj(ArrayList employeeInfo)
        {
            this.EmployeeInfo = employeeInfo;
        }

        #endregion


        #region Properties and Get/Set Methods

        /// <summary>
        /// EmployeeID Property
        /// </summary>
        public int EmployeeID
        {
            get
            { return this.employeeID; }
            set
            { this.employeeID = value; }
        }

        /// <summary>
        /// EmployeeInformationProperty
        /// Expects the following ArrayList format(ordering of items in list matters!):
        /// { firstName, lastName, empType, empNum/ID, pass, isLoggedin }
        /// </summary>
        public ArrayList EmployeeInfo
        {
            get
            { return this.employeeInfo; }
            set
            { this.employeeInfo = value; }
        }

        /// <summary>
        /// EmployeeStatus Property
        /// </summary>
        public Status EmployeeStatus
        {
            get
            { return this.employeeStatus; }
            set
            { this.employeeStatus = value; }
        }

        #endregion
    }
}
