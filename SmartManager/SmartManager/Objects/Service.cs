﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager
{
    partial class ServiceObj
    {
        #region Instance and Class Variables

        private ArrayList serviceInfo;
        private int serviceID;

        #endregion

        #region Constructor and Main Methods
        public ServiceObj(ArrayList serviceInfo)
        {
            this.serviceInfo = serviceInfo;
        }

        public ServiceObj(int serviceID, ArrayList serviceInfo)
        {
            this.serviceInfo = serviceInfo;
            this.serviceID = serviceID;
        }
        #endregion 

        #region Propertiesid and Get/Set Methods
        /// <summary>
        /// serviceID property
        /// </summary>
        public int Service
        {
            get
            {
                return this.serviceID;
            }
            set
            {
                this.serviceID = value;
            }
        }

        /// <summary>
        /// Serivce Info Property
        /// </summary>
        public ArrayList ServiceInfo
        {
            get
            {
                return this.serviceInfo;
            }
            set
            {
                this.serviceInfo = value;
            }
        }
        #endregion

    }
}
