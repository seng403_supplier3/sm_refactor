﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace SmartManager
{
    partial class GuestObj
    {
        #region Instance and Class Variables

        public enum GuestType { Regular, Vip };

        private int guestID;
        private GuestType guestType;
        private ArrayList guestInfo;
        
        #endregion


        #region Constructors and Main Methods

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="guestInfo"></param>
        public GuestObj(ArrayList guestInfo)
        {
            this.guestInfo = new ArrayList();
            this.GuestInfo = guestInfo;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="guestID"></param>
        /// <param name="guestInfo"></param>
        public GuestObj(int guestID, ArrayList guestInfo)
        {
            this.guestInfo = new ArrayList();
            this.guestID = guestID;
            this.GuestInfo = guestInfo;
        }
        #endregion


        #region Properties and Get/Set Methods
        
        /// <summary>
        /// GuestID Property
        /// </summary>
        public int GuestIDInt
        {
            get
            { return this.guestID; }
            set
            { this.guestID = value; }
        }

        /// <summary>
        /// GuestType Property
        /// </summary>
        public GuestType Type
        {
            get
            { return this.guestType; }
            set
            { this.guestType = value; }
        }

        /// <summary>
        /// Guest Information Property
        /// Expects the following ArrayList format(ordering of items in list matters!):
        /// [firstName, lastName, email, phoneNumber, 
        /// address1, address2, city, stateProv, 
        /// zipPostalCode, country,
        /// IsVIP, departureDate, billingCardType, 
        /// billingCardNum, billingCardHolderName, 
        /// billingCardSecurityNum, billingCardExpiration]
        /// </summary>
        public ArrayList GuestInfo
        {
            get
            { return this.guestInfo; }
            set
            {
                this.guestInfo = value;
                
                // set Guest Type (Regular/VIP)
                
                if (Convert.ToSByte(this.guestInfo[10]) == 0)
                    this.Type = GuestType.Vip;
                else
                    this.Type = GuestType.Regular;
            }
        }

        #endregion
    }
}
