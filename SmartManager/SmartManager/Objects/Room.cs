﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager
{
    partial class RoomObj
    {
        #region Instance and Class Variables

        public enum RoomStatus { Occupied, Available };
        public enum RoomType { Single, Double, Twin, TwinDouble, VIPSuit };

        private int roomID;
        private RoomStatus status;
        private ArrayList roomInfo;

        private int guestID;

        #endregion


        #region Constructor and Main Methods

        public RoomObj(ArrayList info)
        {
            this.roomInfo = new ArrayList();
            this.RoomInfo = info;
            this.status = RoomStatus.Available; // default setting

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomID"></param>
        /// <param name="roomInfo"></param>
        public RoomObj(int id, ArrayList info)
        {
            this.roomInfo = new ArrayList();
            this.RoomIDInt = id;
            this.RoomInfo = info;
            this.status = RoomStatus.Available; // default setting
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomNum"></param>
        /// <param name="roomInfo"></param>
        /// <param name="guestID"></param>
        public RoomObj(int roomID, ArrayList roomInfo, int guestID)
        {
            this.roomInfo = new ArrayList();
            this.RoomIDInt = roomID;
            this.RoomInfo = roomInfo;
            this.status = RoomStatus.Available; // default setting
            this.GuestID = guestID;
        }

        #endregion


        #region Properties and Get/Set Methods

        /// <summary>
        /// RoomNumber Property
        /// </summary>
        public int RoomIDInt
        {
            get
            { return this.roomID; }
            set
            { this.roomID = value; }
        }
        
        /// <summary>
        /// StatusInformation Property
        /// </summary>
        public RoomStatus StatusInfo
        {
            get
            { return this.status; }
            set
            { this.status = value; }
        }

        /// <summary>
        /// RoomInformation Property
        /// Expects the following ArrayList format(ordering of items in list matters!):
        /// [roomNum, roomType, rackRate]
        /// </summary>
        public ArrayList RoomInfo
        {
            get
            { return this.roomInfo; }
            set
            { this.roomInfo = value; }
        }

        /// <summary>
        /// GuestID Property
        /// </summary>
        public int GuestID
        {
            get
            { return this.guestID; }
            set
            { this.guestID = value; }
        }

        #endregion
    }
}
