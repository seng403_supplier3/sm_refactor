﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartManager.Objects;
using System.Collections;

namespace SmartManager
{
    interface IDBHandler
    {
        #region Booking Related Methods

        void AddBooking(BookingObj bookingObj, int guestID, RoomObj roomObj);
        BookingObj GetBooking(RoomObj roomObj, GuestObj guestObj);
        ArrayList[] GetCurrBookings();
        void EditBooking(BookingObj bookingObj, RoomObj roomObj, GuestObj guestObj);
        void DeleteBooking(BookingObj bookingObj, RoomObj roomObj, GuestObj guestObj);
        void DeleteBooking(BookingObj bookingObj, GuestObj guestObj);
        void RemoveBooking(BookingObj bookingObj, RoomObj roomObj);
        int GetBookingID(BookingObj bookingObj);

        // TODO getBookingBy[Filter]
        #endregion


        #region Guest Related Methods

        int AddGuest(GuestObj guestObj);
        GuestObj GetGuest(String firstName, String lastName);
        void EditGuest(GuestObj guestObj); // Two guest objects for query (?)
        DateTime DeleteGuest(GuestObj guestObj);
        void RemoveGuest(GuestObj guestObj);
        void CheckInGuest(BookingObj bookingObj, GuestObj guestObj);
        void CheckOutGuest(BookingObj bookingObj, GuestObj guestObj);
        List<GuestObj> SearchGuests(String[] names);

        #endregion


        #region Invoice Related Methods
        void AddInvoice(InvoiceObj invoiceObj, BookingObj bookingObj, GuestObj guestObj);
        InvoiceObj GetInvoice(BookingObj booking, GuestObj guest);
        void AddInvoiceDetails(InvoiceObj invoiceObj, InvoiceDetailsObj invoiceDetailsObj);
        void EditInvoice(BookingObj booking, GuestObj guest, InvoiceObj invoice);
        void RemoveInvoiceDetails(InvoiceDetailsObj invoiceDetObj);
        List<InvoiceDetailsObj> GetInvoiceDetails();
        #endregion 


        #region Room RelatedMethods

        int AddRoom(RoomObj roomObj);
        RoomObj GetRoom(int roomNum, String roomType);
        void EditRoom(RoomObj roomObj);
        DateTime DeleteRoom(RoomObj roomObj);
        void RemoveRoom(RoomObj roomObj);
        List<RoomObj> GetAvailRooms(DateTime startDate, DateTime endDate);
        List<RoomObj> GetOccupiedRooms(DateTime startDate, DateTime endDate);

        #endregion


        #region Service Related methods
        int AddService(ServiceObj serviceInfo);
        ServiceObj GetService(String serviceName);
        void EditService(ServiceObj serviceObj);
        void DeleteService(ServiceObj serviceObj);
        void RemoveService(ServiceObj serviceObj);
        List<ServiceObj> GetAllAvailServices();
        List<ServiceObj> GetAllTypeServices(String type);
        #endregion


        #region Employee Related Methods
        int AddEmployee(EmployeeObj employeeObj);
        EmployeeObj GetEmployee(int employeeNumber);
        List<EmployeeObj> GetAllEmployees();
        void EditEmployee(int employeeNumber, EmployeeObj employee);
        void DeleteEmployee(int employeeNumber);
        int LogInEmployee(int employeeNumber, String password);
        void LogOutEmployee(int employeeNumber, EmployeeObj employeeObj);
        List<EmployeeObj> SearchEmployees(String[] names);
        #endregion
    }
}
