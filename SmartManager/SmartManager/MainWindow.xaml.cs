﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SmartManager.UserControls;
using SmartManager.Functionality;
using SmartManager.Objects;
using System.Collections;

namespace SmartManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Instances
        public MainBookingDash mainBookingDash;
        public BookingForm bookingForm;
        public BookingList bookingList;

        public TotalSalesGraph totalSales; 

        public NewFeedList newsFeedList;

        public ServicesList servicesList;
        public Services_ChargesList chargesList;
        public Services_MiscList miscList;

        public SearchResults searchRes;
        public SearchEmployeeResults searchEmpRes;

        private EmployeeManager empMan;
        public EmployeeObj loggedInEmp; // currently logged in Employee
        public int empID = 0; // id of loggedinEmp

        public int invoiceSelected;

        public EmployeeForm empForm;
        public ChangePasswordForm passwordForm;
        public EmployeeList empList;

        #endregion


        #region Main
        public MainWindow()
        {
            InitializeComponent();

            InitializeGUI();

            this.WindowState = WindowState.Maximized;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
        }

        private void InitializeGUI()
        {
            #region Set class variables
            // hotelBookingButton parts of the GUI
            this.mainBookingDash = new MainBookingDash(this);
            this.bookingForm = new BookingForm(this);
            this.bookingList = new BookingList(this);

            this.totalSales = new TotalSalesGraph(this);

            // newsFeed parts of the GUI
            this.newsFeedList = new NewFeedList(this);

            // roomCharges part of the GUI
            this.servicesList = new ServicesList(this);
            this.chargesList = new Services_ChargesList(this);
            this.miscList = new Services_MiscList(this);

            //search results part of the GUI
            this.searchRes = new SearchResults(this);
            this.searchEmpRes = new SearchEmployeeResults(this);

            // emplyee part of the GUI
            this.empMan = new EmployeeManager();
            this.empForm = new EmployeeForm(this);
            this.passwordForm = new ChangePasswordForm(this);
            this.empList = new EmployeeList(this);

            #endregion

            #region Add children to GUI
            // hotelBookingButton
            this.dashboard.Children.Add(this.mainBookingDash);
            this.dashboard.Children.Add(this.bookingList);
            this.dashboard.Children.Add(this.bookingForm);

            this.dashboard.Children.Add(this.totalSales);

            // newsFeed
            this.liveFeed.Children.Add(this.newsFeedList);
            this.liveFeed.Visibility = Visibility.Visible;

            // roomCharges
            this.dashboard.Children.Add(this.servicesList);
            this.dashboard.Children.Add(this.chargesList);
            this.dashboard.Children.Add(this.miscList);

            // search
            this.dashboard.Children.Add(this.searchRes);
            this.dashboard.Children.Add(this.searchEmpRes);

            // employee
            this.dashboard.Children.Add(this.empForm);
            this.dashboard.Children.Add(this.passwordForm);
            this.dashboard.Children.Add(this.empList);
            #endregion

            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.mainBookingDash.Visibility = Visibility.Visible;


        }
        #endregion


        #region Menu Bar Buttons: HotelBooking, RoomCharges & HouseKeeping
        private void hotelBookingButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.mainBookingDash.Visibility = Visibility.Visible;
        }

        private void roomChargesButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.servicesList.Visibility = Visibility.Visible;
        }

        //TODO housekeeping buttonclicked
        private void houseKeepingButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.totalSales.Visibility = Visibility.Visible;
        }
        #endregion


        #region Menu Bar Search
        //Clear text box on focus
        private void menuSearchTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            menuSearchTextBox.Clear();
        }

        private void byEmployee_Selected(object sender, RoutedEventArgs e)
        {
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.searchEmpRes.Visibility = Visibility.Visible;
            this.searchEmpRes.GetSearchString(this.menuSearchTextBox.Text);
            this.menuSearchTextBox.Text = "Search keyword..."; // clear search textbox

            this.comboSearch.IsEditable = true;
            this.comboSearch.IsReadOnly = true;
            this.comboSearch.Text = "(Select Search Option)";
            // comboSearch.SelectedIndex = -1;
        }

        private void byGuest_Selected(object sender, RoutedEventArgs e)
        {
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.searchRes.Visibility = Visibility.Visible;
            this.searchRes.GetSearchString(this.menuSearchTextBox.Text);
            this.menuSearchTextBox.Text = "Search keyword..."; // clear search textbox

            this.comboSearch.IsEditable = true;
            this.comboSearch.IsReadOnly = true;
            this.comboSearch.Text = "(Select Search Option)";
            // comboSearch.SelectedIndex = -1;

        }

        private void comboSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //clears combosearch after selection get started
            comboSearch.SelectedIndex = -1;
        }
        #endregion


        #region Login/Logout
        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = false;
            empID = Convert.ToInt32(this.empIDTextBox.Text);
            string password = this.empPasswordTextbox.Password.ToString();


            // check for credentials validity
            if (this.empMan.LogInEmployee(empID, password) == 1)
                isValid = true;

            if (isValid)
            {
                this.loginGrid.Visibility = Visibility.Hidden; // this works, no need for zindex

                ArrayList info = this.empMan.GetEmployee(empID).EmployeeInfo;
                this.loggedInEmp = new EmployeeObj(info);

                this.empIDTextBox.Text = "";
                this.empPasswordTextbox.Password = "";

                if (this.loggedInEmp.EmployeeInfo[2].ToString() == "Manager")
                {
                    this.managerMenu.Visibility = Visibility.Visible;
                    this.menuHeader.Header = String.Format("{0} {1} {2}", "Logged in as: ", info[0], info[1]);
                }
                else
                    this.menuHeader2.Header = String.Format("{0} {1} {2}", "Logged in as: ", info[0], info[1]);
            }
            else
                MessageBox.Show("The employee ID and/or password is incorrect. Please try again.", "Login Error", MessageBoxButton.OK);
        }

        private void logoutMenu_Click(object sender, RoutedEventArgs e)
        {
            this.empMan.LogOutEmployee(empID, this.loggedInEmp);
            this.loggedInEmp = null;

            this.loginGrid.Visibility = Visibility.Visible;
            this.managerMenu.Visibility = Visibility.Hidden;
            empID = 0;
        }
        #endregion


        #region User Account Menu Items
        private void changePassMenu_Click(object sender, RoutedEventArgs e)
        {
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.passwordForm.Visibility = Visibility.Visible;
        }

        private void addEmpMenu_Click(object sender, RoutedEventArgs e)
        {
            this.empForm.editFlag = false;

            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.empForm.Visibility = Visibility.Visible;
        }

        private void editEmpMenu_Click(object sender, RoutedEventArgs e)
        {
            this.empList.deleteFlag = false;
            this.empForm.editFlag = true;

            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.empList.Visibility = Visibility.Visible;
        }

        private void deleteEmpMenu_Click(object sender, RoutedEventArgs e)
        {
            this.empList.deleteFlag = true;
           
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.empList.Visibility = Visibility.Visible;
        }
        #endregion



        #region old
        //TODO serach button clicked
        /*
        private void menuSearchButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (UserControl c in this.dashboard.Children)
            {
                c.Visibility = Visibility.Hidden;
            }

            this.searchRes.Visibility = Visibility.Visible;
            this.searchRes.GetSearchString(this.menuSearchTextBox.Text);
            this.menuSearchTextBox.Text = "Searck keyword..."; // clear search textbox
        }
         * */
        #endregion
    }
}
