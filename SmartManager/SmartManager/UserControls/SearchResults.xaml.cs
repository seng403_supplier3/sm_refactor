﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for SearchResults.xaml
    /// </summary>
    public partial class SearchResults : UserControl
    {

        MainWindow mainWindow;
        string searchStr;

        GuestManager guestMan;

        public SearchResults(MainWindow p)
        {
            InitializeComponent();
            this.mainWindow = p;

            this.guestMan = new GuestManager();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            this.mainWindow.mainBookingDash.Visibility = Visibility.Visible;

            //this.searchResultsTitleText.Content = this.searchResultsTitleText.Content.ToString().Split(':')[0];

        }

        public void GetSearchString(string s)
        {
            this.searchStr = s;

            //this.searchResultsTitleText.Content += " " + this.searchStr;

            PopulateList();
        }

        public void PopulateList()
        {
            this.listPanel.Children.Clear();

            String[] names = this.searchStr.Split(' ');
            List<ArrayList> guestList = this.guestMan.searchGuests(names);

            foreach (ArrayList guest in guestList)
            {
                Button button = new Button();
                button.Content = guest[0] + " " + guest[1];
                button.Height = 100;
                button.FontSize = 16;

                this.listPanel.Children.Add(button);
                this.cancelButton.Click += new RoutedEventHandler(cancelButton_Click);
            }
        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // do something here
        }
    }
}
