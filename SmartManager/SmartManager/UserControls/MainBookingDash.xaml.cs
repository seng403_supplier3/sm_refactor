﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for MainBookingDash.xaml
    /// </summary>
    public partial class MainBookingDash : UserControl
    {
        MainWindow mainWindow;
        public MainBookingDash(MainWindow p)
        {
            InitializeComponent();
            this.mainWindow = p;
        }

        //TODO ADD BOOKING CLICKED
        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.bookingForm.editFlag = false;
            this.mainWindow.bookingForm.Visibility = Visibility.Visible;
        }

        //TODO EDIT BOOKING CLICKED
        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.bookingForm.editFlag = true;
            this.mainWindow.bookingList.cancelFlag = false;
            this.mainWindow.bookingList.Visibility = Visibility.Visible;
        }

        //TODO CANCEL BOOING CLICKED
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.bookingList.cancelFlag = true;
            this.mainWindow.bookingList.Visibility = Visibility.Visible;
            Canvas.SetZIndex(this.mainWindow.bookingList, 6);
        }

        public void PopulateFormFields()
        {
            // show form for editting
            this.mainWindow.bookingForm.Visibility = Visibility.Visible;

            // fill in all the text box fields with the guest and booking info
        }
    }
}
