﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for Services_CableInternetList.xaml
    /// </summary>
    public partial class Services_ChargesList : UserControl
    {
        MainWindow mainWindow;
       // private List<Expander> expanderList;


        private List<Button> buttonList;

        //private RoomObj selectedRoom;
        private Functionality.ServiceManager servMan;
        private Functionality.InvoiceManager invMan;
        private BookingManager bookMan;
        private int invoiceID;

        public Services_ChargesList(MainWindow p)
        {
            InitializeComponent();
            this.mainWindow = p;

            servMan = new Functionality.ServiceManager();
            invMan = new Functionality.InvoiceManager();
            bookMan = new BookingManager();

            
        }

        #region Button Functionality

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        //private void SaveButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Visibility = Visibility.Hidden;

        //    // TODO update invoice of room with selected service
        //}

        #endregion


        #region Populating Different Lists
        /// <summary>
        /// Populates the meal list with a list of services of type "Meal Charges"
        /// </summary>
        private void PopulateMealList()
        {
            this.chargesListPanel.Children.Clear();

            buttonList = new List<Button>();
            List<ServiceObj> services = servMan.GetAllSType("Meal Charge");

            foreach (ServiceObj s in services)
            {
                ArrayList info = s.ServiceInfo;
                ArrayList invoiceInfo = new ArrayList();
                Button svBut = new Button();
                
                //sets the event handler for button clik
                svBut.Click += (sender, e) => svBut_Click(sender, e, info);

                //set up the name of the button
                svBut.Content = "Name: " + info[0] + "\nType: " +
                    info[1] + "\nCost: " + info[2];
                svBut.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                svBut.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));

                
                invoiceInfo.Add(DateTime.Now.Date);
                invoiceInfo.Add(info[0]); // TODO change the description
                invoiceInfo.Add(info[2]); // charge

                //adds the button onto panel
                this.buttonList.Add(svBut);
                this.chargesListPanel.Children.Add(svBut);

                SetButtonProperties();
                
            }
        }

        void svBut_Click(object sender, RoutedEventArgs e, ArrayList serviceInfo)
        {
            string str = "Do you want to add:\n" +
                     "\nService:  " + serviceInfo[1].ToString() + " - " + serviceInfo[0].ToString() +
                     "\nCost:  " + serviceInfo[2];
            MessageBoxResult result = MessageBox.Show(str, "Add Service Charge", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {

                ArrayList[] bookings = bookMan.GetCurrBookings();
                ArrayList bookingInfo = bookings[0][mainWindow.invoiceSelected] as ArrayList;
                ArrayList guestInfo = bookings[1][mainWindow.invoiceSelected] as ArrayList;

                //grabs id
                Objects.InvoiceObj invoice = invMan.GetInvoice(bookingInfo, guestInfo);


                ArrayList invoiceDetail = new ArrayList();
                invoiceDetail.Add(serviceInfo[1].ToString() + " - " + serviceInfo[0].ToString());
                invoiceDetail.Add(serviceInfo[2]);

                invMan.AddInvoiceDetail(invoice.InvoiceInfo, invoiceDetail);
            }
            
            
            


        }
        #region oldcode
        /*
        void svExp_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // ROOM SELECTED from ROOM LIST
            Expander selection = sender as Expander;

            // deselect any previous selections
            foreach (Expander exp in this.expanderList)
            {
                if (exp == sender as Expander)
                {
                    int index = this.expanderList.IndexOf(exp);
                    // show confirmation window
                    string info = exp.Content.ToString();

                    MessageBoxResult result = MessageBox.Show("Add the following service? \n" + info, "Confirm Service Charge", MessageBoxButton.YesNo);

                    if (result == MessageBoxResult.Yes)
                    {
                        //TODO add to invoice
                        // servMan.setService(info);
                        //invMan.AddInvoiceDetail(invoiceInfo, this.selectedRoom.RoomInfo);
                        

                        break;
                    }
                }
            }
            }
        */
        #endregion
        private void PopulateCableList()
        {

            this.chargesListPanel.Children.Clear();

            buttonList = new List<Button>();
            List<ServiceObj> services = servMan.GetAllSType("Cable/Internet");

            foreach (ServiceObj s in services)
            {
                ArrayList info = s.ServiceInfo;
                ArrayList invoiceInfo = new ArrayList();
                Button svBut = new Button();

                //sets the event handler for button clik
                svBut.Click += (sender, e) => svBut_Click(sender, e, info);

                //set up the name of the button
                svBut.Content = "Name: " + info[0] + "\nType: " +
                    info[1] + "\nCost: " + info[2];

                svBut.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                svBut.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
                invoiceInfo.Add(DateTime.Now.Date);
                invoiceInfo.Add(info[0]); // TODO change the description
                invoiceInfo.Add(info[2]); // charge

                //adds the button onto panel
                this.buttonList.Add(svBut);
                this.chargesListPanel.Children.Add(svBut);

                SetButtonProperties();

            }

            #region oldcode
            /*
            Functionality.ServiceManager servMan = new Functionality.ServiceManager();
            // RoomManager rmMan = new RoomManager();
            expanderList = new List<Expander>();

            this.chargesListPanel.Children.Clear();


            List<ServiceObj> services = servMan.GetAllSType("Cable/Internet");


            foreach (ServiceObj s in services)
            {
                ArrayList info = s.ServiceInfo;
                Expander rmExp = new Expander();
                rmExp.MouseDoubleClick += new MouseButtonEventHandler(svExp_MouseDoubleClick);

                rmExp.Content = "Name " + info[0] + "\tType: " +
                    info[1] + "\tCost: " + info[2];

                this.expanderList.Add(rmExp);
                this.chargesListPanel.Children.Add(rmExp);

                SetButtonProperties();
            }
             */
            #endregion
        }

        private void PopulateMiniBarList()
        {

            this.chargesListPanel.Children.Clear();

            buttonList = new List<Button>();
            List<ServiceObj> services = servMan.GetAllSType("Minibar");

            foreach (ServiceObj s in services)
            {
                ArrayList info = s.ServiceInfo;
                ArrayList invoiceInfo = new ArrayList();
                Button svBut = new Button();

                //sets the event handler for button clik
                svBut.Click += (sender, e) => svBut_Click(sender, e, info);

                //set up the name of the button
                svBut.Content = "Name: " + info[0] + "\nType: " +
                    info[1] + "\nCost: " + info[2];

                svBut.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                svBut.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
                invoiceInfo.Add(DateTime.Now.Date);
                invoiceInfo.Add(info[0]); // TODO change the description
                invoiceInfo.Add(info[2]); // charge

                //adds the button onto panel
                this.buttonList.Add(svBut);
                this.chargesListPanel.Children.Add(svBut);

                SetButtonProperties();

            }
            #region old code
            /*
            Functionality.ServiceManager servMan = new Functionality.ServiceManager();
            // RoomManager rmMan = new RoomManager();
            expanderList = new List<Expander>();

            this.chargesListPanel.Children.Clear();


            List<ServiceObj> services = servMan.GetAllSType("Mini-Bar");


            foreach (ServiceObj s in services)
            {
                ArrayList info = s.ServiceInfo;
                Expander rmExp = new Expander();
                rmExp.MouseDoubleClick += new MouseButtonEventHandler(svExp_MouseDoubleClick);

                rmExp.Content = "Name " + info[0] + "\tType: " +
                    info[1] + "\tCost: " + info[2];

                this.expanderList.Add(rmExp);
                this.chargesListPanel.Children.Add(rmExp);

                SetButtonProperties();
            }*/
            #endregion
        }

        private void PopulateDamagesList()
        {

            this.chargesListPanel.Children.Clear();

            buttonList = new List<Button>();
            List<ServiceObj> services = servMan.GetAllSType("Damages");

            foreach (ServiceObj s in services)
            {
                ArrayList info = s.ServiceInfo;
                ArrayList invoiceInfo = new ArrayList();
                Button svBut = new Button();

                //sets the event handler for button clik
                svBut.Click += (sender, e) => svBut_Click(sender, e, info);

                //set up the name of the button
                svBut.Content = "Name: " + info[0] + "\nType: " +
                    info[1] + "\nCost: " + info[2];

                svBut.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                svBut.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
                invoiceInfo.Add(DateTime.Now.Date);
                invoiceInfo.Add(info[0]); // TODO change the description
                invoiceInfo.Add(info[2]); // charge

                //adds the button onto panel
                this.buttonList.Add(svBut);
                this.chargesListPanel.Children.Add(svBut);

                SetButtonProperties();

            }
            #region oldcode
            /*
            Functionality.ServiceManager servMan = new Functionality.ServiceManager();
            // RoomManager rmMan = new RoomManager();
            expanderList = new List<Expander>();

            this.chargesListPanel.Children.Clear();


            List<ServiceObj> services = servMan.GetAllSType("Damages");


            foreach (ServiceObj s in services)
            {
                ArrayList info = s.ServiceInfo;
                Expander rmExp = new Expander();
                rmExp.MouseDoubleClick += new MouseButtonEventHandler(svExp_MouseDoubleClick);

                rmExp.Content = "Name " + info[0] + "\tType: " +
                    info[1] + "\tCost: " + info[2];

                this.expanderList.Add(rmExp);
                this.chargesListPanel.Children.Add(rmExp);

                SetButtonProperties();
            }
             */
            #endregion
        }

        #endregion


        #region Check for ServicesList Button Click

        public void ServicesMode(string mode)
        {
            if (mode.Equals("meal"))
            {
                mode = "meal";
                PopulateMealList();
            }
            else if (mode.Equals("cableInternet"))
            {
                mode = "cableInternet";
                PopulateCableList();
            }
            else if (mode.Equals("miniBar"))
            {
                mode = "miniBar";
                PopulateMiniBarList();
            }
            else if (mode.Equals("damages"))
            {
                mode = "damages";
                PopulateDamagesList();
            }


        }

        #endregion

        #region Other Methods
        public void SetButtonProperties()
        {
            foreach (Button exp in this.buttonList)
            {
                exp.Height = 80;
                exp.FontSize = 14;
            }
        }

        private void getInvoiceID()
        {

            ArrayList[] bookings = bookMan.GetCurrBookings();
            ArrayList bookingInfo = bookings[mainWindow.invoiceSelected][0] as ArrayList;
            ArrayList guestInfo = bookings[mainWindow.invoiceSelected][1] as ArrayList;

            //grabs id
            Objects.InvoiceObj invoice =  invMan.GetInvoice(bookingInfo, guestInfo);
            invoiceID = invoice.InvoiceID;
        }

        #endregion
    }
}
