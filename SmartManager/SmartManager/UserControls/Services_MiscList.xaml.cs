﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for Services_MiscList.xaml
    /// </summary>
    public partial class Services_MiscList : UserControl
    {
        MainWindow mainWindow;

        private Functionality.InvoiceManager invMan;
        private BookingManager bookMan;
       // private RoomObj selectedRoom;

        private string miscMsgBoxText;

        public Services_MiscList(MainWindow main)
        {
            InitializeComponent();

            this.mainWindow = main;

            invMan = new Functionality.InvoiceManager();
            bookMan = new BookingManager();
        }

        #region Button Functionality
        /// <summary>
        /// Cancel button closes current screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Saves user information
        /// Add user information to database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (MiscInfoIsValid())
            {

                #region Get information from textboxes

                string misc_name = this.serviceNameTextBox.Text;
                string misc_description = this.serviceDescriptionTextBox.Text;
                decimal misc_charge = Convert.ToDecimal(this.serviceChargeTextBox.Text);

                #endregion

                #region Set information
                ArrayList serviceInfo = new ArrayList();
                ArrayList invoiceDetInfo = new ArrayList();

                serviceInfo.Add(this.serviceNameTextBox.Text);
                serviceInfo.Add(this.serviceDescriptionTextBox.Text);
                serviceInfo.Add(this.serviceChargeTextBox.Text);

                invoiceDetInfo.Add(DateTime.Now.Date);
                invoiceDetInfo.Add(serviceInfo[1]); // TODO description
                invoiceDetInfo.Add(serviceInfo[2]);

                ArrayList[] bookings = bookMan.GetCurrBookings();
                ArrayList bookingInfo = bookings[0][mainWindow.invoiceSelected] as ArrayList;
                ArrayList guestInfo = bookings[1][mainWindow.invoiceSelected] as ArrayList;

                //grabs id
                Objects.InvoiceObj invoice = invMan.GetInvoice(bookingInfo, guestInfo);


                ArrayList invoiceDetail = new ArrayList();
                invoiceDetail.Add(serviceInfo[0].ToString() + " - " + serviceInfo[1].ToString());
                invoiceDetail.Add(serviceInfo[2]);

                invMan.AddInvoiceDetail(invoice.InvoiceInfo, invoiceDetail);

                #endregion

                ClearAllFields();

                this.Visibility = Visibility.Hidden;

                ResetForm();

                // TODO update invoice of room with selected service
                //this.invMan.AddInvoiceDetail(invoiceDetInfo, this.selectedRoom.RoomInfo);
            }
            else
            {
                MessageBox.Show(this.miscMsgBoxText + "Invalid inputs. Please check and try again.");
            }
        }

        #endregion

        #region Save Button Other Methods

        private void ResetForm()
        {
            this.saveButton.Foreground = Brushes.Gray;
        }

        private void ClearAllFields()
        {
            this.serviceNameTextBox.Text = "";
            this.serviceDescriptionTextBox.Text = "";
            this.serviceChargeTextBox.Text = "";
        }

        private bool MiscInfoIsValid()
        {
            bool isValid = true;
            string[] charge = this.serviceChargeTextBox.Text.Split('.');

            #region Null Checks
            if (this.serviceNameTextBox.Text == "")
            {
                this.miscMsgBoxText += "Please enter a service name.\n";
                isValid = false;
            }
            if (this.serviceDescriptionTextBox.Text == "")
            {
                this.miscMsgBoxText += "Please enter a description.\n";
                isValid = false;
            }
            if (this.serviceChargeTextBox.Text == "")
            {
                this.miscMsgBoxText += "Please enter a charge cost.\n";
                isValid = false;
            }
            #endregion 

            #region Format Checks
            
            //if (charge[0].Length == 0)
            //{
            //    this.miscMsgBoxText = this.miscMsgBoxText + "Please enter a valid charge: <#'s>.##\n";
            //    isValid = false;
            //}
            //if (charge[1].Length == 0 || charge[1].Length != 2)
            //{
            //    this.miscMsgBoxText = this.miscMsgBoxText + "Please enter a valid charge: <#'s>.##\n";
            //    isValid = false;
            //}
            #endregion

            return isValid;
        }

        #endregion
    }
}
