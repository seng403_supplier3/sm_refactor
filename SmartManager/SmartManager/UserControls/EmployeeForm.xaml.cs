﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for EmployeeForm.xaml
    /// </summary>
    public partial class EmployeeForm : UserControl
    {
        private MainWindow mainWin;

        private EmployeeManager empMan;
        public bool editFlag = false;

        private ArrayList employeeToEdit;

        public EmployeeForm(MainWindow main)
        {
            InitializeComponent();

            this.mainWin = main;

            this.empMan = new EmployeeManager();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            // get all information (ADD NEW)
            if (!editFlag)
            {
                string fname = this.firstNameTextbox.Text;
                string lname = this.lastNameTextbox.Text;
                string type = this.empTypeComboBox.SelectedItem.ToString().Split(' ')[1];
                string password = GeneratePassword();
                string blank = ""; // filler so that indices for the ArrayList info matches 

                ArrayList list = new ArrayList();
                list.Add(fname);
                list.Add(lname);
                list.Add(type);
                list.Add(blank);
                list.Add(password);

                int empNum = this.empMan.SetEmployee(list);

                string msg = "New " + type + ": " + fname + " " + lname + " \nSaved!" +
                    "\n\n" + fname + "'s login credentials \n" +
                    "Employee ID: " + empNum + "\nTemporary Password: " + password +
                    "\n\nThis password can be changed upon first login.";

                ClearFields();

                this.Visibility = Visibility.Hidden;
                this.mainWin.empList.Visibility = Visibility.Visible;

                MessageBox.Show(msg);

                this.mainWin.empList.UpdateList();
            }
            else if (editFlag)
            {
                string fname = this.firstNameTextbox.Text;
                string lname = this.lastNameTextbox.Text;
                string type = this.empTypeComboBox.SelectedItem.ToString().Split(' ')[1];

                this.employeeToEdit[0] = fname;
                this.employeeToEdit[1] = lname;
                this.employeeToEdit[2] = type;

                this.empMan.EditEmployee(Convert.ToInt32(this.employeeToEdit[3]), this.employeeToEdit);

                MessageBox.Show("Your changes have been saved!");

                ClearFields();

                this.Visibility = Visibility.Hidden;

                this.mainWin.empList.UpdateList();
            }
        }

        private void ClearFields()
        {
            this.firstNameTextbox.Text = "";
            this.lastNameTextbox.Text = "";
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;

            if (!editFlag)
                this.mainWin.mainBookingDash.Visibility = Visibility.Visible;
            else
                this.mainWin.empList.Visibility = Visibility.Visible;

            ClearFields();
        }

        private string GeneratePassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[10];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            return finalString;
        }

        private void EmployeeForm_VisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Visibility == Visibility.Visible && this.editFlag)
            {
                PopulateFields();
            }
        }

        private void PopulateFields()
        {
            this.firstNameTextbox.Text = this.employeeToEdit[0].ToString();
            this.lastNameTextbox.Text = this.employeeToEdit[1].ToString();

            if (this.employeeToEdit[2].ToString().Equals("Employee"))
                this.empTypeComboBox.SelectedItem = this.employeeType;
            else if (this.employeeToEdit[2].ToString().Equals("Manager"))
                this.empTypeComboBox.SelectedItem = this.managerType;
        }

        public void SetEmployeeToEdit(ArrayList info)
        {
            this.employeeToEdit = info;
        }
    }
}
