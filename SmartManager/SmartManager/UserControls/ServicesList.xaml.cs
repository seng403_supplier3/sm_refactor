﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for ServicesList.xaml
    /// </summary>
    public partial class ServicesList : UserControl
    {
        MainWindow mainWindow;

        private RoomManager rmMan;
        //private RoomObj selectedRoom;



        private Functionality.ServiceManager serMan;
        private Functionality.InvoiceManager invMan;
        private BookingManager bookMan;

        private List<Button> buttonList;


        private int pgTracker = 1;

        public ServicesList(MainWindow p)
        {
            InitializeComponent();
            this.mainWindow = p;

            rmMan = new RoomManager();
            serMan = new Functionality.ServiceManager();
            invMan = new Functionality.InvoiceManager();
            bookMan = new BookingManager();

            buttonList = new List<Button>();
         
            PopulateBookingList();
        }
        
        #region BookingList

        /// <summary>
        /// populats the booking list that will be used
        /// </summary>
        private void PopulateBookingList()
        {
            this.bookingListPanel.Children.Clear();
            ArrayList[] currBookings = bookMan.GetCurrBookings();

            for (int i = 0; i < currBookings[0].Count; i++)
            {
                ArrayList booking = currBookings[0][i] as ArrayList;
                ArrayList guest = currBookings[1][i] as ArrayList;

                Button bookBut = new Button();
                bookBut.Click += new RoutedEventHandler(bookBut_Click);
                bookBut.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                bookBut.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));

                buttonList.Add(bookBut);

                bookBut.Content = "Room booking for:\n" + guest[0].ToString() + " " + guest[1].ToString() + "\n"
                    + "Start booking:  " + booking[0].ToString() + "\tEnd booking:  " + booking[1].ToString();;
                bookBut.Height = 80;
                
                this.bookingListPanel.Children.Add(bookBut);


            }
          
            
       

        }

        
        void bookBut_Click(object sender, RoutedEventArgs e)
        {
            Button selected = sender as Button;

            foreach (Button b in buttonList)
            {
                b.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                b.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
            }

            selected.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4699FF"));
            selected.Foreground = Brushes.White;
            
            

            mainWindow.invoiceSelected = this.bookingListPanel.Children.IndexOf(selected);

        }
        #endregion


        #region Room List

        #region old code
        /*
        private void PopulateRoomList()
        {
            this.roomListPanel.Children.Clear();

            DateTime start = DateTime.Now;
            DateTime end = start.AddDays(1); // guest stays at room for at least 1 day
            List<RoomObj> rooms = rmMan.GetOccupiedRooms(start, end);

            foreach (RoomObj rm in rooms)
            {
                ArrayList info = rm.RoomInfo;
                Expander rmExp = new Expander();

                rmExp.IsExpanded = true;
                rmExp.MouseDoubleClick += new MouseButtonEventHandler(rmExp_MouseDoubleClick);
               
                rmExp.Content = "Room test" + info[0] + "\tRoom Type: " +
                    info[1] + "\tRack Rate: " + info[2];

                this.roomListPanel.Children.Add(rmExp);
            }
        }
         */
        /*
                void rmExp_MouseDoubleClick(object sender, MouseButtonEventArgs e)
                {
                    // ROOM SELECTED from ROOM LIST
                    Expander selection = sender as Expander;

                    // deselect any previous selections
                    foreach (Expander exp in this.roomListPanel.Children)
                        exp.Background = Brushes.LightGray;

                    selection.Background = Brushes.White;

                    string[] text = selection.Content.ToString().Split('\t');
                    Console.WriteLine(text[0] + "//" + text[1] + "//" + text[2]);

                    ArrayList roomInfo = new ArrayList();
                    roomInfo.Add(text[0].Split(' ')[1]);
                    roomInfo.Add(text[1].Split(' ')[2]);
                    roomInfo.Add(text[2].Split(' ')[2]);

                    this.selectedRoom = new RoomObj(roomInfo);

                    // Retrieve invoice
                    // TODO getInvoice for the selected room
                }
                */
        #endregion
        #endregion

        //##TODO  needs to display the infomration for specific invoice
        #region Invoice Functionality

        /// <summary>
        /// dispalys the selected invoice
        /// </summary>
        void DisplayInvoice()
        {
            Decimal subtotalCost = 0;

            this.invoiceListPanel.Children.Clear();

            ArrayList[] bookings = bookMan.GetCurrBookings();
            ArrayList bookingInfo = bookings[0][mainWindow.invoiceSelected]as ArrayList;
            ArrayList guestInfo = bookings[1][mainWindow.invoiceSelected] as ArrayList;

            Objects.InvoiceObj invoice = invMan.GetInvoice(bookingInfo, guestInfo);
            //sets up rackrate label and cost plus formatting
            Label roomCharge = new Label();
           // subtotalCost += Convert.ToDecimal(bookingInfo[2]); //update subtotal
            roomCharge.Content = "Room Rack Rate\nCost:  " + bookingInfo[2].ToString();
            roomCharge.Height = 50;
            roomCharge.FontSize = 15;
            roomCharge.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
            roomCharge.Foreground = Brushes.White;
            roomCharge.Margin = new Thickness(2);

            this.invoiceListPanel.Children.Add(roomCharge);

            //setting up room extra label to divide
            Label roomExtras = new Label();
            roomExtras.Content = "Room Extras";
            roomExtras.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
            roomExtras.Foreground = Brushes.White;

            this.invoiceListPanel.Children.Add(roomExtras);

            ArrayList invoiceDetails = invoice.InvoiceInfo[4] as ArrayList;
            
            //for loop to add the invoice details
            for (int i = 0; i < invoiceDetails.Count; i++)
            {
               //gets object from arrayList
                Objects.InvoiceDetailsObj id = invoiceDetails[i] as Objects.InvoiceDetailsObj;

                //sets up button and formatting
                Button idBut = new Button();
                subtotalCost += Convert.ToDecimal(id.InvoiceDetailsInfo[1]); //update subtotal
                idBut.Content = "Date Added:  " + id.InvoiceDetailsInfo[2].ToString() +
                     "\tService Type:  " + id.InvoiceDetailsInfo[0].ToString() + 
                     "\tService Name:  " + id.InvoiceDetailsInfo[3].ToString() +
                     "\nCost:  "+ id.InvoiceDetailsInfo[1].ToString();
                idBut.HorizontalContentAlignment = HorizontalAlignment.Left;
                idBut.Height = 50;
                idBut.FontSize = 14;
                idBut.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                idBut.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));

                if (id.InvoiceDetailsInfo[3].ToString().Equals(" Rack Rate")){
                    idBut.IsEnabled = false;
                }

                idBut.Click += (sender, e) => idBut_Click(sender, e, id.InvoiceDetailsInfo);

                this.invoiceListPanel.Children.Add(idBut);
            }

            //updated subtotal tax and taotals
            Decimal tax = subtotalCost * (Decimal)0.05;
            Decimal total = tax + subtotalCost;

            //updates invoice part of sub total tax and totals
            invoice.InvoiceInfo[1] = subtotalCost;
            invoice.InvoiceInfo[2] = tax;
            invoice.InvoiceInfo[3] = total;

            //edit invoice in database
            invMan.EditInvoice(bookingInfo, guestInfo, invoice);

            totalCostlabel.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
            totalCostlabel.Foreground = Brushes.White;
            //displays total to user
            totalCostlabel.Content = "Sub-total:\t$" + subtotalCost.ToString("F") +
                "\nTaxes:\t\t$" + tax.ToString("F") +"\nTotal:\t\t$" + total.ToString("F");
            

        }

        /// <summary>
        /// when invoice detail button clicked on ask if user wants to delete the service charge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void idBut_Click(object sender, RoutedEventArgs e, ArrayList invoiceDetInfo)
        {


            string str = "Do you want to remove:\n" + "Date Added:  " + invoiceDetInfo[2].ToString() +
                     "\nService Type:  " + invoiceDetInfo[0].ToString() +
                     "\nService Name:  " + invoiceDetInfo[3].ToString() +
                     "\nCost:  " + invoiceDetInfo[1].ToString();
            MessageBoxResult result = MessageBox.Show(str, "Remove Service Charge", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                //removes detail off of invoice
                invMan.RemoveInvoiceDetail(invoiceDetInfo);
                //redisplay the invoice
                DisplayInvoice();

            }
        }

        /*
        /// <summary>
        /// populates the invoice list
        /// </summary>
        void PopulateInvoiceList()
        {

            this.invoiceListPanel.Children.Clear();


            DateTime start = DateTime.Now;
            DateTime end = start.AddDays(1); // guest stays at room for at least 1 day
            List<Objects.InvoiceDetailsObj> invoices = invMan.GetAllInvoices(); //TODO change to room-guest's invoice

            foreach (var inv in invoices)
            {
                ArrayList info = inv.InvoiceDetailsInfo;
                Button invoice = new Button();


                invoice.Height = 45;
                invoice.FontSize = 14;

                invoice.MouseDoubleClick += new MouseButtonEventHandler(invExp_MouseDoubleClick);
                invoice.Content = "Service Date " + info[0] + "\tService Type: " +
                    info[1] +"\tService Cost: $" + info[2];

                this.invoiceListPanel.Children.Add(invoice);
            }
            this.invoiceListPanel.Children.Clear();
        }
        */
        /*
        void invExp_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string str = "Do you wish to Delete:\n" + (sender as Expander).Content;
            MessageBoxResult result = MessageBox.Show(str, "Checkin / Checkout", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                
                this.invoiceListPanel.Children.Remove(sender as Expander);
                // TODO
                //invMan.RemoveInvoiceDetail(
            }
        } 
         */
        #endregion


        #region Footer Button Functionality
        private

    
    void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.pgTracker == 0)
            {
                Grid.SetZIndex(this.roomGrid, 1);
                Grid.SetZIndex(this.navigationGrid, 0);
                Grid.SetZIndex(this.invoiceGrid, 0);

                this.mainLabel.Content = "Available Invoices";

                this.backButton.Foreground = Brushes.Gray;
                this.invoiceButton.Foreground = Brushes.Black;
                this.chargesButton.Foreground = Brushes.Gray;
                pgTracker++;
            }
        }       
        
        private void InvoiceButton_Click(object sender, RoutedEventArgs e)
        {
            // room List -> invoice
            if (this.pgTracker == 1)
            {
                
                Grid.SetZIndex(this.roomGrid, 0);
                Grid.SetZIndex(this.navigationGrid, 0);
                Grid.SetZIndex(this.invoiceGrid, 1);

                DisplayInvoice();
                
                this.backButton.Foreground = Brushes.Black;
                this.invoiceButton.Foreground = Brushes.Gray;
                this.chargesButton.Foreground = Brushes.Black;

                //to get the booking ID
                ArrayList[] bookings = bookMan.GetCurrBookings();
                ArrayList bookingInfo = bookings[0][mainWindow.invoiceSelected] as ArrayList;

                ArrayList bookInfo = new ArrayList();
                bookInfo.Add(bookingInfo[3]);
                bookInfo.Add(bookingInfo[4]);


                this.mainLabel.Content = "Booking ID: " + bookMan.GetBookingID(bookInfo).ToString() ;
                pgTracker--;
            }
        }

        private void ChargesButton_Click(object sender, RoutedEventArgs e)
        {
            // room List -> invoice
            if (this.pgTracker == 1)
            {
                Grid.SetZIndex(this.roomGrid, 0);
                Grid.SetZIndex(this.navigationGrid, 1);
                Grid.SetZIndex(this.invoiceGrid, 0);

                this.backButton.Foreground = Brushes.Black;
                this.invoiceButton.Foreground = Brushes.Gray;
                this.chargesButton.Foreground = Brushes.Gray;

                this.mainLabel.Content = "Hotel Services";
                pgTracker--;
            }
        }
        #endregion


        #region Services Charge Buttons
        private void MealButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.chargesList.Visibility = Visibility.Visible;

            this.mainWindow.chargesList.informationLabel.Content = "Meal Charges";
            this.mainWindow.chargesList.ServicesMode("meal");
        }

        private void CableInternetButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.chargesList.Visibility = Visibility.Visible;

            this.mainWindow.chargesList.informationLabel.Content = "Cable/Internet Charges";
            this.mainWindow.chargesList.ServicesMode("cableInternet");
        }

        private void MiniBarButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.chargesList.Visibility = Visibility.Visible;

            this.mainWindow.chargesList.informationLabel.Content = "Mini Bar Charges";
            this.mainWindow.chargesList.ServicesMode("miniBar");
        }

        private void DamageButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.chargesList.Visibility = Visibility.Visible;

            this.mainWindow.chargesList.informationLabel.Content = "Damages Charges";
            this.mainWindow.chargesList.ServicesMode("damages");
        }

        private void MiscButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.miscList.Visibility = Visibility.Visible;
        }

        #endregion

        private void addDiscountBut_Click(object sender, RoutedEventArgs e)
        {

            //error check
            if (discountCmb.Text == string.Empty)
            {
                MessageBox.Show("Please select a value for discount");
            }


            string str = "Do you want to apply a " + discountCmb.Text + " discount?";
            MessageBoxResult result = MessageBox.Show(str, "Apply discount", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {



                ArrayList[] bookings = bookMan.GetCurrBookings();
                ArrayList bookingInfo = bookings[0][mainWindow.invoiceSelected] as ArrayList;
                ArrayList guestInfo = bookings[1][mainWindow.invoiceSelected] as ArrayList;

                //grabs id
                Objects.InvoiceObj invoice = invMan.GetInvoice(bookingInfo, guestInfo);


                decimal discount = Convert.ToDecimal(((Decimal)bookingInfo[2] * (Convert.ToDecimal((discountCmb.Text).ToString().Trim('%')) / (Decimal)100)).ToString("F"));

                ArrayList invoiceDetail = new ArrayList();
                invoiceDetail.Add("Room Charges - Discounted by " + (discountCmb.Text).ToString());
                invoiceDetail.Add(-discount);

                invMan.AddInvoiceDetail(invoice.InvoiceInfo, invoiceDetail);

                DisplayInvoice();
            }
        }

        #region Other Functionality
        //public void SetButtonProperties()
        //{
        //    foreach (Expander exp in this.expanderList)
        //    {
        //        exp.Height = 80;
        //        exp.FontSize = 14;
        //        exp.IsExpanded = true;

               
        //    }
        //}
        #endregion
    }
}
