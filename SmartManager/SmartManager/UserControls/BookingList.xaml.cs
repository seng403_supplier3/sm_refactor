﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for BookingList.xaml
    /// </summary>
    public  partial class BookingList : UserControl
    {
        #region Instance and Class Variables
        MainWindow mainWindow;
        private List<string[]> bookingList;
        //private List<string> bookingKeys;
        private List<Expander> expanderList;

        private BookingManager bkMan;
        private ArrayList[] allbookings;
        public ArrayList[] bookingToEdit;

        public bool cancelFlag = false;
        #endregion


        #region Constructor
        public BookingList(MainWindow p)
        {
            InitializeComponent();

            this.mainWindow = p;

            this.bkMan = new BookingManager();

            PopulateList();
            SetButtonProperties();
        }
        #endregion


        public void PopulateList()
        {
            bookingList = new List<string[]>();
            expanderList = new List<Expander>();
            //bookingKeys = BookingManager.Instance.GetBookingKeys();

            
            #region Old (temp code)
            //for (int i = 0; i < BookingManager.Instance.GetBookingCount(); i++)
            //{
            //    bookingList.Add(BookingManager.Instance.GetBooking(bookingKeys[i]));

            //    name = GuestManager.Instance.GetGuest(bookingList[i][0])[2] + ", " +
            //        GuestManager.Instance.GetGuest(bookingList[i][0])[3];
            //    control = new Button();
            //    control.Content = name + "\n" + bookingList[i][2] + " - " + bookingList[i][3];

            //    this.buttonList.Add(control);
            //    this.stackList.Children.Add(control);
            //}
            #endregion

            this.allbookings = this.bkMan.GetCurrBookings();

            for (int i = 0; i < this.allbookings[0].Count; i++)
            {
                ArrayList binfo = this.allbookings[0][i] as ArrayList;
                ArrayList ginfo = this.allbookings[1][i] as ArrayList;
                Expander bkExp = new Expander();

                // TODO: need to get the Guest linked to the booking entity
                bkExp.Content = ginfo[0] +" "+ ginfo[1] +" || Checkin: "+ 
                    binfo[0].ToString().Split(' ')[0] +" || Checkout: "+ binfo[1].ToString().Split(' ')[0];

                this.expanderList.Add(bkExp);
                this.stackList.Children.Add(bkExp);
            }
        }

        public void UpdateList()
        {
            this.stackList.Children.Clear();
            this.expanderList.Clear();

            PopulateList();
            SetButtonProperties();
        }

        public void SetButtonProperties()
        {
            foreach (Expander exp in this.expanderList)
            {
                exp.Height = 80;
                exp.FontSize = 14;
                exp.IsExpanded = true;

                exp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                exp.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));

                exp.MouseDoubleClick += new MouseButtonEventHandler(bookingButton_DoubleClick);
                exp.MouseEnter += new MouseEventHandler(exp_MouseEnter);
                exp.MouseLeave += new MouseEventHandler(exp_MouseLeave);
            }
        }

        void exp_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Expander).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
            (sender as Expander).Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
        }

        void exp_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Expander).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
            (sender as Expander).Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
        }

        public void bookingButton_DoubleClick(object sender, RoutedEventArgs e)
        {
            // check which button is being clicked
            foreach (Expander exp in this.expanderList)
            {
                if (exp == sender as Expander)
                {
                    // edit
                    if (!cancelFlag)
                    {
                        //Console.WriteLine(exp.Content);
                        int index = this.expanderList.IndexOf(exp);

                        this.mainWindow.bookingForm.SetBookingToEdit(this.allbookings[0][index] as ArrayList);
                        this.mainWindow.bookingForm.SetGuestToEdit(this.allbookings[1][index] as ArrayList);

                        this.mainWindow.bookingForm.Visibility = Visibility.Visible;
                        //Canvas.SetZIndex(this, -1);
                    }
                    // cancel
                    else if (cancelFlag)
                    {
                        int index = this.expanderList.IndexOf(exp);
                        // show confirmation window
                        string info = exp.Content.ToString();

                          MessageBoxResult result = MessageBox.Show("Cancel the following booking? \n" + info, "Confirm Cancellation", MessageBoxButton.YesNo);

                        if (result == MessageBoxResult.Yes)
                        {
                            //BookingManager.Instance.DeleteBooking(bookingKeys[index]);
                            this.stackList.Children.Remove(exp);
                            this.expanderList.RemoveAt(index);

                            this.bkMan.DeleteBooking(this.allbookings[0][index] as ArrayList, 
                                this.allbookings[1][index] as ArrayList);

                            this.cancelFlag = false;
                            break;
                        }
                    }
                }
            }
        }

        //TODO sort the list by last name
        private void lastNameButton_Click(object sender, RoutedEventArgs e)
        {

        }

        //TODO sort the list by booking date
        private void dateButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void bookingList_VisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Visibility == Visibility.Visible)
            {
                UpdateList();

                if (cancelFlag)
                    this.bookingLabel.Content = "Booking List: CANCEL Booking";
                else if (!cancelFlag)
                    this.bookingLabel.Content = "Booking List: EDIT Booking";
            }
        }
    }
}
