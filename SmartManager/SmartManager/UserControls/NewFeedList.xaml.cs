﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for NewFeedList.xaml
    /// </summary>
    public partial class NewFeedList : UserControl
    {
        MainWindow mainWindow;

        ArrayList[] toCheckIn;
        ArrayList[] toCheckOut;
        List<Button> inButtonList;
        List<Button> outButtonList;

        BookingManager bkMan;

        public NewFeedList(MainWindow main)
        {
            InitializeComponent();

            this.mainWindow = main;
            this.bkMan = new BookingManager();
            this.toCheckIn = new ArrayList[2] { new ArrayList(), new ArrayList() };
            this.toCheckOut = new ArrayList[2] { new ArrayList(), new ArrayList() };
            this.inButtonList = new List<Button>();
            this.outButtonList = new List<Button>();

            PopulateFeed();
        }

        public void PopulateFeed()
        {
            ArrayList[] unfiltered = this.bkMan.GetCurrBookings();
            ArrayList booking = unfiltered[0] as ArrayList;
            ArrayList guest = unfiltered[1] as ArrayList;

            // get all bookings to be checked in or out for current date
            for (int i = 0; i < booking.Count; i++)
            {
                if ((DateTime.Parse((booking[i] as ArrayList)[0].ToString()).Date) == DateTime.Now.Date)
                {
                    this.toCheckIn[0].Add(booking[i]);
                    this.toCheckIn[1].Add(guest[i]);
                }
                if ((DateTime.Parse((booking[i] as ArrayList)[1].ToString()).Date) == DateTime.Now.Date)
                {
                    this.toCheckOut[0].Add(booking[i]);
                    this.toCheckOut[1].Add(guest[i]);
                }
            }

            // create buttons and add to stack panel
            if (this.toCheckIn.Count() == 0)
            {
                Label label = new Label();
                label.Content = "No Bookings to Check In";
                label.FontSize = 16;

                this.checkInPanel.Children.Add(label);
            }
            else if (this.toCheckOut.Count() == 0)
            {
                Label label = new Label();
                label.Content = "No Bookings to Check Out";
                label.FontSize = 16;

                this.checkOutPanel.Children.Add(label);
            }
            else
            {
                for (int i = 0; i < this.toCheckIn[0].Count; i++)
                {
                    ArrayList info = this.toCheckIn[1][i] as ArrayList;
                    Button button = new Button();
                    button.Content = info[1] + ", " + info[0]
                        + "\n" + info[12].ToString().Split(' ')[0] + " - " + info[13].ToString().Split(' ')[0];

                    this.inButtonList.Add(button);
                    this.checkInPanel.Children.Add(button);

                    button.Height = 100;
                    button.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                    button.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
                    button.Click += new RoutedEventHandler(checkInButton_Click);
                }

                for (int i = 0; i < this.toCheckOut[0].Count; i++)
                {
                    ArrayList info = this.toCheckOut[1][i] as ArrayList;
                    Button button = new Button();
                    button.Content = info[1] + ", " + info[0]
                        + "\n" + info[12].ToString().Split(' ')[0] + " - " + info[13].ToString().Split(' ')[0];

                    this.outButtonList.Add(button);
                    this.checkOutPanel.Children.Add(button);

                    button.Height = 100;
                    button.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D7E9FF"));
                    button.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486B98"));
                    button.Click += new RoutedEventHandler(checkOutButton_Click);
                }

            }
        }

        void checkInButton_Click(object sender, RoutedEventArgs e)
        {
            string str = "Confirm checkin for:\n" + (sender as Button).Content;
            MessageBoxResult result = MessageBox.Show(str, "Checkin", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                int index = this.inButtonList.IndexOf(sender as Button);

                this.inButtonList.Remove(sender as Button);
                this.checkInPanel.Children.Remove(sender as Button);

                ArrayList info = this.toCheckIn[0][index] as ArrayList;
                ArrayList ginfo = this.toCheckIn[1][index] as ArrayList;
                this.toCheckIn[0].RemoveAt(index);
                this.toCheckIn[1].RemoveAt(index);

                BookingObj booking = new BookingObj(info);
                GuestObj guest = new GuestObj(ginfo);
                this.bkMan.CheckIn(booking, guest);

                //Update();
            }
        }

        void checkOutButton_Click(object sender, RoutedEventArgs e)
        {
            string str = "Confirm checkout for:\n" + (sender as Button).Content;
            MessageBoxResult result = MessageBox.Show(str, "Checkout", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                int index = this.outButtonList.IndexOf(sender as Button);

                this.outButtonList.Remove(sender as Button);
                this.checkOutPanel.Children.Remove(sender as Button);

                ArrayList info = this.toCheckOut[0][index] as ArrayList;
                ArrayList ginfo = this.toCheckOut[1][index] as ArrayList;
                this.toCheckOut[0].RemoveAt(index);
                this.toCheckOut[1].RemoveAt(index);

                BookingObj booking = new BookingObj(info);
                //this.bkMan.CheckOut(booking);

                //Update();
            }
        }

        // TODO add functionality to edit Database
        public void Update()
        {
            this.checkInPanel.Children.Clear();
            this.checkOutPanel.Children.Clear();

            PopulateFeed();
        }
    }
}
