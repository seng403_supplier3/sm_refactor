﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for BookingForm.xaml
    /// </summary>
    public partial class BookingForm : UserControl
    {
        #region Instance and Class Variables
        MainWindow mainWindow;

        private RoomManager rmMan;
        private BookingManager bkMan;
        private GuestManager guestMan;

        private RoomObj selectedRoom;

        private int pgTracker = 1;
        private string guestMsgBoxText;
        private string billingMsgBoxText;

        public bool editFlag = false;
        private ArrayList guestToEdit;
        private ArrayList bookingToEdit;

        private Expander selectedRm;
        #endregion


        #region Constructor
        public BookingForm(MainWindow main)
        {
            InitializeComponent();

            this.mainWindow = main;

            rmMan = new RoomManager();
            bkMan = new BookingManager();
            guestMan = new GuestManager();

            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(BookingForm_IsVisibleChanged);
        }

        void BookingForm_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Visibility == Visibility.Visible && this.editFlag)
            {
                PopulateAll();
            }
        }

        private void PopulateAll()
        {
            this.firstNameTextBox.Text = this.guestToEdit[0].ToString();
            this.lastNameTextBox.Text = this.guestToEdit[1].ToString();
            this.emailTextBox.Text = this.guestToEdit[2].ToString();
            this.phNumTextBox.Text = this.guestToEdit[3].ToString();
            this.address1TextBox.Text = this.guestToEdit[4].ToString();
            this.address2TextBox.Text = this.guestToEdit[5].ToString();
            this.cityTextBox.Text = this.guestToEdit[6].ToString();
            this.regionTextBox.Text = this.guestToEdit[7].ToString();
            this.postalTextBox.Text = this.guestToEdit[8].ToString();
            this.countryTextBox.Text = this.guestToEdit[9].ToString();
            Console.WriteLine(this.guestToEdit[12].ToString());
            Console.WriteLine(this.guestToEdit[13].ToString());
            //this.checkInPicker.SelectedDate = DateTime.Parse(this.guestToEdit[12].ToString());
            //this.checkOutPicker.SelectedDate = DateTime.Parse(this.guestToEdit[13].ToString());
            this.creditTypeComboBox.SelectedIndex =
                this.creditTypeComboBox.Items.IndexOf(this.guestToEdit[14].ToString());
            this.creditCardNumTextBox.Text = this.guestToEdit[15].ToString();
            this.cardHolderTextBox.Text = this.guestToEdit[16].ToString();
            this.securityNumTextBox.Text = this.guestToEdit[17].ToString();
            this.expiryTextBox.Text = this.guestToEdit[18].ToString();

            // VIP status
            if (Convert.ToByte(this.guestToEdit[10]) == 0)
                this.guestTypeComboBox.SelectedIndex = this.guestTypeComboBox.Items.IndexOf("Regular");
            else
                this.guestTypeComboBox.SelectedIndex = this.guestTypeComboBox.Items.IndexOf("VIP");
        }

        public void SetGuestToEdit(ArrayList guestInfo)
        {
            this.guestToEdit = guestInfo;
        }

        public void SetBookingToEdit(ArrayList bookingInfo)
        {
            this.bookingToEdit = bookingInfo;
        }
        #endregion



        #region Button OnClick Functionality

        /// <summary>
        /// Cancel button closes current screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;

            this.editFlag = false;

            ClearAllFields();
        }

        /// <summary>
        /// Page changes when you click 'next'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            //Grid guestCopy = this.guestInfo;
            //Grid stayCopy = this.stayInfo;
            //Grid billingCopy = this.billingInfo;


            // guest info --> stay info
            if (this.pgTracker == 1)
            {
                if (!GuestInfoIsValid())
                {
                    MessageBox.Show(this.guestMsgBoxText);
                }
                else
                {
                    this.informationLabel.Content = "Stay Information";
                    //guestCopy = this.guestInfo;
                    //this.dockPanel.Children.Remove(this.guestInfo);
                    //this.dockPanel.Children.Add(stayCopy);

                    // stayInfo appears in the foreground (higher ZIndex)
                    Grid.SetZIndex(this.guestInfo, 0);
                    Grid.SetZIndex(this.stayInfo, 1);
                    Grid.SetZIndex(this.billingInfo, 0);

                    //this.backButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#acbacc"));
                    //this.nextButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                    this.pgTracker++;
                }
            }

            // stayinfo --> billing info
            else if (this.pgTracker == 2)
            {
                if (!StayInfoIsValid())
                {
                    MessageBox.Show("Please select a check-in and/or check-out date and/or room.");
                }
                else
                {
                    this.informationLabel.Content = "Billing Information";
                    //this.dockPanel.Children.Remove(this.stayInfo);
                    //this.dockPanel.Children.Add(this.billingInfo);

                    // billingInfo appears in the foreground (higher ZIndex)
                    Grid.SetZIndex(this.guestInfo, 0);
                    Grid.SetZIndex(this.stayInfo, 0);
                    Grid.SetZIndex(this.billingInfo, 1);

                    //this.backButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                    //this.nextButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#acbacc"));
                    this.pgTracker++;
                }
            }
        }

        /// <summary>
        /// Page changes when you click 'back'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            // billing info --> stay info
            if (this.pgTracker == 3)
            {
                if (!BillingInfoIsValid())
                {
                    MessageBox.Show(this.billingMsgBoxText);
                }
                else
                {
                    this.informationLabel.Content = "Stay Information";
                    //this.dockPanel.Children.Add(this.stayInfo);
                    //this.dockPanel.Children.Remove(this.billingInfo);

                    // stayInfo appears in the foreground (higher ZIndex)
                    Grid.SetZIndex(this.guestInfo, 0);
                    Grid.SetZIndex(this.stayInfo, 1);
                    Grid.SetZIndex(this.billingInfo, 0);

                    //this.backButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                    //this.nextButton.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#acbacc"));
                    this.pgTracker--;
                }
            }

            // stay info --> guest info
            else if (this.pgTracker == 2)
            {
                if (!StayInfoIsValid())
                {
                    MessageBox.Show("Please select a check-in and/or check-out date and/or room.");
                }
                else
                {
                    this.informationLabel.Content = "Guest Information";
                    //this.dockPanel.Children.Add(this.guestInfo);
                    //this.dockPanel.Children.Remove(this.stayInfo);

                    // stayInfo appears in the foreground (higher ZIndex)
                    Grid.SetZIndex(this.guestInfo, 1);
                    Grid.SetZIndex(this.stayInfo, 0);
                    Grid.SetZIndex(this.billingInfo, 0);

                    //this.backButton.Foreground = Brushes.Gray;
                    //this.nextButton.Foreground = Brushes.Black;
                    this.pgTracker--;
                }
            }
        }

        /// <summary>
        /// Saves user information
        /// Add user information to database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            // SAVE INFORMATION TO DATABASE
            if (GuestInfoIsValid() && StayInfoIsValid() && BillingInfoIsValid())
            {
                // Set guestType to match boolean parameter
                ComboBoxItem tmp_guestType = (ComboBoxItem)this.guestTypeComboBox.SelectedItem; // tmp var
                string guest_guestType = tmp_guestType.Content.ToString();
                Boolean guest_isVIP = setGuestType(guest_guestType);

                int checkedIn = 0;
                // get IscChecked in Value -- if checkInDate is today then auto checkin
                if (this.checkInPicker.SelectedDate.Value.Date == DateTime.Now.Date)
                    checkedIn = 1;

                #region Set Guest and Booking Info
                ArrayList guestInfo = new ArrayList();
                ArrayList bookingInfo = new ArrayList();

                // add info to guest
                guestInfo.Add(this.firstNameTextBox.Text);
                guestInfo.Add(this.lastNameTextBox.Text);
                guestInfo.Add(this.emailTextBox.Text);
                guestInfo.Add(this.phNumTextBox.Text);
                guestInfo.Add(this.address1TextBox.Text);
                guestInfo.Add(this.address2TextBox.Text);
                guestInfo.Add(this.cityTextBox.Text);
                guestInfo.Add(this.regionTextBox.Text);
                guestInfo.Add(this.postalTextBox.Text);
                guestInfo.Add(this.countryTextBox.Text);
                guestInfo.Add(guest_isVIP);
                guestInfo.Add(checkedIn);
                guestInfo.Add(this.checkInPicker.SelectedDate.Value);
                guestInfo.Add(this.checkOutPicker.SelectedDate.Value);
                guestInfo.Add(this.creditTypeComboBox.SelectedItem.ToString().Split(' ')[1]);
                guestInfo.Add(this.creditCardNumTextBox.Text);
                guestInfo.Add(this.cardHolderTextBox.Text);
                guestInfo.Add(this.securityNumTextBox.Text);
                guestInfo.Add(this.expiryTextBox.Text);

                // add info to booking
                bookingInfo.Add(this.checkInPicker.SelectedDate.Value.Date);
                bookingInfo.Add(this.checkOutPicker.SelectedDate.Value.Date);
                bookingInfo.Add(this.selectedRoom.RoomInfo[2]);

                // edit
                if (this.editFlag)
                {
                    this.bkMan.EditBooking(bookingInfo, this.selectedRoom.RoomInfo, guestInfo);
                }
                // add new
                else
                {
                    int guestID = guestMan.SetGuest(guestInfo);
                    bkMan.SetBooking(bookingInfo, guestID, this.selectedRoom.RoomInfo);
                }

                #endregion

                MessageBox.Show("Save Successful!");

                ClearAllFields();

                // stayInfo appears in the foreground (higher ZIndex)
                Grid.SetZIndex(this.guestInfo, 2);
                Grid.SetZIndex(this.stayInfo, 1);
                Grid.SetZIndex(this.billingInfo, 0);
                this.Visibility = Visibility.Hidden; //hide everything

                ResetForm();

                this.mainWindow.bookingList.UpdateList();

                this.editFlag = false;
            }
            else
            {
                MessageBox.Show("Invalid inputs. Please check and try again.");
            }
        }

        private void ResetForm()
        {
            this.pgTracker = 1;
            this.informationLabel.Content = "Guest Information";
            this.backButton.Foreground = Brushes.Gray;
            this.nextButton.Foreground = Brushes.Black;
        }

        /// <summary>
        /// Change string guestType to boolean
        /// TODO - may not be necessary
        /// </summary>
        /// <param name="guestType"></param>
        /// <returns></returns>
        private Boolean setGuestType(string guestType)
        {
            Boolean isVIP = false;

            if (guestType.Equals("Regular"))
            {
                // do nothing
            }
            else if (guestType.Equals("VIP"))
            {
                isVIP = true;
            }
            else
            {
                // returns error if necessary

            }
            return isVIP;
        }
        #endregion



        #region Stay Information

        /// <summary>
        /// Method for checkIn datePicker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckInPicker_SelectedDate(object sender, SelectionChangedEventArgs e)
        {
            var picker = sender as DatePicker;
            DateTime selectedDate = picker.SelectedDate.Value;
            // Set variable = user input
            // Pass var to roomManager setDate --> Riane: Why the room manager?

            // check that check in date is not a date before today
            if (selectedDate.Date < DateTime.Today)
            {
                MessageBox.Show("INVALID: Please choose today or a later date.");
            }

        }

        /// <summary>
        /// Method for checkOut datePicker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckOutPicker_SelectedDate(object sender, SelectionChangedEventArgs e)
        {
            var picker = sender as DatePicker;
            DateTime selectedDate = picker.SelectedDate.Value;

            // Set variable = user input
            // Pass var to roomManager setDate --> Riane: Why the room manager?

            // check if checkout date is after checkin date or same as check in date
            if (selectedDate.Date < DateTime.Today || selectedDate.Date == DateTime.Today)
            {
                MessageBox.Show("INVALID: Please choose tomorrow or a later date.");
            }

            // populate room list
            PopulateRoomList();
        }

        // Note: previously written here but functionality moved to RoomManager for better management
        private void PopulateRoomList()
        {
            this.roomListPanel.Children.Clear();

            DateTime start = this.checkInPicker.SelectedDate.Value;
            DateTime end = this.checkOutPicker.SelectedDate.Value;
            List<RoomObj> rooms = rmMan.GetAvailRooms(start, end);

            foreach (RoomObj rm in rooms)
            {
                ArrayList info = rm.RoomInfo;
                Expander rmExp = new Expander();
                rmExp.IsExpanded = true;
                rmExp.MouseDoubleClick += new MouseButtonEventHandler(rmExp_MouseDoubleClick);

                rmExp.Content = "Room " + info[0] + "\tRoom Type: " +
                    info[1] + "\tRack Rate: " + info[2];

                rmExp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                rmExp.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
                rmExp.MouseEnter += new MouseEventHandler(rmExp_MouseEnter);
                rmExp.MouseLeave += new MouseEventHandler(rmExp_MouseLeave);

                this.roomListPanel.Children.Add(rmExp);
            }
        }

        void rmExp_MouseLeave(object sender, MouseEventArgs e)
        {
            if (sender != selectedRm)
            {
                (sender as Expander).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                (sender as Expander).Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
            }
        }

        void rmExp_MouseEnter(object sender, MouseEventArgs e)
        {
            if (sender != selectedRm)
            {
                (sender as Expander).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
                (sender as Expander).Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
            }
        }

        void rmExp_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // ROOM SELECTED from ROOM LIST
            Expander selection = sender as Expander;
            selectedRm = selection;

            // deselect any previous selections
            foreach (Expander exp in this.roomListPanel.Children)
            {
                exp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                exp.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
            }

            selection.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4699ff"));
            selection.Foreground = Brushes.White;

            string[] text = selection.Content.ToString().Split('\t');
            Console.WriteLine(text[0] + "//" + text[1] + "//" + text[2]);

            ArrayList roomInfo = new ArrayList();
            roomInfo.Add(text[0].Split(' ')[1]);
            roomInfo.Add(text[1].Split(' ')[2]);
            roomInfo.Add(text[2].Split(' ')[2]);

            this.selectedRoom = new RoomObj(roomInfo);
        }

        #region Temp (Currently not need)
        /// <summary>
        /// Method for roomList expander - collapsed
        /// Populate room data when expander is clicked:
        /// roomListPanel -> roomListScrollView -> roomList (expander name) -> roomListTextBlock ->
        /// roomImage
        /// TODO: add more (for later)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoomList_Collapsed(object sender, RoutedEventArgs e)
        {
            // Retrieve information from roomManager
            // Set .xaml properties to retrieved info
        }

        /// <summary>
        /// Method for roomList expander - expanded
        /// Populate room data for main section:
        /// roomListPanel -> roomListScrollView -> roomList (expander name) -> roomListProperties
        /// roomType
        /// roomNum
        /// isAvailable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoomList_Expanded(object sender, RoutedEventArgs e)
        {
            // Retrieve information from roomManager
            // Set .xaml properties to retrieved info
        }
        #endregion

        #endregion



        #region Other Methods
        private void ClearAllFields()
        {
            this.cardHolderTextBox.Text = "";
            this.creditCardNumTextBox.Text = "";
            this.securityNumTextBox.Text = "";
            this.expiryTextBox.Text = "";

            //this.checkInPicker.SelectedDate = null;
            //this.checkOutPicker.SelectedDate = null;

            this.firstNameTextBox.Text = "";
            this.lastNameTextBox.Text = "";
            this.address1TextBox.Text = "";
            this.address2TextBox.Text = "";
            this.cityTextBox.Text = "";
            this.regionTextBox.Text = "";
            this.countryTextBox.Text = "";
            this.postalTextBox.Text = "";
            this.phNumTextBox.Text = "";
            this.emailTextBox.Text = "";
        }


        private bool GuestInfoIsValid()
        {
            bool isValid = true;
            this.guestMsgBoxText = "Invalid entries.\n";
            string[] phone = this.phNumTextBox.Text.Split('-');
            string[] email = this.emailTextBox.Text.Split('@');

            // check that all the information has been filled out
            #region Null Checks
            // check that textfields are NOT null
            if (this.lastNameTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a last name.\n";
                isValid = false;
            }
            if (this.firstNameTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a first name.\n";
                isValid = false;
            }
            if (this.address1TextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a valid address.\n";
                isValid = false;
            }
            if (this.cityTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please entry a city.\n";
                isValid = false;
            }
            if (this.regionTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a region.\n";
                isValid = false;
            }
            if (this.countryTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a country.\n";
                isValid = false;
            }
            if (this.postalTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a postal code.\n";
                isValid = false;
            }
            if (this.phNumTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a valid phone number: xxx-xxx-xxx\n";
                isValid = false;
            }
            if (this.emailTextBox.Text == "")
            {
                this.guestMsgBoxText += "Please enter a valid email address: myemail@host.domain.\n";
                isValid = false;
            }
            if (this.guestTypeComboBox.SelectedItem == null)
            {
                this.guestMsgBoxText += "Please select a guest type.\n";
                isValid = false;
            }
            #endregion

            // check for formatting
            #region Format Checks
            if (phone.Length != 3 || phone[0].Length != 3 || phone[1].Length != 3 || phone[2].Length != 4)
            {
                this.guestMsgBoxText = this.guestMsgBoxText + "Please enter a valid phone number: xxx-xxx-xxx\n";
                isValid = false;
            }
            if (email.Length != 2 || email[1].Split('.').Length == 1)
            {
                this.guestMsgBoxText = this.guestMsgBoxText + "Please enter a valid email address: myemail@host.domain.\n";
                isValid = false;
            }
            #endregion

            return isValid;
        }

        private bool StayInfoIsValid()
        {
            bool isValid = true;

            if (this.checkInPicker.SelectedDate == null || this.checkOutPicker.SelectedDate == null)
                isValid = false;

            if (this.selectedRoom == null)
                isValid = false;

            return isValid;
        }

        private bool BillingInfoIsValid()
        {
            bool isValid = true;
            this.billingMsgBoxText = "Invalid entries.\n";
            string[] expiry = this.expiryTextBox.Text.Split('/');
            char[] ccNum = this.creditCardNumTextBox.Text.ToCharArray();

            // check that all the information has been filled out
            #region Null Checks
            if (this.cardHolderTextBox.Text == "")
            {
                this.billingMsgBoxText += "Please enter the credit card holder's name.\n";
                isValid = false;
            }
            if (this.creditTypeComboBox.SelectedItem == null)
            {
                this.billingMsgBoxText += "Please choose a credit card type.\n";
                isValid = false;
            }
            if (this.creditCardNumTextBox.Text == "")
            {
                this.billingMsgBoxText += "Please enter a valid credit card number.\n";
                isValid = false;
            }
            if (this.securityNumTextBox.Text == "")
            {
                this.billingMsgBoxText += "Please enter the three digit security number.\n";
                isValid = false;
            }
            if (this.expiryTextBox.Text == "")
            {
                this.billingMsgBoxText += "Please enter a valid expiry date: mm/yy\n";
                isValid = false;
            }
            #endregion

            // check for formatting
            //TODO: bug when you fill out guest info and try to save on billing info
            #region Format Checks
            if (this.creditTypeComboBox.SelectedItem.ToString().Equals("Visa") &&
                (ccNum[0] != 4 || ccNum.Length != 13 || ccNum.Length != 16))
            {
                this.billingMsgBoxText += "Please enter a valid credit card number.\n";
                isValid = false;
            }
            if (this.creditTypeComboBox.SelectedItem.ToString().Equals("Mastercard") &&
                (ccNum[0] != 5 || ccNum[1] < 1 || ccNum[1] > 5 || ccNum.Length != 16))
            {
                this.billingMsgBoxText += "Please enter a valid credit card number.\n";
                isValid = false;
            }
            if (this.creditTypeComboBox.SelectedItem.ToString().Equals("American Express") &&
                (ccNum[0] != 3 || ccNum[1] != 4 || ccNum[1] != 7 || ccNum.Length != 15))
            {
                this.billingMsgBoxText += "Please enter a valid credit card number.\n";
                isValid = false;
            }
            if (expiry[0].Length != 2 || expiry[1].Length != 2)
            {
                this.billingMsgBoxText += "Please enter a valid expiry date: mm/yy\n";
                isValid = false;
            }
            #endregion

            return isValid;
        }
        #endregion

    }
}
