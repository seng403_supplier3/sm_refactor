﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for EmployeeList.xaml
    /// </summary>
    public partial class EmployeeList : UserControl
    {
        MainWindow mainWin;
        EmployeeManager empMan;

        List<Expander> expanderList;
        List<EmployeeObj> allEmployees;

        public bool deleteFlag = false;

        public EmployeeList(MainWindow main)
        {
            InitializeComponent();

            this.mainWin = main;
            this.empMan = new EmployeeManager();

            this.expanderList = new List<Expander>();

            PopulateList();
        }

        public void UpdateList()
        {
            this.stackList.Children.Clear();
            this.expanderList.Clear();
            this.allEmployees = null;

            PopulateList();
        }

        private void PopulateList()
        {
            this.stackList.Children.Clear(); // allows for updating the list

            this.allEmployees = this.empMan.GetAllEmployees();

            foreach (EmployeeObj emp in allEmployees)
            {
                ArrayList info = emp.EmployeeInfo;
                Expander exp = new Expander();

                exp.Content = info[1] + ", " + info[0] + "\n" + info[2];

                this.stackList.Children.Add(exp);
                this.expanderList.Add(exp);
            }

            SetExpanderProperties();
        }

        private void SetExpanderProperties()
        {
            foreach (Expander exp in this.expanderList)
            {
                exp.IsExpanded = true;

                exp.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
                exp.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
                exp.FontSize = 14;

                exp.MouseDoubleClick += new MouseButtonEventHandler(empExpander_DoubleClick);
                exp.MouseEnter += new MouseEventHandler(exp_MouseEnter);
                exp.MouseLeave += new MouseEventHandler(exp_MouseLeave);
            }
        }

        void exp_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Expander).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
            (sender as Expander).Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
        }

        void exp_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Expander).Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#486b98"));
            (sender as Expander).Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#d7e9ff"));
        }

        public void empExpander_DoubleClick(object sender, RoutedEventArgs e)
        {
            // check which button is being clicked
            foreach (Expander exp in this.expanderList)
            {
                if (exp == sender as Expander)
                {
                    // edit
                    if (!deleteFlag)
                    {
                        int index = this.expanderList.IndexOf(exp);

                        this.mainWin.empForm.SetEmployeeToEdit(this.allEmployees[index].EmployeeInfo);

                        this.mainWin.empForm.Visibility = Visibility.Visible;
                        Canvas.SetZIndex(this, -1);
                    }
                    // cancel
                    else if (deleteFlag)
                    {
                        int index = this.expanderList.IndexOf(exp);
                        // show confirmation window
                        string info = exp.Content.ToString();

                        MessageBoxResult result = MessageBox.Show("Delete the following employee? \n" + 
                            info, "Confirm Cancellation", MessageBoxButton.YesNo);

                        if (result == MessageBoxResult.Yes)
                        {
                            this.stackList.Children.Remove(exp);
                            this.expanderList.RemoveAt(index);

                            this.empMan.DeletEmployee(Convert.ToInt32(this.allEmployees[index].EmployeeInfo[3]));
                            this.allEmployees.RemoveAt(index);

                            this.deleteFlag = false;
                            break;
                        }
                    }
                }
            }
        }

        private void lastNameButton_Click(object sender, RoutedEventArgs e)
        {
            // sort list by employee last name
        }

        private void typeButton_Click(object sender, RoutedEventArgs e)
        {
            // sortlist by employee type (Managers first)
        }

        private void EmployeeList_VisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Visibility == Visibility.Visible)
            {
                UpdateList();
                if (!deleteFlag)
                    this.employeeListLabel.Content = "Employee List: EDIT Employee";
                else if (deleteFlag)
                    this.employeeListLabel.Content = "Employee List: DELETE Employee";
            }
        }
    }
}
