﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for ChangePasswordForm.xaml
    /// </summary>
    public partial class ChangePasswordForm : UserControl
    {
        MainWindow mainWin;
        EmployeeManager empMan;

        public ChangePasswordForm(MainWindow main)
        {
            InitializeComponent();

            this.mainWin = main;
            this.empMan = new EmployeeManager();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            this.mainWin.mainBookingDash.Visibility = Visibility.Visible;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            string currPass = this.currentPasswordBox.Password.ToString();

            // check if current password is valid
            if (currPass.Equals(this.mainWin.loggedInEmp.EmployeeInfo[4]))
            {
                string newPass = this.newPasswordBox.Password.ToString();
                string confirmPass = this.confirmPasswordBox.Password.ToString();

                // check that passwords are equal
                if (newPass.Equals(confirmPass))
                {
                    this.mainWin.loggedInEmp.EmployeeInfo[4] = newPass;
                    this.empMan.EditEmployee(this.mainWin.empID, this.mainWin.loggedInEmp.EmployeeInfo);

                    MessageBox.Show("Password changes have been saved!");

                    this.Visibility = Visibility.Hidden;
                    this.mainWin.mainBookingDash.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("Passwords do not match!");

                    this.newPasswordBox.Password = "";
                    this.confirmPasswordBox.Password = "";
                }
            }
            else
            {
                MessageBox.Show("Current Password is incorrect.\n\nPlease try again.");

                this.currentPasswordBox.Password = "";
                this.newPasswordBox.Password = "";
                this.confirmPasswordBox.Password = "";
            }
        }
    }
}
