﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Charts;
using Color = System.Drawing.Color;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for TotalSalesGraph.xaml
    /// </summary>
    public partial class TotalSalesGraph : UserControl
    {
        string[] yearArray;

        MainWindow mainWindow;
        ComboBox yearComboBox;
        //chart controls
        ChartControl fullStackedBarChart;
        XYDiagram2D diagram;
        BarStackedSeries2D series1;
        BarStackedSeries2D series2;
        public TotalSalesGraph(MainWindow p)
        {
            fullStackedBarChart = new ChartControl();
            diagram = new XYDiagram2D();

            fullStackedBarChart.Diagram = diagram;
            series1 = new BarStackedSeries2D()
            {
                ValueScaleType = ScaleType.Numerical
            };
            series2 = new BarStackedSeries2D()
            {
                ValueScaleType = ScaleType.Numerical
            };
            diagram.Series.Add(series1);
            diagram.Series.Add(series2);

            InitializeComponent();
            this.mainWindow = p;
            yearComboBox = new ComboBox();

            this.stackPanel.Children.Add(fullStackedBarChart);

            stackPanel.Width = Double.NaN;
            stackPanel.Height = Double.NaN;

            grid.Width = Double.NaN;
            grid.Height = Double.NaN;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //ComboBox yearComboBox = new ComboBox();
            //yearComboBox.Width = 200;
            //yearComboBox.Height = 30;
            //this.Content = yearComboBox;
            //AddYearsToComboBox();

            /*
            List<string> yearList = new List<string>();
            yearList.Add("2012");
            yearList.Add("2013");
            yearList.Add("2014");

            foreach (string str in yearList) // Generate the objects from our list of strings.
            {
                yearComboBox.Items.Add(str); // Add each newly constructed item to our NAMED combobox.
            }
            this.grid.Children.Add(yearComboBox);
            */




            var db = new SmartManagerDataClassesDataContext();
            var query = (from d in db.InvoiceDetails
                         select d.LineItemDate.Year).Distinct().ToArray();
            //convert all entries in query array from ints to strings
            yearArray = query.Select(x => x.ToString()).ToArray();

            /*
            foreach (var elem in query)
            {
                //yearComboBox.Items.Add(elem);
                yearBox.Items.Add(elem.ToString());
            }
            */


            //this.stackPanel.Children.Add(yearComboBox);
            yearBox.ItemsSource = yearArray;

            // for demo purpose only - change to user input for year
            //CreateStackedRevenueGraph("2013");
            CreateStackedRevenueGraph(query.ElementAt(0).ToString());

            //yearComboBox.SelectionChanged += new SelectionChangedEventHandler(yearComboBox_SelectionChanged);
            yearBox.SelectionChanged += new SelectionChangedEventHandler(yearBox_SelectionChanged);
        }
        private void yearBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //string year = (e.AddedItems[0] as ComboBoxItem).Content as string;           
            //ComboBoxItem cbi = (ComboBoxItem)yearComboBox.SelectedItem;
            //string year = ((ComboBoxItem)yearBox.SelectedItem).Content.ToString();
            string year = yearBox.SelectedItem.ToString();
            //RemoveSeries();
            CreateStackedRevenueGraph(year);
        }

        /*
                void yearComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
                {
                    string year = (e.AddedItems[0] as ComboBoxItem).Content as string;           
                    ComboBoxItem cbi = (ComboBoxItem)yearComboBox.SelectedItem;
                    RemoveSeries();
                    CreateStackedRevenueGraph(year);
                }
        */
        /// <summary>
        /// Used to get monthly revenue totals (excludes taxes) based on revenue type requested for the 
        /// bar graph.
        /// Revenue types are: Room charges or all other charges. All monthly totals 
        /// do not include any taxes.
        /// 
        /// Revenue come from the InvoiceDetails table for invoices that have been paid only
        /// (not  future booked items). i.e. only guests that have already checked out.
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="chargeType"></param>
        /// <returns></returns>
        private double GetMonthlyRevenueByType(string month, string year, string chargeType)
        {
            //Convert month and year to SQL DateTime format
            DateTime fromDate = Convert.ToDateTime(month + "/1/" + year);
            DateTime toDate = Convert.ToDateTime(month + "/28/" + year);
            var db = new SmartManagerDataClassesDataContext();
            // All room charges for month
            if (chargeType.ToLower() == "room charge")
            {
                var q5 = from d in db.InvoiceDetails
                         where d.VDelDate == null
                         join i in db.Invoices on d.InvoiceID equals i.InvoiceID
                         where i.VDelDate == null
                         join r in db.RoomBookings on i.RoomBookingID equals r.RoomBookingID
                         where
                             r.VDelDate == null &&
                             r.CheckedOutDate != null &&
                             (d.LineItemDate.Month == fromDate.Month || d.LineItemDate.Month == toDate.Month) &&
                             (d.LineItemDate.Year == fromDate.Year || d.LineItemDate.Year == toDate.Year)
                         where d.LineItemDescription.Contains(("Room charge").ToLower())
                         group d by new { d.LineItemDate.Month, d.LineItemDate.Year }
                             into s
                             select new
                             {
                                 subtotal = s.Sum(d => d.LineItemCharge)
                             };

                double monthTotal = 0;
                foreach (var elements in q5)
                {
                    monthTotal = (double)elements.subtotal;
                }
                return monthTotal;
            }
            else
            {
                // All other charges, excluding taxes
                var q5 = from d in db.InvoiceDetails
                         where d.VDelDate == null
                         join i in db.Invoices on d.InvoiceID equals i.InvoiceID
                         where i.VDelDate == null
                         join r in db.RoomBookings on i.RoomBookingID equals r.RoomBookingID
                         where
                             r.VDelDate == null &&
                             r.CheckedOutDate != null &&
                             (d.LineItemDate.Month == fromDate.Month || d.LineItemDate.Month == toDate.Month) &&
                             (d.LineItemDate.Year == fromDate.Year || d.LineItemDate.Year == toDate.Year)
                         where
                             !d.LineItemDescription.Contains(("Room charge").ToLower()) &&
                             !d.LineItemDescription.Contains(("tax").ToLower())
                         group d by new { d.LineItemDate.Month, d.LineItemDate.Year }
                             into s
                             select new
                             {
                                 subtotal = s.Sum(d => d.LineItemCharge)
                             };

                double monthTotal = 0;
                foreach (var elements in q5)
                {
                    monthTotal = (double)elements.subtotal;
                }
                return monthTotal;
            }
        }

        /// <summary>
        /// Create stacked revenue graph for year chosen.
        /// Revenue is segregated into 2  types: hotel room charges and all other
        /// invoiced charges. All taxes are excluded from these values.
        /// </summary>
        /// <param name="year"></param>
        private void CreateStackedRevenueGraph(string year)
        {

            string[] strMonthValue = { "Ja", "Fe", "Mr", "Ap", "Ma", "Jn", "Jl", "Au", "Se", "Oc", "No", "De" };

            // Create a new chart.
            //var fullStackedBarChart = new ChartControl();
            fullStackedBarChart.Width = Double.NaN;
            fullStackedBarChart.Height = Double.NaN;

            series1.Points.Clear();
            series2.Points.Clear();

            //var diagram = new XYDiagram2D();
            //fullStackedBarChart.Diagram = diagram;

            // Create two full-stacked bar series.
            /*
            var series1 = new BarStackedSeries2D()
            {
                ValueScaleType = ScaleType.Numerical
            };
            var series2 = new BarStackedSeries2D()
            {
                ValueScaleType = ScaleType.Numerical
            };
           */
            //Add both series to diagram
            //diagram.Series.Add(series1);
            //diagram.Series.Add(series2);

            //TODO: total amt for y-axis, title
            //diagram.AxisY.NumericOptions.Format = NumericFormat.Currency;
            //diagram.AxisY.NumericOptions.Precision = 1;

            //diagram.AxisY.NumericOptions.Format = NumericFormat.Number;
            //series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Number;
            //series2.PointOptions.ValueNumericOptions.Format = NumericFormat.Number;
            //((XYDiagram2D)fullStackedBarChart.Diagram).AxisY.NumericOptions.Format = NumericFormat.Number;

            // Add points to series.
            for (var i = 1; i <= 12; i++)
            {
                series1.Points.Add(new SeriesPoint(strMonthValue[i - 1] + " " + year, GetMonthlyRevenueByType(i.ToString(), year, "Room charge")));
            }

            for (var i = 1; i <= 12; i++)
            {
                series2.Points.Add(new SeriesPoint(strMonthValue[i - 1] + " " + year, GetMonthlyRevenueByType(i.ToString(), year, "Other")));
            }

            //Set the display names for tooltip
            series1.DisplayName = "Room Charge Revenue";
            series2.DisplayName = "Other Revenue";
            series1.ShowInLegend = true;
            series2.ShowInLegend = true;
            series1.LabelsVisibility = true;
            series2.LabelsVisibility = true;

            // Add the chart to the diagram.
            //this.Content = fullStackedBarChart;




        }

        private void AddYearsToComboBox()
        {

            //var db = new DataClasses1DataContext();
            //var query = from d in db.InvoiceDetails
            /*where d.VDelDate == null
            join i in db.Invoices on d.InvoiceID equals i.InvoiceID
            where i.VDelDate == null
            join r in db.RoomBookings on i.RoomBookingID equals r.RoomBookingID
            where
                r.VDelDate == null &&
                r.CheckedOutDate != null
             */
            //            select d.LineItemDate.Year;
            //select new Item { Year = d.LineItemDate.Year }.ToList<Item>();

            //yearComboBox.Items.Add(query);

            List<string> yearList = new List<string>();
            yearList.Add("2012");
            yearList.Add("2013");
            yearList.Add("2014");

            foreach (string str in yearList) // Generate the objects from our list of strings.
            {
                yearComboBox.Items.Add(str); // Add each newly constructed item to our NAMED combobox.
            }

        }

        public void RemoveSeries()
        {
            diagram.Series.Remove(series1);
            diagram.Series.Remove(series2);
        }

        public void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String str = ((sender as ListBox).SelectedItem as ListBoxItem).Content.ToString();
            RemoveSeries();
            CreateStackedRevenueGraph(str);
        }
        public void SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            RemoveSeries();
            //CreateStackedRevenueGraph(
            //refresh graph
            //chartControl.RefreshData
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {

        }


    }
}
