﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace SmartManager.UserControls
{
    /// <summary>
    /// Interaction logic for SearchEmployeeResults.xaml
    /// </summary>
    public partial class SearchEmployeeResults : UserControl
    {
        MainWindow mainWindow;
        string searchStr;

        EmployeeManager employeeMan;

        public SearchEmployeeResults(MainWindow p)
        {
            InitializeComponent();
            this.mainWindow = p;

            this.employeeMan = new EmployeeManager();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            this.mainWindow.mainBookingDash.Visibility = Visibility.Visible;

            //this.searchResultsTitleText.Content = this.searchResultsTitleText.Content.ToString().Split(':')[0];

        }

        public void GetSearchString(string s)
        {
            this.searchStr = s;

            //this.searchResultsTitleText.Content += " " + this.searchStr;

            PopulateList();
        }

        public void PopulateList()
        {
            this.listPanel.Children.Clear();

            String[] names = this.searchStr.Split(' ');
            List<EmployeeObj> employeeList = this.employeeMan.searchEmployees(names);

            //For each employee returned, we grab their first and last names from
            //the employee object.
            foreach (EmployeeObj employee in employeeList)
            {
                Button button = new Button();
                button.Content = employee.EmployeeInfo[0] + " " + employee.EmployeeInfo[1];
                button.Height = 100;
                button.FontSize = 16;

                this.listPanel.Children.Add(button);
                this.cancelButton.Click += new RoutedEventHandler(cancelButton_Click);
            }
        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // do something here
        }
    }
}
