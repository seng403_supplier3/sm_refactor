﻿#pragma checksum "..\..\..\..\UserControls\BookingList.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "83304A2BF7CE3EEFACABCA52945C3012"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SmartManager.UserControls {
    
    
    /// <summary>
    /// BookingList
    /// </summary>
    public partial class BookingList : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SmartManager.UserControls.BookingList bookingListControl;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel dockPanel;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label bookingLabel;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid filterBar;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label sortText;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button lastNameButton;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button dateButton;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer stackScroll;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\UserControls\BookingList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stackList;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SmartManager;component/usercontrols/bookinglist.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\UserControls\BookingList.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.bookingListControl = ((SmartManager.UserControls.BookingList)(target));
            return;
            case 2:
            this.dockPanel = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 3:
            this.bookingLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.filterBar = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.sortText = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lastNameButton = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\..\..\UserControls\BookingList.xaml"
            this.lastNameButton.Click += new System.Windows.RoutedEventHandler(this.lastNameButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.dateButton = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\..\..\UserControls\BookingList.xaml"
            this.dateButton.Click += new System.Windows.RoutedEventHandler(this.dateButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.stackScroll = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 9:
            this.stackList = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

