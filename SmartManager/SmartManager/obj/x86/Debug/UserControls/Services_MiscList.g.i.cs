﻿#pragma checksum "..\..\..\..\UserControls\Services_MiscList.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "945E18EDA62096EDEE6698E7D0FCB0A5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SmartManager.UserControls {
    
    
    /// <summary>
    /// Services_MiscList
    /// </summary>
    public partial class Services_MiscList : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SmartManager.UserControls.Services_MiscList services_MiscListControl;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel dockPanel;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid mainBar;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label invoiceTitleText;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelButton;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid footerBar;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button saveButton;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label informationLabel;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid infoGrid;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid miscInfo;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label serviceName;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox serviceNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label serviceDescription;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox serviceDescriptionTextBox;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label serviceCharge;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\..\UserControls\Services_MiscList.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox serviceChargeTextBox;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SmartManager;component/usercontrols/services_misclist.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\UserControls\Services_MiscList.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.services_MiscListControl = ((SmartManager.UserControls.Services_MiscList)(target));
            return;
            case 2:
            this.dockPanel = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 3:
            this.mainBar = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.invoiceTitleText = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.cancelButton = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\..\..\UserControls\Services_MiscList.xaml"
            this.cancelButton.Click += new System.Windows.RoutedEventHandler(this.CancelButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.footerBar = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.saveButton = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\..\UserControls\Services_MiscList.xaml"
            this.saveButton.Click += new System.Windows.RoutedEventHandler(this.SaveButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.informationLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.infoGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.miscInfo = ((System.Windows.Controls.Grid)(target));
            return;
            case 11:
            this.serviceName = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.serviceNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.serviceDescription = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.serviceDescriptionTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.serviceCharge = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.serviceChargeTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

