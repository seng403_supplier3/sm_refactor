﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager
{
    class RoomManager
    {
        #region Instances and Class Variables

        private IDBHandler dbh;
        private RoomObj room;

        #endregion


        #region Constructor

        public RoomManager()
        {
            dbh = new DBHandler();
        }

        #endregion


        #region Main Methods

        public void SetRoom(ArrayList info)
        {
            SetRoomInfo(info);

            dbh.AddRoom(GetRoomInfo());
        }

        public RoomObj GetRoom(int roomNum, String roomType)
        {
            // TODO Error handling when retrieving incorrect room
            return dbh.GetRoom(roomNum, roomType);
        }

        public void DeleteRoom(ArrayList info)
        {
            RoomObj room = new RoomObj(info);

            dbh.DeleteRoom(room);
        }

        public List<RoomObj> GetAvailRooms(DateTime start, DateTime end)
        {
            return dbh.GetAvailRooms(start, end);
        }

        public List<RoomObj> GetOccupiedRooms(DateTime start, DateTime end)
        {
            return dbh.GetOccupiedRooms(start, end);
        }

        #endregion


        #region Getters/Setters

        public RoomObj GetRoomInfo()
        {
            return this.room;
        }

        public void SetRoomInfo(ArrayList info)
        {
            this.room = new RoomObj(info);
        }

        #endregion
    }
}