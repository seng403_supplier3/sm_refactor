﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager.Functionality
{
    class ServiceManager
    {
        #region Instances and Class Variables
        private IDBHandler dbh;
        #endregion

        #region Constructor / Main Methods
        /// <summary>
        /// constructor
        /// </summary>
        public ServiceManager()
        {
            dbh = new DBHandler();
        }
        #endregion

        #region Main Methods

        /// <summary>
        /// Sets the service
        /// </summary>
        /// <param name="serviceInfo"></param>
        public void setService(ArrayList serviceInfo)
        {
            ServiceObj service = new ServiceObj(serviceInfo);
            dbh.AddService(service);
        }

       /// <summary>
       /// gets the service
       /// </summary>
       /// <param name="serviceName"></param>
       /// <returns></returns>
        public ServiceObj getService(String serviceName)
        {
            ServiceObj service = dbh.GetService(serviceName);
            return service;
        }

        /// <summary>
        /// edits the service
        /// </summary>
        /// <param name="serviceInfo"></param>
        public void editService(ArrayList serviceInfo)
        {
            ServiceObj service = new ServiceObj(serviceInfo);
            dbh.EditService(service);
        }

        /// <summary>
        /// deletes the service
        /// </summary>
        /// <param name="serviceInfo"></param>
        public void DeleteService(ArrayList serviceInfo)
        {
            ServiceObj service = new ServiceObj(serviceInfo);
            dbh.DeleteService(service);
        }
        
        /// <summary>
        /// removes the service
        /// </summary>
        /// <param name="serviceInfo"></param>
        public void removeService(ArrayList serviceInfo)
        {
            ServiceObj service = new ServiceObj(serviceInfo);
            dbh.RemoveService(service);
        }

        /// <summary>
        /// returns a list of all available services
        /// </summary>
        /// <returns></returns>
        public List<ServiceObj> GetAllServices()
        {
            return dbh.GetAllAvailServices();
        }
        #endregion

        /// <summary>
        /// returns a list fo all avial by types
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<ServiceObj> GetAllSType(String name)
        {
            return dbh.GetAllTypeServices(name);
        }

    }
}
