﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartManager
{
    class BookingManager
    {
        #region Instances and Class Variables
        private IDBHandler dbh;
        private BookingObj booking;
        private RoomObj room;
        private GuestObj guest;

        #endregion

        #region Constructor / Main Methods

        public BookingManager()
        {
            dbh = new DBHandler();
        }

        /// <summary>
        /// SetBooking calls a method to encapsulate input data into object  
        /// and passes the object to the DBHandler that stores it into the DB
        /// </summary>
        /// <param name="bookingInfo"> 
        /// Expects the following format:
        /// [RoomBookingID, RoomID, GuestID, StartBookDate, EndBookDate, BillingRoomRate, CheckoutDate, ModifiedDate, VDelDate]
        /// </param>
        /// <param name="guestID"></param>
        /// <param name="roomInfo">
        /// Expects the following format:
        /// [RoomID, RoomNumber, TypeOfRoom, RackRate, VDelDate]
        /// </param>
        public void SetBooking(ArrayList bookingInfo, int guestID, ArrayList roomInfo)
        {
            booking = new BookingObj(bookingInfo);
            room = new RoomObj(roomInfo);

            dbh.AddBooking(booking, guestID, room);
        }

        /// <summary>
        /// GetBooking retrieves the booking info using the room info and guest info
        /// </summary>
        /// <returns> a bookingObj containing booking info </returns>
        public BookingObj GetBooking(ArrayList roomInfo, ArrayList guestInfo)
        {
            room = new RoomObj(roomInfo);
            guest = new GuestObj(guestInfo);
            booking = dbh.GetBooking(room, guest);

            if (booking == null)
                return null;
            else
                return booking;
        }

        public ArrayList[] GetCurrBookings()
        {
            ArrayList[] allbookings = dbh.GetCurrBookings();

            ArrayList[] bookingInfos = new ArrayList[2] { new ArrayList(), new ArrayList() };

            for (int i = 0; i < allbookings[0].Count; i++)
            {
                bookingInfos[0].Add((allbookings[0][i] as BookingObj).BookingInfo);
                bookingInfos[1].Add((allbookings[1][i] as GuestObj).GuestInfo);
            }

            return bookingInfos;
        }

        /// <summary>
        /// EditBooking will edit all information including the 
        /// guest, booking and room information
        /// </summary>
        public void EditBooking(ArrayList bookingInfo, ArrayList roomInfo, ArrayList guestInfo)
        {
            //booking = GetBooking(roomInfo, guestInfo);
            booking = new BookingObj(bookingInfo);

            room = new RoomObj(roomInfo);
            guest = new GuestObj(guestInfo);

            dbh.EditBooking(booking, room, guest);
        }

        public int GetBookingID(ArrayList bookingInfo)
        {
            BookingObj booking = new BookingObj(bookingInfo);

            return dbh.GetBookingID(booking);
        }

        public void DeleteBooking(ArrayList bookingInfo, ArrayList roomInfo, ArrayList guestInfo)
        {
            booking = GetBooking(roomInfo, guestInfo);

            room = new RoomObj(roomInfo);
            guest = new GuestObj(guestInfo);

            dbh.DeleteBooking(booking, room, guest);
        }

        public void DeleteBooking(ArrayList bookingInfo, ArrayList guestInfo)
        {
            booking = new BookingObj(bookingInfo);
            guest = new GuestObj(guestInfo);

            dbh.DeleteBooking(booking, guest);
        }

        /// <summary>
        /// CheckIn simply sets the CheckIn flag in the database, of the Guest who made the booking
        /// </summary>
        public void CheckIn(BookingObj booking, GuestObj guest)
        {
            if (booking == null)
            {
                throw new System.ArgumentNullException();
            }
            if (booking.BookingStatus == BookingObj.Status.CheckedIn)
            {
                throw new System.Exception();
            }
            else
            {
                booking.BookingStatus = BookingObj.Status.CheckedIn;
                dbh.CheckInGuest(booking, guest);
            }
        }

        /// <summary>
        /// Checkout is called either by front desk or automatically by the system.
        /// It will set the checked-out flag in the booking object passed,
        /// as well as setting the CheckOutDate for both the guest & booking object
        /// </summary>
        public void CheckOut(BookingObj booking, ArrayList roomInfo, ArrayList guestInfo)
        {
            guest = new GuestObj(guestInfo);

            if (booking.BookingStatus == BookingObj.Status.NotCheckedIn)
            {
                throw new System.Exception();
            }
            else
            {
                booking.BookingStatus = BookingObj.Status.NotCheckedIn;
                dbh.CheckOutGuest(booking, guest);
            }
        }

        #endregion


    }
}
