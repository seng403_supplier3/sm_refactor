﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartManager
{
    class GuestManager
    {

        #region Instances and Class Variables
        private IDBHandler dbh;
        #endregion


        #region Constructor / Main Methods
        /// <summary>
        /// constructor
        /// </summary>
        public GuestManager(){
            dbh = new DBHandler();
        }

        /// <summary>
        /// Adds the guestobj into the database
        /// </summary>
        /// <param name="guest"></param>
        public int SetGuest(ArrayList guestInfo)
        {
            GuestObj guest = new GuestObj(guestInfo);
            //TODO checks?
            //Modify address to simply a white space if it is null
            if (guestInfo[5] == null)
            {
                guestInfo[5] = "";
            }
            return dbh.AddGuest(guest);

        }


        /// <summary>
        /// gets the guest from database
        /// </summary>
        /// <param name="guest"></param>
        /// <returns></returns>
        public GuestObj GetGuest(ArrayList guestInfo)
        {
            //TODO not sure if the param is a guest
            //ALSO do i need to check for anything.
            GuestObj returnGuest = dbh.GetGuest((String) guestInfo[0], 
                (String)guestInfo[1]);

            return returnGuest;
        }

        /// <summary>
        /// edits the uest in the Datebase
        /// </summary>
        /// <param name="guest"></param>
        public void EditGuest(ArrayList guestInfo)
        {
            GuestObj guest = new GuestObj(guestInfo);

            dbh.EditGuest(guest);

        }

        /// <summary>
        /// deletes the guest in the database
        /// </summary>
        /// <param name="guest"></param>
        public void DeleteGuest(ArrayList guestInfo)
        {
            GuestObj guest = new GuestObj(guestInfo);
            dbh.DeleteGuest(guest);
        }

        public GuestObj SearchGuest(ArrayList name)
        {
            return dbh.GetGuest((String)name[1], (String)name[2]);
        }
        
        public List<ArrayList> searchGuests(String[] names)
        {
            List<ArrayList> list = new List<ArrayList>();

            foreach (var guest in dbh.SearchGuests(names))
            {
                list.Add(guest.GuestInfo);
            }

            return list;
        }
        #endregion
    }
}
