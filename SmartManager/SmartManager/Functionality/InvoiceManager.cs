﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartManager.Objects;
using System.Collections;

namespace SmartManager.Functionality
{
    class InvoiceManager
    {
        #region Instances and Class Variable
        private IDBHandler dbh;
        #endregion

        #region Constructor and Main Methods
        public InvoiceManager()
        {
            dbh = new DBHandler();
        }

        public void AddInvoice(ArrayList invoiceInfo, ArrayList bookingInfo, ArrayList guestInfo)
        {
            InvoiceObj invoiceObj = new InvoiceObj(invoiceInfo);
            BookingObj bookingObj = new BookingObj(bookingInfo);
            GuestObj guestObj = new GuestObj(guestInfo);

            dbh.AddInvoice(invoiceObj, bookingObj, guestObj);
        }

        public InvoiceObj GetInvoice(ArrayList bookingInfo, ArrayList guestInfo)
        {
            BookingObj bookingObj = new BookingObj(bookingInfo);
            GuestObj guestObj = new GuestObj(guestInfo);

            //return dbh.GetInvoice((String)guestObj.GuestInfo[0],
            //    (String)guestObj.GuestInfo[1]);

            return dbh.GetInvoice(bookingObj, guestObj);
        }

        public void EditInvoice(ArrayList bookingInfo, ArrayList guestInfo, InvoiceObj invoiceObj)
        {
            BookingObj bookingObj = new BookingObj(bookingInfo);
            GuestObj guestObj = new GuestObj(guestInfo);

            dbh.EditInvoice(bookingObj, guestObj, invoiceObj);
        
        }

        
        //TODO: do we need this? --> not for this iteration, this can be added later
        public void DeleteInvoice() { }
        #endregion 

        #region InvoiceDetails

        public void AddInvoiceDetail(ArrayList invoiceInfo, ArrayList invoiceDetInfo)
        {
            InvoiceObj invoiceObj = new InvoiceObj(invoiceInfo);
            InvoiceDetailsObj invoiceDetObj = new InvoiceDetailsObj(invoiceDetInfo);

            dbh.AddInvoiceDetails(invoiceObj, invoiceDetObj);
        }

        public void RemoveInvoiceDetail(ArrayList invoiceInfo)
        {
            
            InvoiceDetailsObj invoiceDetObj = new InvoiceDetailsObj(invoiceInfo);

            dbh.RemoveInvoiceDetails(invoiceDetObj);
        }

        public List<InvoiceDetailsObj> GetAllInvoices()
        {
            return dbh.GetInvoiceDetails();
        }
        #endregion
    }
}
