﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using SmartManager.Objects;

namespace SmartManager
{
    class EmployeeManager
    {
        #region Instances and Class Variables
        private DBHandler dbh;
        #endregion


        #region Constructor / Main Methods
        /// <summary>
        /// constructor for employeeManager
        /// </summary>
        public EmployeeManager(){
            dbh = new DBHandler();
        }

        /// <summary>
        /// Adds the EmployeeObj into the database
        /// </summary>
        /// <param name="employeeInfo">comtains info in the following order:
        /// [firstName, lastName, employeeType, password, isLoggedIn, vDelDate]
        /// </param>
        public int SetEmployee(ArrayList employeeInfo)
        {
            EmployeeObj employee = new EmployeeObj(employeeInfo);

            return dbh.AddEmployee(employee);
        }


        /// <summary>
        /// Gets the employee from database
        /// </summary>
        /// <param name="employee"></param>
        /// <returns>EmployeeObj with following information returned:
        /// [firstName, lastName, employeeType, employeeNumber, password, isLoggedIn]
        /// </returns>
        public EmployeeObj GetEmployee(int employeeNumber)
        {
            EmployeeObj returnEmployee = dbh.GetEmployee(employeeNumber);

            return returnEmployee;
        }


        /// <summary>
        /// Gets a list of all active employees (regular and managerial type)
        /// </summary>
        /// <returns></returns>
        public List<EmployeeObj> GetAllEmployees()
        {
            return dbh.GetAllEmployees();
        }

        /// <summary>
        /// edits the employee in the Datebase
        /// </summary>
        /// <param name="employeeNumber"> of the employee to edit</param>
        /// <param name="employeeInfo"></param>
        public void EditEmployee(int employeeNumber, ArrayList employeeInfo)
        {
            EmployeeObj employee = new EmployeeObj(employeeInfo);

            dbh.EditEmployee(employeeNumber, employee);

        }

        /// <summary>
        /// deletes the employee in the database
        /// </summary>
        /// <param name="employee"></param>
        public void DeletEmployee(int employeeNumber)
        {
            dbh.DeleteEmployee(employeeNumber);
        }
        
        /// <summary>
        /// Searches and returns list of employees
        /// </summary>
        /// <param name="names"></param>
        /// <returns></returns>
        public List<EmployeeObj> searchEmployees(String[] names)
        {
            List<EmployeeObj> listObj = new List<EmployeeObj>();

            foreach (var employee in dbh.SearchEmployees(names))
            {
                listObj.Add(employee);
            }

            return listObj;
        }

        /// <summary>
        /// Logs employee in using their employeenumber and password and
        /// returns an employee object if found in database. Otherwise, 
        /// return null
        /// </summary>
        /// <param name="employeeNumber"> is the username </param>
        /// <param name="password"></param>
        public int LogInEmployee(int employeeNumber, String password)
        {
            return dbh.LogInEmployee(employeeNumber, password);
        }

        public void LogOutEmployee(int employeeNumber, EmployeeObj employeeObj)
        {
            dbh.LogOutEmployee(employeeNumber, employeeObj);
        }

        #endregion
    }
}
