﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using SmartManager.Objects;

namespace SmartManager
{
    class DBHandler : IDBHandler
    {
        #region Booking Related Methods

        /// <summary>
        /// Adds a new booking with appropriate values
        /// </summary>
        /// <param name="bookingObj"></param>
        /// <param name="guestID"></param>
        /// <param name="roomID"></param>
        public void AddBooking(BookingObj bookingObj,
                                int guestID, RoomObj roomObj)
        {
            ArrayList bookingInfo = bookingObj.BookingInfo;

            var db = new SmartManagerDataClassesDataContext();

            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(roomObj.RoomInfo[0])
                             where r.TypeOfRoom.Equals(roomObj.RoomInfo[1])
                             where r.VDelDate.Equals(null)
                             select r).Single();

            var booking = new RoomBooking();

            //setting up the values for columns
            booking.RoomID = roomQuery.RoomID;
            booking.GuestID = guestID;
            booking.StartBookedDate = (DateTime)bookingInfo[0];
            booking.EndBookedDate = (DateTime)bookingInfo[1];
            booking.BillingRoomRate = Convert.ToDecimal(bookingInfo[2]);
            booking.CheckedOutDate = null; // to set in manager
            booking.ModifiedDate = DateTime.Now;
            booking.VDelDate = null;

            //process insert before submit
            db.RoomBookings.InsertOnSubmit(booking);

            //set up invoice detail room charge

            var invoicedet = new InvoiceDetail();
            //add rack rate
            invoicedet.LineItemCharge = Convert.ToDecimal(bookingInfo[2]);
            invoicedet.LineItemDescription = "Room Charges - Rack Rate";
            invoicedet.LineItemDate = DateTime.Now;
            
            //try a submit change
            try
            {
                db.SubmitChanges();

                //create an invoice after invoice is generatied
                int invoiceID = AddInvoice(booking.RoomBookingID, guestID);
                invoicedet.InvoiceID = invoiceID;

                db.InvoiceDetails.InsertOnSubmit(invoicedet);
                //submit invoice detail changes
                db.SubmitChanges();

            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        /// <summary>
        /// gets booking ID
        /// </summary>
        /// <param name="bookingObj"></param>
        /// <returns></returns>
        public int GetBookingID(BookingObj bookingObj)
        {
            var db = new SmartManagerDataClassesDataContext();
            ArrayList bookingInfo = bookingObj.BookingInfo;
            // query guest
            // TODO refactor if this works
            var query = (from b in db.RoomBookings
                         where b.RoomID.Equals(bookingInfo[0])  
                         where b.GuestID.Equals(bookingInfo[1])
                         select b).Single();
             
            return query.RoomBookingID;
        }
        /// <summary>
        /// GetBooking retrieves information from roomObj and
        /// guestObj to find a unique entry.
        /// </summary>
        /// <param name="bookingObj"></param>
        /// <param name="roomObj"></param>
        /// <param name="guestObj"></param>
        /// <returns>
        /// BookingObject
        /// </returns>
        public BookingObj GetBooking(RoomObj roomObj,
                                    GuestObj guestObj)
        {
            //ArrayList booking = bookingObj.BookingInfo;
            ArrayList room = roomObj.RoomInfo;
            ArrayList guest = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();

            // query guest
            // TODO refactor if this works
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guest[0]) &&
                                      g.LastName.Equals(guest[1])
                              select g).Single();

            int guestID = guestQuery.GuestID;

            // query room
            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(room[0]) &&
                                     r.TypeOfRoom.Equals(room[1])
                             select r).Single();

            int roomID = roomQuery.RoomID;

            // Assume guest books one room
            var bookingQuery = (from b in db.RoomBookings
                                where b.GuestID.Equals(guestID) &&
                                        b.RoomID.Equals(roomID) &&
                                        b.VDelDate.Equals(null)
                                select b).SingleOrDefault();

            // new instance of arrayList and bookingObj
            if (bookingQuery != null)
            {
                ArrayList newBookingInfo = new ArrayList();
                newBookingInfo.Add(bookingQuery.StartBookedDate.ToString());
                newBookingInfo.Add(bookingQuery.EndBookedDate.ToString());
                newBookingInfo.Add(bookingQuery.BillingRoomRate.ToString());

                BookingObj newBooking = new BookingObj(bookingQuery.RoomBookingID, newBookingInfo);
                newBooking.BookingID = bookingQuery.RoomBookingID;

                return newBooking;
            }
            else
                return null;
        }

        /// <summary>
        /// EditBooking retrieves information from roomObj and
        /// guestObj to find a unique entry.
        /// It then (essentially) adds a new booking with
        /// updated information
        /// </summary>
        /// <param name="bookingObj"></param>
        /// <param name="roomObj"></param>
        /// <param name="guestObj"></param>
        public void EditBooking(BookingObj bookingObj,
                                RoomObj roomObj,
                                GuestObj guestObj)
        {
            ArrayList booking = bookingObj.BookingInfo;
            ArrayList room = roomObj.RoomInfo;
            ArrayList guest = guestObj.GuestInfo;

            EditGuest(guestObj);

            var db = new SmartManagerDataClassesDataContext();
            //query guest
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guest[0]) &&
                                      g.LastName.Equals(guest[1])
                              select g).Single();

            int guestID = guestQuery.GuestID;

            // this is done in case the edit in booking also changed the room, so it needs to be updated
            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(room[0]) &&
                                     r.TypeOfRoom.Equals(room[1])
                             select r).SingleOrDefault();

            int roomID = roomQuery.RoomID;

            var bookingQuery = (from b in db.RoomBookings
                                join g in db.Guests on b.GuestID equals g.GuestID
                                //join r in db.Rooms on b.RoomID equals r.RoomID
                                where b.GuestID == guestID
                                where b.RoomID == roomID
                                where g.CheckedOutDate == null
                                where b.CheckedOutDate == null
                                where g.VDelDate == null
                                where b.VDelDate == null
                                //where r.VDelDate == null
                                select b).SingleOrDefault();

            // Assign new values
            if (bookingQuery != null)
            {
                bookingQuery.RoomID = roomID; // update room ID
                bookingQuery.StartBookedDate = Convert.ToDateTime(booking[0]);
                bookingQuery.EndBookedDate = Convert.ToDateTime(booking[1]);
                bookingQuery.BillingRoomRate = Convert.ToDecimal(booking[2]);
                bookingQuery.CheckedOutDate = null;
                bookingQuery.ModifiedDate = DateTime.Now;
                bookingQuery.VDelDate = null;

                db.SubmitChanges();
            }
        }

        /// <summary>
        /// DeleteBooking sets VDelDate value to the current time
        /// </summary>
        /// <param name="bookingObj"></param>
        /// <param name="roomObj"></param>
        /// <param name="guestObj"></param>
        public void DeleteBooking(BookingObj bookingObj,
                                RoomObj roomObj,
                                GuestObj guestObj)
        {
            ArrayList booking = bookingObj.BookingInfo;
            ArrayList room = roomObj.RoomInfo;
            ArrayList guest = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();

            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guest[0]) &&
                                      g.LastName.Equals(guest[1])
                              select g).Single();

            int guestID = guestQuery.GuestID;

            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(room[0]) &&
                                     r.TypeOfRoom.Equals(room[1])
                             select r).Single();

            int roomID = roomQuery.RoomID;

            // Assume guest books one room
            var bookingQuery = (from b in db.RoomBookings
                                where b.GuestID.Equals(guestID) &&
                                        b.RoomID.Equals(roomID) &&
                                        b.VDelDate.Equals(null)
                                select b).SingleOrDefault();

            if (bookingQuery != null)
                bookingQuery.VDelDate = DateTime.Now;

            db.SubmitChanges();
        }

        public void DeleteBooking(BookingObj bookingObj,
                                GuestObj guestObj)
        {
            ArrayList booking = bookingObj.BookingInfo;
            ArrayList guest = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();

            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guest[0]) &&
                                      g.LastName.Equals(guest[1])
                              select g).SingleOrDefault();

            int guestID = guestQuery.GuestID;

            // Assume guest books one room
            var bookingQuery = (from b in db.RoomBookings
                                join g in db.Guests on b.GuestID equals g.GuestID
                                where b.RoomBookingID.Equals(b.RoomBookingID) &&
                                        b.VDelDate.Equals(null)
                                select b).FirstOrDefault();

            if (bookingQuery != null)
                bookingQuery.VDelDate = DateTime.Now;

            db.SubmitChanges();
        }

        public void RemoveBooking(BookingObj bookingObj, RoomObj roomObj)
        {
            ArrayList booking = bookingObj.BookingInfo;
            ArrayList room = roomObj.RoomInfo;

            var db = new SmartManagerDataClassesDataContext();

            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(room[0]) &&
                                     r.TypeOfRoom.Equals(room[1])
                             select r).Single();

            int roomID = roomQuery.RoomID;

            // Assume guest books one room
            var bookingQuery = (from b in db.RoomBookings
                                where b.RoomID.Equals(roomID) &&
                                        b.VDelDate.Equals(null)
                                select b).SingleOrDefault();

            if (bookingQuery == null)
                return;

            db.RoomBookings.DeleteOnSubmit(bookingQuery);
            db.SubmitChanges();
        }

        public ArrayList[] GetCurrBookings()
        {
            ArrayList[] currBookings = new ArrayList[2] { new ArrayList(), new ArrayList() };

            var db = new SmartManagerDataClassesDataContext();
            var query = (from b in db.RoomBookings
                         //where (b.StartBookedDate >= DateTime.Now.Date) ||
                         //   (b.StartBookedDate < DateTime.Now.Date && b.EndBookedDate >= DateTime.Now.Date)
                         where b.VDelDate.Equals(null)
                         select b);

            foreach (var bk in query)
            {
                ArrayList info = new ArrayList();
                info.Add(bk.StartBookedDate);
                info.Add(bk.EndBookedDate);
                info.Add(bk.BillingRoomRate);
                info.Add(bk.RoomID);
                info.Add(bk.GuestID);

                BookingObj booking = new BookingObj(info);

                var gq = (from g in db.Guests
                          where g.GuestID.Equals(bk.GuestID)
                          select g).Single();

                ArrayList info2 = new ArrayList();
                info2.Clear();
                info2.Add(gq.FirstName);
                info2.Add(gq.LastName);
                info2.Add(gq.Email);
                info2.Add(gq.Phone);
                info2.Add(gq.Address1);
                info2.Add(gq.Address2);
                info2.Add(gq.City);
                info2.Add(gq.StateProv);
                info2.Add(gq.ZipPostalCode);
                info2.Add(gq.Country);
                info2.Add(gq.IsVip);
                info2.Add(gq.IsCheckedIn);
                info2.Add(gq.ArrivalDate);
                info2.Add(gq.DepartureDate);
                info2.Add(gq.BillingCardType);
                info2.Add(gq.BillingCardNumber);
                info2.Add(gq.BillingCardholderName);
                info2.Add(gq.BillingCardSecurityNumber);
                info2.Add(gq.BillingCardExpiration);
                GuestObj guest = new GuestObj(info2);

                currBookings[0].Add(booking);
                currBookings[1].Add(guest);
            }


            return currBookings;
        }
        #endregion


        #region Guest Related Methods

        /// <summary>
        /// Adds a guest with basic information
        /// </summary>
        /// <param name="guestObj"></param>
        public int AddGuest(GuestObj guestObj)
        {
            ArrayList guestInfo = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();

            var guest = new Guest();

            //setting up the values for columns
            guest.FirstName = guestInfo[0].ToString();
            guest.LastName = guestInfo[1].ToString();
            guest.Email = guestInfo[2].ToString();
            guest.Phone = guestInfo[3].ToString();
            guest.Address1 = guestInfo[4].ToString();
            guest.Address2 = guestInfo[5].ToString();
            guest.City = guestInfo[6].ToString();
            guest.StateProv = guestInfo[7].ToString();
            guest.ZipPostalCode = guestInfo[8].ToString();
            guest.Country = guestInfo[9].ToString();
            guest.IsVip = Convert.ToByte(guestInfo[10]);
            guest.IsCheckedIn = Convert.ToByte(guestInfo[11]);
            guest.ArrivalDate = Convert.ToDateTime(guestInfo[12]);
            guest.DepartureDate = Convert.ToDateTime(guestInfo[13]);
            guest.BillingCardType = guestInfo[14].ToString();
            guest.BillingCardNumber = guestInfo[15].ToString();
            guest.BillingCardholderName = guestInfo[16].ToString();
            guest.BillingCardSecurityNumber = guestInfo[17].ToString();
            guest.BillingCardExpiration = guestInfo[18].ToString();
            guest.CheckedOutDate = null;
            guest.VDelDate = null;

            //if no guests are found with the same first + last name, and VDelDate not set, insert
            if (!db.Guests.Any(g => g.FirstName == guestInfo[0].ToString()
                && g.LastName == guestInfo[1].ToString() && g.VDelDate == null))
            {


                //process insert before submit
                db.Guests.InsertOnSubmit(guest);

                //try a submit change
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception e)
                { Console.WriteLine(e.Message.ToString()); }

            }
            return guest.GuestID;
        }

        /// <summary>
        /// Retrieves information based on
        /// parameters.
        /// "" Used for blank areas
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public GuestObj GetGuest(String firstName, String lastName)
        {

            var db = new SmartManagerDataClassesDataContext();

            // query guest
            // TODO refactor if this works
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(firstName) &&
                                      g.LastName.Equals(lastName)
                              select g).SingleOrDefault();

            //handle no results found
            if (guestQuery == null)
            {
                return null;
            }

            // new instance of arrayList and guestObj
            ArrayList newGuestInfo = new ArrayList();
            newGuestInfo.Add(guestQuery.FirstName.ToString());
            newGuestInfo.Add(guestQuery.LastName.ToString());
            newGuestInfo.Add(guestQuery.Email.ToString());
            newGuestInfo.Add(guestQuery.Phone.ToString());
            newGuestInfo.Add(guestQuery.Address1.ToString());
            newGuestInfo.Add(guestQuery.Address2.ToString());
            newGuestInfo.Add(guestQuery.City.ToString());
            newGuestInfo.Add(guestQuery.StateProv.ToString());
            newGuestInfo.Add(guestQuery.ZipPostalCode.ToString());
            newGuestInfo.Add(guestQuery.Country.ToString());
            newGuestInfo.Add(guestQuery.IsVip);
            newGuestInfo.Add(guestQuery.IsCheckedIn);
            newGuestInfo.Add(guestQuery.ArrivalDate.ToString());
            newGuestInfo.Add(guestQuery.DepartureDate.ToString());
            newGuestInfo.Add(guestQuery.BillingCardType.ToString());
            newGuestInfo.Add(guestQuery.BillingCardNumber.ToString());
            newGuestInfo.Add(guestQuery.BillingCardholderName.ToString());
            newGuestInfo.Add(guestQuery.BillingCardSecurityNumber.ToString());
            newGuestInfo.Add(guestQuery.BillingCardExpiration.ToString());

            //conversion to DateTimes to match formatting of DateTime return by database
            DateTime arrivalConvert = Convert.ToDateTime(newGuestInfo[12]);
            newGuestInfo[12] = arrivalConvert;
            DateTime departureConvert = Convert.ToDateTime(newGuestInfo[13]);
            newGuestInfo[13] = departureConvert;

            GuestObj newBooking = new GuestObj(guestQuery.GuestID, newGuestInfo);

            return newBooking;
        }

        /// <summary>
        /// Edits guest values
        /// </summary>
        /// <param name="guestObj"></param>
        public void EditGuest(GuestObj guest)
        {
            //ArrayList guest = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();
            // query guest
            // TODO refactor if this works
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guest.GuestInfo[0].ToString()) &&
                                      g.LastName.Equals(guest.GuestInfo[1].ToString())
                              select g).SingleOrDefault();


            // int guestID = guestQuery.GuestID;

            // Assign new values
            guestQuery.FirstName = guest.GuestInfo[0].ToString();
            guestQuery.LastName = guest.GuestInfo[1].ToString();
            guestQuery.Email = guest.GuestInfo[2].ToString();
            guestQuery.Phone = guest.GuestInfo[3].ToString();
            guestQuery.Address1 = guest.GuestInfo[4].ToString();

            if (guest.GuestInfo[5] != null)
                guestQuery.Address2 = guest.GuestInfo[5].ToString();

            guestQuery.City = guest.GuestInfo[6].ToString();
            guestQuery.StateProv = guest.GuestInfo[7].ToString();
            guestQuery.ZipPostalCode = guest.GuestInfo[8].ToString();
            guestQuery.Country = guest.GuestInfo[9].ToString();
            guestQuery.IsVip = Convert.ToByte(guest.GuestInfo[10]);
            guestQuery.IsCheckedIn = Convert.ToByte(guest.GuestInfo[11]);
            guestQuery.ArrivalDate = Convert.ToDateTime(guest.GuestInfo[12]);
            guestQuery.DepartureDate = Convert.ToDateTime(guest.GuestInfo[13]);
            guestQuery.BillingCardType = guest.GuestInfo[14].ToString();
            guestQuery.BillingCardNumber = guest.GuestInfo[15].ToString();
            guestQuery.BillingCardholderName = guest.GuestInfo[16].ToString();
            guestQuery.BillingCardSecurityNumber = guest.GuestInfo[17].ToString();
            guestQuery.BillingCardExpiration = guest.GuestInfo[18].ToString();
            guestQuery.CheckedOutDate = null;
            guestQuery.VDelDate = null;

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }

        }

        /// <summary>
        /// Sets VDelDate to current time
        /// </summary>
        /// <param name="guestObj"></param>
        public DateTime DeleteGuest(GuestObj guestObj)
        {
            ArrayList guest = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();
            // query guest
            // TODO refactor if this works
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guest[0]) &&
                                      g.LastName.Equals(guest[1])
                              select g).Single();

            guestQuery.VDelDate = DateTime.Now;
            return Convert.ToDateTime(guestQuery.VDelDate);
        }

        public void RemoveGuest(GuestObj guestObj)
        {
            ArrayList guest = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();
            // query guest
            // TODO refactor if this works
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guest[0]) &&
                                      g.LastName.Equals(guest[1])
                              select g).SingleOrDefault();
            //checks if the guest in here exist 
            if (guestQuery == null)
            {
                //TODO MAYBE THROW ERROR?
                return;
            }
            db.Guests.DeleteOnSubmit(guestQuery);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        public void CheckInGuest(BookingObj bookingObj, GuestObj guestObj)
        {
            var db = new SmartManagerDataClassesDataContext();

            // query guest
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guestObj.GuestInfo[0].ToString()) &&
                                      g.LastName.Equals(guestObj.GuestInfo[1].ToString())
                              select g).SingleOrDefault();

            int guestID = guestQuery.GuestID;

            // bookingquery
            var bookingQuery = (from b in db.RoomBookings
                                where b.GuestID.Equals(guestID)
                                where b.StartBookedDate.Date.Equals(DateTime.Parse(bookingObj.BookingInfo[0].ToString()).Date)
                                where b.EndBookedDate.Date.Equals(DateTime.Parse(bookingObj.BookingInfo[1].ToString()).Date)
                                where b.VDelDate.Equals(null)
                                select b).SingleOrDefault();

            //checks if the guest in here exist 
            if (guestQuery == null || guestQuery.IsCheckedIn.Equals(0))
            {
                return;
            }

            guestQuery.IsCheckedIn = 1;

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }


        }

        public void CheckOutGuest(BookingObj bookingObj, GuestObj guestObj)
        {
            var db = new SmartManagerDataClassesDataContext();

            // query guest
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(guestObj.GuestInfo[0].ToString()) &&
                                      g.LastName.Equals(guestObj.GuestInfo[1].ToString())
                              select g).SingleOrDefault();

            int guestID = guestQuery.GuestID;

            // bookingquery
            var bookingQuery = (from b in db.RoomBookings
                                where b.GuestID.Equals(guestID)
                                where b.StartBookedDate.Date.Equals(DateTime.Parse(bookingObj.BookingInfo[0].ToString()).Date)
                                where b.EndBookedDate.Date.Equals(DateTime.Parse(bookingObj.BookingInfo[1].ToString()).Date)
                                where b.VDelDate.Equals(null)
                                select b).SingleOrDefault();

            //checks if the guest in here exist 
            if (guestQuery == null || guestQuery.IsCheckedIn.Equals(1))
            {
                return;
            }

            if (bookingQuery != null && guestQuery != null)
            {
                if (bookingQuery.CheckedOutDate == null)
                {
                    guestQuery.CheckedOutDate = (System.DateTime)guestObj.GuestInfo[18];
                    bookingQuery.CheckedOutDate = (System.DateTime)guestObj.GuestInfo[18];
                }

                else
                    throw new System.Exception();
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        public List<GuestObj> SearchGuests(String[] names)
        {
            var db = new SmartManagerDataClassesDataContext();
            ArrayList queryList = new ArrayList();
            //start query
            var queryAll = (from g in db.Guests
                            where g.FirstName.Equals(names[0]) || g.LastName.Equals(names[0])
                            select g);
            //should serach for all the names 
            foreach (var name in names)
            {
                var query = (from g in db.Guests
                             where g.FirstName.Equals(name) || g.LastName.Equals(name)
                             select g);

                queryAll = queryAll.Union(query);
            }

            //set up the List to be passed back
            List<GuestObj> guestList = new List<GuestObj>();
            foreach (var q in queryAll)
            {

                ArrayList info = new ArrayList();
                //testing only first name and last name
                info.Add(q.FirstName);
                info.Add(q.LastName);
                info.Add(q.Email);
                info.Add(q.Phone);
                info.Add(q.Address1);
                info.Add(q.Address2);
                info.Add(q.City);
                info.Add(q.StateProv);
                info.Add(q.ZipPostalCode);
                info.Add(q.Country);
                info.Add(q.IsVip);
                info.Add(q.IsCheckedIn);
                info.Add(q.ArrivalDate);
                info.Add(q.DepartureDate);
                info.Add(q.BillingCardType);
                info.Add(q.BillingCardNumber);
                info.Add(q.BillingCardholderName);
                info.Add(q.BillingCardSecurityNumber);
                info.Add(q.BillingCardExpiration);
                GuestObj guest = new GuestObj(info);

                guestList.Add(guest);
            }

            return guestList;

        }

        #endregion


        #region Invoice Related Methods


        public void AddInvoice(InvoiceObj invoiceObj, BookingObj bookingObj, GuestObj guestObj)
        {
            ArrayList invoiceInfo = invoiceObj.InvoiceInfo;
            ArrayList bookingInfo = bookingObj.BookingInfo;
            ArrayList guestInfo = guestObj.GuestInfo;

            var db = new SmartManagerDataClassesDataContext();

            var invoice = new Invoice();

            // grab guestID
            var query = (from g in db.Guests
                         join b in db.RoomBookings
                            on g.GuestID equals b.GuestID
                        where g.FirstName.Equals(guestInfo[0])
                        where g.LastName.Equals(guestInfo[1])
                        where b.StartBookedDate.Equals(bookingInfo[0])
                        where b.EndBookedDate.Equals(bookingInfo[1])
                        select b).SingleOrDefault();

            int bookingID = query.RoomBookingID;
            int guestID = query.GuestID;

            //setting up the values for columns
            invoice.GuestID = guestID;
            invoice.RoomBookingID = bookingID;
            invoice.SubTotal = Convert.ToInt32(invoiceInfo[2]);
            invoice.Taxes = Convert.ToInt32(invoiceInfo[3]);
            invoice.Total = Convert.ToInt32(invoiceInfo[4]);
            invoice.VDelDate = null;

            //process insert before submit
            db.Invoices.InsertOnSubmit(invoice);

            //try a submit change
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        //used for creating an invoice
        public int AddInvoice(int bookingID, int guestID)
        {
            var db = new SmartManagerDataClassesDataContext();

            var invoice = new Invoice();

            invoice.RoomBookingID = bookingID;
            invoice.GuestID = guestID;

            db.Invoices.InsertOnSubmit(invoice);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }

            return invoice.InvoiceID;

        }

        public InvoiceObj GetInvoice(BookingObj booking, GuestObj guest)
        {
            #region previouscode
            //var db = new SmartManagerDataClassesDataContext();

            //// query guest
            //var guestQuery = (from g in db.Guests
            //                  where g.FirstName.Equals(first) &&
            //                          g.LastName.Equals(last)
            //                  select g).SingleOrDefault();

            ////invoice query
            //var invoiceQuery = (from i in db.Invoices
            //                    where i.GuestID.Equals(guestQuery.GuestID)
            //                    select i).SingleOrDefault();

            ////new arrayList for invoiceObj
            //ArrayList newInvoiceInfo = new ArrayList();
            //newInvoiceInfo.Add(invoiceQuery.InvoiceID);
            //newInvoiceInfo.Add(invoiceQuery.GuestID);
            //newInvoiceInfo.Add(invoiceQuery.SubTotal);
            //newInvoiceInfo.Add(invoiceQuery.Taxes);
            //newInvoiceInfo.Add(invoiceQuery.Total);
            //newInvoiceInfo.Add(invoiceQuery.VDelDate);

            //InvoiceObj newInvoice = new InvoiceObj(invoiceQuery.InvoiceID, newInvoiceInfo);

            //return newInvoice;
            #endregion
            var db = new SmartManagerDataClassesDataContext();

            ArrayList blist = booking.BookingInfo;
            ArrayList glist = guest.GuestInfo;

            ArrayList invInfo = new ArrayList();
            
            ArrayList listOfDetails = new ArrayList();


            // get invoice
            var query = (from inv in db.Invoices
                         join rb in db.RoomBookings
                            on inv.GuestID equals rb.GuestID
                         join g in db.Guests
                            on rb.GuestID equals g.GuestID
                         where g.FirstName.Equals(glist[0])
                         where g.LastName.Equals(glist[1])
                         where rb.StartBookedDate.Equals(blist[0])
                         select inv).Single();

            // get invoice details
            var detQuery = (from d in db.InvoiceDetails
                            where d.InvoiceID.Equals(query.InvoiceID)
                            where d.VDelDate == null
                            select d);

            foreach (var invDet in detQuery)
            {
                ArrayList detInfo = new ArrayList();
                string[] temp = invDet.LineItemDescription.ToString().Split('-');
                detInfo.Add(temp[0]); // service type
                detInfo.Add(invDet.LineItemCharge); // price
                detInfo.Add(invDet.LineItemDate); // date charged
                detInfo.Add(temp[1]); // service description/details

                listOfDetails.Add(new InvoiceDetailsObj(detInfo));
            }

            invInfo.Add(query.InvoiceNumber);
            invInfo.Add(query.SubTotal);
            invInfo.Add(query.Taxes);
            invInfo.Add(query.Total);
            invInfo.Add(listOfDetails);

            InvoiceObj invoice = new InvoiceObj(invInfo);
            return invoice;
        }

        public void EditInvoice(BookingObj booking, GuestObj guest, InvoiceObj invoice)
        {

            var db = new SmartManagerDataClassesDataContext();

            ArrayList blist = booking.BookingInfo;
            ArrayList glist = guest.GuestInfo;

            //gets the invoice
            var query = (from inv in db.Invoices
                         join rb in db.RoomBookings
                            on inv.GuestID equals rb.GuestID
                         join g in db.Guests
                            on rb.GuestID equals g.GuestID
                         where g.FirstName.Equals(glist[0])
                         where g.LastName.Equals(glist[1])
                         where rb.StartBookedDate.Equals(blist[0])
                         select inv).Single();

            query.SubTotal = Convert.ToDecimal(invoice.InvoiceInfo[1]);
            query.Taxes = Convert.ToDecimal(invoice.InvoiceInfo[2]);
            query.Total = Convert.ToDecimal(invoice.InvoiceInfo[3]);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }


        }

        public void RemoveInvoice(int roombookingID, int guestID)
        {
            var db = new SmartManagerDataClassesDataContext();

            var invoiceQuery = (from i in db.Invoices
                                where i.GuestID.Equals(guestID)
                                where i.RoomBookingID.Equals(roombookingID)
                                select i).SingleOrDefault();

            if (invoiceQuery == null)
            {
                return;
            }
            db.Invoices.DeleteOnSubmit(invoiceQuery);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }

        }
        public void RemoveInvoice(String first, String last){
            var db = new SmartManagerDataClassesDataContext();
            var guestQuery = (from g in db.Guests
                              where g.FirstName.Equals(first) &&
                                      g.LastName.Equals(last)
                              select g).SingleOrDefault();
           
            var invoiceQuery = (from i in db.Invoices
                                where i.GuestID.Equals(guestQuery.GuestID)
                                select i).SingleOrDefault();

            if (invoiceQuery == null)
            {
                return;
            }
            db.Invoices.DeleteOnSubmit(invoiceQuery);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }
        }

        #region Invoice details
        
        public void AddInvoiceDetails(InvoiceObj invoiceObj, InvoiceDetailsObj invoiceDetailsObj)
        {
            ArrayList invoiceInfo = invoiceObj.InvoiceInfo;
            ArrayList invoiceDetailsInfo = invoiceDetailsObj.InvoiceDetailsInfo;

            var db = new SmartManagerDataClassesDataContext();

            #region old code
            //var roomQuery = (from r in db.Rooms
            //                 where r.RoomNumber.Equals(roomNum) &&
            //                         r.TypeOfRoom.Equals(roomType)
            //                 select r).Single();

            //var guestQuery = (from b in db.RoomBookings
            //                  join g in db.Guests
            //                      on b.GuestID equals g.GuestID
            //                  where b.RoomID == roomQuery.RoomID
            //                  select b).SingleOrDefault();

            //int guestID = guestQuery.GuestID;

            //var invoiceQuery = (from i in db.Invoices
            //                    where i.GuestID == guestID
            //                    select i).SingleOrDefault();

            //var invoiceDet = new InvoiceDetail();
            #endregion


            var invQuery = (from i in db.Invoices
                            where i.InvoiceNumber.Equals(invoiceInfo[0])
                            where i.VDelDate == null
                            select i).Single();


            var invoiceDet = new InvoiceDetail();
            //setting up the values for columns
            invoiceDet.InvoiceID = invQuery.InvoiceID;
            invoiceDet.LineItemDate = DateTime.Now.Date;
            invoiceDet.LineItemDescription = invoiceDetailsInfo[0].ToString(); // TODO change description
            invoiceDet.LineItemCharge = Convert.ToDecimal(invoiceDetailsInfo[1]);
            invoiceDet.VDelDate = null;

            //process insert before submit
            db.InvoiceDetails.InsertOnSubmit(invoiceDet);

            //try a submit change
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        /// <summary>
        /// removes the service charge on invoice
        /// </summary>
        /// <param name="invoiceDetObj"></param>
        public void RemoveInvoiceDetails(InvoiceDetailsObj invoiceDetObj)
        {
            
            ArrayList invd = invoiceDetObj.InvoiceDetailsInfo;

            var db = new SmartManagerDataClassesDataContext();

            #region old code
            //var roomQuery = (from r in db.Rooms
            //                 where r.RoomNumber.Equals(room[0]) &&
            //                         r.TypeOfRoom.Equals(room[1])
            //                 select r).Single();

            //var guestQuery = (from b in db.RoomBookings
            //                  join g in db.Guests
            //                      on b.GuestID equals g.GuestID
            //                  where b.RoomID == roomQuery.RoomID
            //                  select b).SingleOrDefault();

            //int guestID = guestQuery.GuestID;
#endregion

            var invoiceQuery = (from i in db.InvoiceDetails
                                 where i.VDelDate == null
                                 where i.LineItemDate.Equals(invd[2])
                                 where i.LineItemCharge.Equals(invd[1])
                                 where i.LineItemDescription.Equals(invd[0].ToString() + "-" + invd[3].ToString())
                                select i).FirstOrDefault();

            invoiceQuery.VDelDate = DateTime.Now.Date;
            
            

            db.SubmitChanges();

        }

        public List<InvoiceDetailsObj> GetInvoiceDetails()
        {
            var db = new SmartManagerDataClassesDataContext();

            var invoiceQuery = (from i in db.InvoiceDetails
                                where i.VDelDate == null
                                select i);

            List<InvoiceDetailsObj> allInvoices = new List<InvoiceDetailsObj>();

            foreach (var invoice in invoiceQuery)
            {
                ArrayList info = new ArrayList();
                info.Add(invoice.LineItemDate);
                info.Add(invoice.LineItemDescription);
                info.Add(invoice.LineItemCharge);

                InvoiceDetailsObj invoiceDetObj = new InvoiceDetailsObj(info);
                allInvoices.Add(invoiceDetObj);

            }

            return allInvoices;
        }

        #endregion

        #endregion


        #region Room RelatedMethods

        /// <summary>
        /// Adds a room with with appropriate values
        /// </summary>
        /// <param name="roomObj"></param>
        public int AddRoom(RoomObj roomObj)
        {

            ArrayList info = roomObj.RoomInfo;

            // adds room to DB
            var db = new SmartManagerDataClassesDataContext();

            var room = new Room();

            // set up values for columns
            room.RoomNumber = Convert.ToInt32(info[0].ToString());
            room.TypeOfRoom = info[1].ToString();
            room.RackRate = Convert.ToDecimal(info[2].ToString());
            room.VDelDate = null;

            db.Rooms.InsertOnSubmit(room);

            //db.SubmitChanges();

            try
            { db.SubmitChanges(); }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }

            return room.RoomID;


        }

        /// <summary>
        /// GetRoom queries base on a room number and type
        /// </summary>
        /// <param name="roomObj"></param>
        /// <returns>
        /// RoomObject
        /// </returns>
        public RoomObj GetRoom(int roomNum, String roomType)
        {
            var db = new SmartManagerDataClassesDataContext();

            // query room
            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(roomNum) &&
                                     r.TypeOfRoom.Equals(roomType) &&
                                     r.VDelDate.Equals(null)
                             select r).SingleOrDefault();

            if (roomQuery == null)
                return null;

            // new instance of roomList and room
            ArrayList newRoomInfo = new ArrayList();
            newRoomInfo.Add(roomQuery.RoomNumber.ToString());
            newRoomInfo.Add(roomQuery.TypeOfRoom.ToString());
            newRoomInfo.Add(roomQuery.RackRate.ToString());

            RoomObj newRoom = new RoomObj(roomQuery.RoomID, newRoomInfo);

            return newRoom;
        }

        public void EditRoom(RoomObj roomObj)
        {
            ArrayList room = roomObj.RoomInfo;

            var db = new SmartManagerDataClassesDataContext();

            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(room[0])
                             where r.TypeOfRoom.Equals(roomObj.RoomInfo[1])
                             select r).Single();

            // Assign new values
            roomQuery.RoomNumber = Convert.ToInt32(room[0]);
            roomQuery.TypeOfRoom = room[1].ToString();
            roomQuery.RackRate = Convert.ToDecimal(room[2].ToString());
            roomQuery.VDelDate = null;

            db.SubmitChanges();
        }

        /// <summary>
        /// Sets VDelDate to current time
        /// </summary>
        /// <param name="roomObj">which contains roomInfo containing:
        /// [roomNumber, roomType, rackRate]
        /// </param>
        public DateTime DeleteRoom(RoomObj roomObj)
        {
            ArrayList room = roomObj.RoomInfo;

            var db = new SmartManagerDataClassesDataContext();

            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(room[0])
                             where r.TypeOfRoom.Equals(roomObj.RoomInfo[1])
                             where r.VDelDate.Equals(null)
                             select r).SingleOrDefault();
           
            if (roomQuery == null)
                throw new System.Exception();

            roomQuery.VDelDate = DateTime.Now;

            db.SubmitChanges();

            return Convert.ToDateTime(roomQuery.VDelDate);
        }

        /// <summary>
        /// Removes the room from the database
        /// </summary>
        /// <param name="roomObj"></param>
        public void RemoveRoom(RoomObj roomObj)
        {

            ArrayList room = roomObj.RoomInfo;

            var db = new SmartManagerDataClassesDataContext();

            var roomQuery = (from r in db.Rooms
                             where r.RoomNumber.Equals(room[0])
                             where r.TypeOfRoom.Equals(roomObj.RoomInfo[1])
                             select r).SingleOrDefault();

            //check if empty
            if (roomQuery == null)
            {
                return;
            }
            db.Rooms.DeleteOnSubmit(roomQuery);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }

        }

        public List<RoomObj> GetAvailRooms(DateTime startDate, DateTime endDate)
        {
            List<RoomObj> allRooms = new List<RoomObj>();

            var db = new SmartManagerDataClassesDataContext();
            var query = (from r in db.Rooms
                         from b in db.RoomBookings
                         where r.RoomID.Equals(b.RoomID) &&
                            !((b.StartBookedDate >= startDate) && (b.StartBookedDate < endDate)) &&
                            !((b.EndBookedDate > startDate) && (b.EndBookedDate <= endDate))
                         select r);
            //.Concat
            //(from r2 in db.Rooms
            // from b2 in db.RoomBookings
            // where r2.RoomID.Equals(b2.RoomID)
            // select r2);

            foreach (var rm in query)
            {
                ArrayList info = new ArrayList();
                info.Add(rm.RoomNumber);
                info.Add(rm.TypeOfRoom);
                info.Add(rm.RackRate);

                RoomObj room = new RoomObj(info);
                allRooms.Add(room);
            }

            return allRooms;
        }

        public List<RoomObj> GetOccupiedRooms(DateTime startDate, DateTime endDate)
        {
            List<RoomObj> allRooms = new List<RoomObj>();

            var db = new SmartManagerDataClassesDataContext();
            var query = (from r in db.Rooms
                         from b in db.RoomBookings
                         from g in db.Guests
                         where r.RoomID.Equals(b.RoomID) &&
                            g.GuestID.Equals(b.GuestID) &&
                            ((b.StartBookedDate.Date == startDate.Date) ||
                            (b.EndBookedDate.Date == endDate.Date))
                         select r);
            //.Concat
            //(from r2 in db.Rooms
            // from b2 in db.RoomBookings
            // where r2.RoomID.Equals(b2.RoomID)
            // select r2);

            foreach (var rm in query)
            {
                ArrayList info = new ArrayList();
                info.Add(rm.RoomNumber);
                info.Add(rm.TypeOfRoom);
                info.Add(rm.RackRate);

                RoomObj room = new RoomObj(info);
                allRooms.Add(room);
            }

            return allRooms;
        }

        #endregion


        #region Service Related Methods

        /// <summary>
        /// Adds a service int the database
        /// </summary>
        /// <param name="serviceInfo"></param>
        /// <returns></returns>
        public int AddService(ServiceObj serviceInfo)
        {
            // adds service to DB
            var db = new SmartManagerDataClassesDataContext();

            var service = new Service();
            service.ServiceName = serviceInfo.ServiceInfo[0].ToString();
            service.ServiceType = serviceInfo.ServiceInfo[1].ToString();
            service.ServiceCharge = Convert.ToDecimal(serviceInfo.ServiceInfo[2]);
            service.VDelDate = null;

            db.Services.InsertOnSubmit(service);

            try
            { db.SubmitChanges(); }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
            return service.ServiceID;
        }

        /// <summary>
        /// gets the service from the data base
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public ServiceObj GetService(String serviceName)
        {
            var db = new SmartManagerDataClassesDataContext();

            var serviceQuery = (from r in db.Services
                                where r.ServiceName.Equals(serviceName)
                                select r).SingleOrDefault();
            ArrayList serviceInfo = new ArrayList();

            serviceInfo.Add(serviceQuery.ServiceName);
            serviceInfo.Add(serviceQuery.ServiceType);
            serviceInfo.Add(serviceQuery.ServiceCharge);
            serviceInfo.Add(serviceQuery.VDelDate);

            ServiceObj service = new ServiceObj(serviceInfo);
            return service;

        }

        /// <summary>
        /// Edits the service from database
        /// </summary>
        /// <param name="serviceObj"></param>
        public void EditService(ServiceObj serviceObj)
        {
            var db = new SmartManagerDataClassesDataContext();

            var serviceQuery = (from r in db.Services
                                where r.ServiceName.Equals(serviceObj.ServiceInfo[0].ToString())
                                select r).SingleOrDefault();

            serviceQuery.ServiceName = serviceObj.ServiceInfo[0].ToString();
            serviceQuery.ServiceType = serviceObj.ServiceInfo[1].ToString();
            serviceQuery.ServiceCharge = Convert.ToDecimal(serviceObj.ServiceInfo[2]);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        /// <summary>
        /// deletes the service and adds the date delted
        /// </summary>
        /// <param name="serviceObj"></param>
        public void DeleteService(ServiceObj serviceObj)
        {
            var db = new SmartManagerDataClassesDataContext();

            var serviceQuery = (from r in db.Services
                                where r.ServiceName.Equals(serviceObj.ServiceInfo[0].ToString())
                                select r).SingleOrDefault();

            serviceQuery.VDelDate = DateTime.Now;

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        /// <summary>
        /// removes from the database
        /// </summary>
        /// <param name="serviceObj"></param>
        public void RemoveService(ServiceObj serviceObj)
        {
            var db = new SmartManagerDataClassesDataContext();

            var serviceQuery = (from r in db.Services
                                where r.ServiceName.Equals(serviceObj.ServiceInfo[0].ToString())
                                select r).SingleOrDefault();


            //check if empty
            if (serviceQuery == null)
            {
                return;
            }
            db.Services.DeleteOnSubmit(serviceQuery);

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        /// <summary>
        /// returns a set of all available services (ones that are not listed as deleted).
        /// </summary>
        /// <returns></returns>
        public List<ServiceObj> GetAllAvailServices()
        {
            var db = new SmartManagerDataClassesDataContext();

            var serviceQuery = (from r in db.Services
                                where r.VDelDate == null
                                select r);
            List<ServiceObj> allServices = new List<ServiceObj>();

            foreach (var service in serviceQuery)
            {
                ArrayList info = new ArrayList();
                info.Add(service.ServiceName);
                info.Add(service.ServiceType);
                info.Add(service.ServiceCharge);

                ServiceObj serviceObj = new ServiceObj(info);
                allServices.Add(serviceObj);

            }

            return allServices;
        }

        public List<ServiceObj> GetAllTypeServices(String type)
        {
            var db = new SmartManagerDataClassesDataContext();

            var serviceQuery = (from r in db.Services
                                where r.VDelDate == null && r.ServiceType.Equals(type)
                                select r);
            List<ServiceObj> allServices = new List<ServiceObj>();

            foreach (var service in serviceQuery)
            {
                ArrayList info = new ArrayList();
                info.Add(service.ServiceName);
                info.Add(service.ServiceType);
                info.Add(service.ServiceCharge);

                ServiceObj serviceObj = new ServiceObj(info);
                allServices.Add(serviceObj);

            }

            return allServices;
        }

        public void david()
        {
        }

        #endregion


        #region Employee Related Methods
        /// <summary>
        /// Adds an employee with basic information
        /// </summary>
        /// <param name="employeeObj"></param>
        /// <returns
        public int AddEmployee(EmployeeObj employeeObj)
        {
            ArrayList EmployeeInfo = employeeObj.EmployeeInfo;

            var db = new SmartManagerDataClassesDataContext();

            var employee = new Employee();

            //setting up the values for columns
            employee.FirstName = EmployeeInfo[0].ToString();
            employee.LastName = EmployeeInfo[1].ToString();
            employee.EmployeeType = EmployeeInfo[2].ToString();
            employee.Password = EmployeeInfo[3].ToString();
            employee.IsLoggedIn = 0;
            employee.VDelDate = null;

            //process insert before submit
            db.Employees.InsertOnSubmit(employee);

            //try a submit change
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }

            // query employees
            var employeeQuery = (from e in db.Employees
                                 where e.VDelDate.Equals(null)
                                 select e).ToList().LastOrDefault();

            return (int)employeeQuery.EmployeeNumber;
        }

        /// <summary>
        /// Retrieves information based on
        /// parameters.
        /// "" Used for blank areas
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <returns> EmployeeObj</returns>
        public EmployeeObj GetEmployee(int employeeNumber)
        {

            var db = new SmartManagerDataClassesDataContext();

            // query employees
            var employeeQuery = (from e in db.Employees
                                 where e.EmployeeNumber.Equals(employeeNumber)
                                 where e.VDelDate.Equals(null)
                              select e).SingleOrDefault();

            //handle no results found
            if (employeeQuery == null)
            {
                return null;
            }

            // new instance of arrayList and employeeObj
            ArrayList newEmployeeInfo = new ArrayList();
            newEmployeeInfo.Add(employeeQuery.FirstName.ToString());
            newEmployeeInfo.Add(employeeQuery.LastName.ToString());
            newEmployeeInfo.Add(employeeQuery.EmployeeType.ToString());
            newEmployeeInfo.Add(employeeQuery.EmployeeNumber);
            newEmployeeInfo.Add(employeeQuery.Password.ToString());
            newEmployeeInfo.Add(employeeQuery.IsLoggedIn);
            newEmployeeInfo.Add(employeeQuery.VDelDate);

            EmployeeObj newEmployee = new EmployeeObj(employeeQuery.EmployeeID, newEmployeeInfo);

            return newEmployee;
        }

        /// <summary>
        /// Gets a list of all active employees
        /// </summary>
        /// <returns></returns>
        public List<EmployeeObj> GetAllEmployees()
        {
            var db = new SmartManagerDataClassesDataContext();

            var query = (from e in db.Employees
                         where e.VDelDate.Equals(null)
                         select e);

            List<EmployeeObj> allEmployees = new List<EmployeeObj>();

            foreach (var emp in query)
            {
                ArrayList info = new ArrayList();
                info.Add(emp.FirstName);
                info.Add(emp.LastName);
                info.Add(emp.EmployeeType);
                info.Add(emp.EmployeeNumber);
                info.Add(emp.Password);
                info.Add(emp.IsLoggedIn);

                EmployeeObj employee = new EmployeeObj(info);

                allEmployees.Add(employee);
            }

            return allEmployees;
        }

        /// <summary>
        /// Edits employee values
        /// </summary>
        /// <param name="employeeObj"></param>
        public void EditEmployee(int employeeNumber, EmployeeObj employee)
        {
            var db = new SmartManagerDataClassesDataContext();
            // query employees
            var employeeQuery = (from e in db.Employees
                              where e.EmployeeNumber.Equals(employeeNumber) &&
                              e.VDelDate.Equals(null)
                              select e).SingleOrDefault();

            if (employeeQuery != null)
            {
                // Assign new values
                employeeQuery.FirstName = employee.EmployeeInfo[0].ToString();
                employeeQuery.LastName = employee.EmployeeInfo[1].ToString();
                employeeQuery.EmployeeType = employee.EmployeeInfo[2].ToString();
                employeeQuery.Password = employee.EmployeeInfo[4].ToString();   

                db.SubmitChanges();
            }

           
        }

        /// <summary>
        /// Deletes employee by setting the VDelDate to current time
        /// </summary>
        /// <param name="employeeObj"></param>
        public void DeleteEmployee(int employeeNumber)
        {
            var db = new SmartManagerDataClassesDataContext();
            // query employees
            var employeeQuery = (from e in db.Employees
                                 where e.EmployeeNumber.Equals(employeeNumber) &&
                                        e.VDelDate.Equals(null)
                              select e).SingleOrDefault();

            if (employeeQuery == null)
                throw new ArgumentNullException();

            employeeQuery.VDelDate = DateTime.Now;

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        /// <summary>
        /// Logs employee in using their employeenumber and password and
        /// returns 1 if found in database. Otherwise, 
        /// return 0
        /// </summary>
        /// <param name="employeeNumber"> is the username </param>
        /// <param name="password"></param>
        public int LogInEmployee(int employeeNumber, String password)
        {
            var db = new SmartManagerDataClassesDataContext();
            
            // query employees
            var employeeQuery = (from e in db.Employees
                              where e.EmployeeNumber.Equals(employeeNumber)
                              where e.Password.Equals(password)
                              where e.VDelDate.Equals(null)
                              select e).SingleOrDefault();

            if (employeeQuery == null)
                return 0;

            employeeQuery.IsLoggedIn = 1;

            // Commits the modified flag of login
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }

            return 1;
        }

        /// <summary>
        /// Logs employee out
        /// </summary>
        /// <param name="employeeNumber">of the employee to logout</param>
        /// <param name="employeeObj"></param>
        public void LogOutEmployee(int employeeNumber, EmployeeObj employeeObj)
        {
            var db = new SmartManagerDataClassesDataContext();

            // query employees
            var employeeQuery = (from e in db.Employees
                                 where e.EmployeeNumber.Equals(employeeNumber)
                                 select e).SingleOrDefault();

            employeeQuery.IsLoggedIn = 0;

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            { Console.WriteLine(e.Message.ToString()); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="names">is [firstName, lastName]</param>
        /// <returns>list of EmployeeObj's</returns>
        public List<EmployeeObj> SearchEmployees(String[] names)
        {
            var db = new SmartManagerDataClassesDataContext();

            ArrayList queryList = new ArrayList();
            //start query
            var queryAll = (from e in db.Employees
                            where e.FirstName.Equals(names[0]) || e.LastName.Equals(names[0])
                            where e.VDelDate.Equals(null)
                            select e);

            //should search for all the names 
            foreach (var name in names)
            {
                var query = (from e in db.Employees
                             where e.FirstName.Equals(name) || e.LastName.Equals(name)
                             where e.VDelDate.Equals(null)
                             select e);

                queryAll = queryAll.Union(query);
            }

            //set up the List to be passed back
            List<EmployeeObj> employeeList = new List<EmployeeObj>();
            foreach (var q in queryAll)
            {

                ArrayList info = new ArrayList();
                //testing only first name and last name
                info.Add(q.FirstName);
                info.Add(q.LastName);
                info.Add(q.EmployeeType);
                info.Add(q.EmployeeNumber);
                info.Add(q.Password);
                info.Add(q.IsLoggedIn);
                info.Add(q.VDelDate);
                EmployeeObj employee = new EmployeeObj(info);

                employeeList.Add(employee);
            }

            return employeeList;

        }
        #endregion

    }
}
